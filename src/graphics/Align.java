package graphics;

/**
 * The Align enumeration class provides constant values to align displays or components.
 */
public enum Align
{
    /**
     * Defines the NONE value (0).
     */
    NONE ( 0 , "none" ) ,

    /**
     * Defines the CENTER value (1).
     */
    CENTER ( 1 , "c" ) ,

    /**
     * Defines the LEFT value (2).
     */
    LEFT ( 2 , "l" ) ,

    /**
     * Defines the RIGHT value (4).
     */
    RIGHT ( 4 , "r" ) ,

    /**
     * Defines the TOP value (8).
     */
    TOP ( 8 , "t" ) ,

    /**
     * Defines the BOTTOM value (16).
     */
    BOTTOM ( 16 , "b" ) ,

    /**
     * Defines the REVERSE value (32).
     */
    REVERSE ( 32 , "r" ) ,

    /**
     * Defines the BOTTOM_LEFT value (18).
     */
    BOTTOM_LEFT ( 18 , "bl" ) , // Align.BOTTOM | Align.LEFT

    /**
     * Defines the BOTTOM_RIGHT value (20).
     */
    BOTTOM_RIGHT ( 20 , "br" ) , // Align.BOTTOM | Align.RIGHT

    /**
     * Defines the CENTER_LEFT value (3).
     */
    CENTER_LEFT ( 3 , "cl" ) , // CENTER | LEFT ;

    /**
     * Defines the CENTER_RIGHT value (5).
     */
    CENTER_RIGHT ( 5 , "cr" ) , // CENTER | RIGHT ;

    /**
     * Defines the TOP_LEFT value (10).
     */
    TOP_LEFT ( 10 , "tl" ) , // TOP | LEFT ;

    /**
     * Defines the TOP_RIGHT value (12).
     */
    TOP_RIGHT ( 12 , "tr" ) , // TOP | RIGHT ;

    /**
     * Defines the LEFT_BOTTOM value (50).
     */
    LEFT_BOTTOM ( 50 , "lb" ) , // BOTTOM_LEFT | REVERSE ;

    /**
     * Defines the RIGHT_BOTTOM value (52).
     */
    RIGHT_BOTTOM ( 52 , "rb" ) , // BOTTOM_RIGHT | REVERSE ;

    /**
     * Defines the LEFT_TOP value (42).
     */
    LEFT_TOP ( 42 , "lb" ) , // TOP_LEFT | REVERSE ;

    /**
     * Defines the RIGHT_TOP value (44).
     */
    RIGHT_TOP ( 44 , "rt" ) ; // TOP_RIGHT | REVERSE ;

    /**
     * Creates a new Align instance.
     * @param value The value of this item.
     * @param name The name of this item.
     */
    Align( int value , String name )
    {
        _name  = name ;
        _value = value ;
    }

    /**
     * Returns the String representation of the object.
     * @return the String representation of the object.
     */
    public String getName()
    {
        return _name ;
    }

    /**
     * Returns the value of the object.
     * @return the value of the object.
     */
    public int getValue()
    {
        return _value ;
    }

    private String _name  ;
    private int    _value ;
}
