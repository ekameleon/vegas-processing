package graphics.geom;

/**
 * An utility class to transform and change the Matrix class.
 */
public class Matrixs
{
    /**
     * Appends a skew transformation to a matrix (angles in radians). The skew matrix has the following form:
     *  <pre>
     *  | cos(skewY)  -sin(skewX)  0 |
     *  | sin(skewY)   cos(skewX)  0 |
     *  |     0            0       1 |
     *  </pre>
     * @param matrix The Matrix to transform.
     * @param skewX The skewX value.
     * @param skewY The skewY value.
     */
    public static void skew( Matrix matrix , double skewX , double skewY )
    {
        double sinX = Math.sin(skewX);
        double cosX = Math.cos(skewX);
        double sinY = Math.sin(skewY);
        double cosY = Math.cos(skewY);
        matrix.setTo
        (
            matrix.a   * cosY - matrix.b * sinX ,
            matrix.a   * sinY + matrix.b * cosX ,
            matrix.c   * cosY - matrix.d * sinX ,
            matrix.c   * sinY + matrix.d * cosX ,
            matrix.tx * cosY - matrix.ty * sinX ,
            matrix.tx * sinY + matrix.ty * cosX
        );
    }

    /**
     * Uses a matrix to transform 2D coordinates into a different space. If you pass an <code>out</code>-point, the result will be stored in this point instead of creating a new object.
     * @param matrix The matrix to transform the specific coordinates.
     * @param x The x coordinate to transform.
     * @param y The y coordinate to transform.
     * @return The new point with the new coordinates.
     */
    public static Point transformCoords( Matrix matrix , double x , double y )
    {
        return transformCoords( matrix , x , y , null ) ;
    }

    /**
     * Uses a matrix to transform 2D coordinates into a different space. If you pass an <code>out</code>-point, the result will be stored in this point instead of creating a new object.
     * @param matrix The matrix to transform the specific coordinates.
     * @param x The x coordinate to transform.
     * @param y The y coordinate to transform.
     * @param out The point to store the new coordinates.
     * @return The new point with the new coordinates.
     */
    public static Point transformCoords( Matrix matrix , double x , double y , Point out )
    {
        if (out == null) out = new Point();
        out.x = matrix.a * x + matrix.c * y + matrix.tx;
        out.y = matrix.d * y + matrix.b * x + matrix.ty;
        return out;
    }

    /**
     * Transform a point with the given matrix.
     * @param matrix The matrix to transform the specific coordinates.
     * @param point The point to transform.
     * @return The new point with the new coordinates.
     */
    public static Point transformPoint( Matrix matrix, Point point )
    {
        return transformCoords(matrix, point.x, point.y );
    }

    /**
     * Transform a point with the given matrix.
     * @param matrix The matrix to transform the specific coordinates.
     * @param point The point to transform.
     * @param out The point to store the new coordinates.
     * @return The new point with the new coordinates.
     */
    public static Point transformPoint( Matrix matrix, Point point , Point out )
    {
        return transformCoords(matrix, point.x, point.y, out);
    }
}
