package graphics.geom;

/**
 * The Matrix class represents a transformation matrix that determines how to map points from one coordinate space to another. You can perform doubleious graphical transformations on a display object by setting the properties of a Matrix object, applying that Matrix object to the <code>matrix</code> property of a Transform object, and then applying that Transform object as the <code>transform</code> property of the display object. These transformation functions include translation (<i>x</i> and <i>y</i> repositioning), rotation, scaling, and skewing.
 */
public class Matrix
{
    /**
     * Creates a new Matrix object with the specified parameters.
     * <p>If you do not provide any parameters to the <code>new Matrix()</code> constructor, it creates an <i>identity matrix</i> with the following values:</p>a 
     * <pre>b </pre>
     * <pre>c </pre>
     * <pre>d </pre>
     * <pre>tx </pre>
     * <pre>ty </pre>
     * @param a The value that affects the positioning of pixels along the <i>x</i> axis when scaling or rotating an image.
     * @param b The value that affects the positioning of pixels along the <i>y</i> axis when rotating or skewing an image.
     * @param c The value that affects the positioning of pixels along the <i>x</i> axis when rotating or skewing an image.
     * @param d The value that affects the positioning of pixels along the <i>y</i> axis when scaling or rotating an image..
     * @param tx The distance by which to translate each point along the <i>x</i> axis.
     * @param ty The distance by which to translate each point along the <i>y</i> axis.
     */
    public Matrix( double a , double b , double c , double d , double tx , double ty )
    {
        this.a  =  a ;
        this.b  =  b ;
        this.c  =  c ;
        this.d  =  d ;
        this.tx = tx ;
        this.ty = ty ;
    }

    public Matrix( double a , double b , double c , double d , double tx )
    {
        this( a , b , c , d , tx , 0 ) ;
    }

    public Matrix( double a , double b , double c , double d  )
    {
        this( a , b , c, d, 0 , 0f ) ;
    }

    public Matrix( double a , double b , double c)
    {
        this( a , b , c, 1, 0 , 0 ) ;
    }

    public Matrix( double a, double b  )
    {
        this( a , b , 0 , 1, 0 , 0 ) ;
    }

    public Matrix( double a  )
    {
        this( a , 0 , 0 , 1 , 0 , 0 ) ;
    }

    public Matrix()
    {
        this( 1 , 0 , 0 , 1 , 0 , 0 ) ;
    }

    /**
     * The value that affects the positioning of pixels along the <i>x</i> axis when scaling or rotating an image.
     */
    public double a ;

    /**
     * The value that affects the positioning of pixels along the <i>y</i> axis when rotating or skewing an image.
     */
    public double b ;

    /**
     * The value that affects the positioning of pixels along the <i>x</i> axis when rotating or skewing an image.
     */
    public double c ;

    /**
     * The value that affects the positioning of pixels along the <i>y</i> axis when scaling or rotating an image.
     */
    public double d ;

    /**
     * The distance by which to translate each point along the <i>x</i> axis.
     */
    public double tx ;

    /**
     * The distance by which to translate each point along the <i>y</i> axis.
     */
    public double ty ;

    private static double MAGIC_GRADIENT_FACTOR = 16384/10;

    /**
     * Returns a new Matrix object that is a clone of this matrix, with an exact copy of the contained object.
     * @return A Matrix object.
     */
    public Matrix clone()
    {
        return new Matrix(a, b, c, d, tx, ty);
    }
    /**
     * Concatenates a matrix with the current matrix, effectively combining the geometric effects of the two. In mathematical terms, concatenating two matrixes is the same as combining them using matrix multiplication.
     * <p>For example, if matrix <code>m1</code> scales an object by a factor of four, and matrix <code>m2</code> rotates an object by 1.5707963267949 radians (<code>Math.PI/2</code>), then <code>m1.concat(m2)</code> transforms <code>m1</code> into a matrix that scales an object by a factor of four and rotates the object by <code>Math.PI/2</code> radians.</p>
     * <p>This method replaces the source matrix with the concatenated matrix. If you want to concatenate two matrixes without altering either of the two source matrixes, first copy the source matrix by using the <code>clone()</code> method, as shown in the Class Examples section.</p>
     * @param matrix The matrix to be concatenated to the source matrix.
     *
     */
    public void concat( Matrix matrix ) 
    {
        double a  = this.a;
        double b  = this.b;
        double c  = this.c;
        double d  = this.d;
        double tx = this.tx;
        double ty = this.ty;
        
        this.a  = matrix.a * a  + matrix.c * b;
        this.b  = matrix.b * a  + matrix.d * b;
        this.c  = matrix.a * c  + matrix.c * d;
        this.d  = matrix.b * c  + matrix.d * d;
        this.tx = matrix.a * tx + matrix.c * ty + matrix.tx;
        this.ty = matrix.b * tx + matrix.d * ty + matrix.ty;
    }

    /**
     * Copies all of data from the source Matrix object into the calling Matrix object.
     * @param source The source to copy in the current Matrix object.
     * @return The current Matrix reference.
     */
    public Matrix copyFrom( Matrix source )
    {
        this.a  = source.a ;
        this.b  = source.b ;
        this.c  = source.c ;
        this.d  = source.d ;
        this.tx = source.tx ;
        this.ty = source.ty ;
        return this ;
    }

    /**
     * Includes parameters for scaling, rotation, and translation. When applied to a matrix it sets the matrix's values based on those parameters.
     * <p>Using the <code>createBox()</code> method lets you obtain the same matrix as you would if you applied the <code>identity()</code>, <code>rotate()</code>, <code>scale()</code>, and <code>translate()</code> methods in succession. For example, <code>mat1.createBox(2,2,Math.PI/4, 100, 100)</code> has the same effect as the following:</p>
     * <pre>
     * import graphics.geom.Matrix;
     *
     * Matrix matrix = new Matrix();
     *
     * matrix.identity();
     * matrix.rotate(Math.PI/4);
     * matrix.scale(2,2);
     * matrix.translate(10,20);
     * </pre>
     * @param scaleX The factor by which to scale horizontally.
     * @param scaleY The factor by which scale vertically.
     * @param rotation The amount to rotate, in radians.
     * @param tx The number of pixels to translate (move) to the right along the <i>x</i> axis.
     * @param ty The number of pixels to translate (move) down along the <i>y</i> axis.
     */
    public void createBox( double scaleX , double scaleY , double rotation , double tx , double ty )
    {
        if ( rotation == 0 )
        {
            a = d = 1 ;
            b = c = 0 ;
        }
        else
        {
            a = Math.cos(rotation);
            b = Math.sin(rotation);
            c = -b;
            d = a;
        }
        if ( scaleX != 1 )
        {
            a *= scaleX;
            c *= scaleX;
        }
        if ( scaleY != 1 )
        {
            b *= scaleY;
            d *= scaleY;
        }
        this.tx = tx;
        this.ty = ty;
    }

    public void createBox( double scaleX , double scaleY , double rotation , double tx )
    {
        createBox( scaleX , scaleY , rotation , tx , 0 ) ;
    }

    public void createBox( double scaleX , double scaleY , double rotation )
    {
        createBox( scaleX , scaleY , rotation , 0 , 0 ) ;
    }

    public void createBox( double scaleX , double scaleY )
    {
        createBox( scaleX , scaleY , 0 , 0 , 0 ) ;
    }

    public void createGradientBox( double width , double height, double rotation, double tx, double ty)
    {
        this.createBox(width/MAGIC_GRADIENT_FACTOR, height/MAGIC_GRADIENT_FACTOR, rotation, tx + width/2, ty + height/2) ;
    }

    public void createGradientBox( double width , double height, double rotation, double tx)
    {
        this.createBox(width/MAGIC_GRADIENT_FACTOR, height/MAGIC_GRADIENT_FACTOR, rotation, tx + width/2, height/2 ) ;
    }

    public void createGradientBox( double width , double height, double rotation )
    {
        this.createBox(width/MAGIC_GRADIENT_FACTOR, height/MAGIC_GRADIENT_FACTOR, rotation, width/2, height/2 ) ;
    }

    public void createGradientBox( double width , double height )
    {
        this.createBox(width/MAGIC_GRADIENT_FACTOR, height/MAGIC_GRADIENT_FACTOR, 0, width/2, height/2 ) ;
    }

    /**
     * Given a point in the pre transform coordinate space, returns the coordinates of that point after the transformation occurs. Unlike the standard transformation applied using the <code>transformPoint()</code> method, the <code>deltaTransformPoint()</code> method's transformation does not consider the translation parameters <code>tx</code> and <code>ty</code>.
     * @param point The point for which you want to get the result of the matrix transformation.
     * @return The point resulting from applying the matrix transformation.
     */
    public Point deltaTransformPoint( Point point)
    {
        return new Point(a * point.x + c * point.y, b * point.x + d * point.y);
    }

    /**
     * Sets each matrix property to a value that causes a null transformation. An object transformed by applying an identity matrix will be identical to the original.
     * <p>After calling the <code>identity()</code> method, the resulting matrix has the following properties: <code>a</code>=1, <code>b</code>=0, <code>c</code>=0, <code>d</code>=1, <code>tx</code>=0, <code>ty</code>=0.</p>
     */
    public void identity()
    {
        a = d = 1;
        b = c = tx = ty = 0;
    }

    /**
     * Performs the opposite transformation of the original matrix. You can apply an inverted matrix to an object to undo the transformation performed when applying the original matrix.
     */
    public void invert()
    {
        double a  = this.a;
        double b  = this.b;
        double c  = this.c;
        double d  = this.d;
        double tx = this.tx;
        double ty = this.ty;

        double det = a*d - c*b;

        this.a = d/det;
        this.b = -b/det;
        this.c = -c/det;
        this.d = a/det;

        this.tx = - (this.a * tx + this.c * ty);
        this.ty = - (this.b * tx + this.d * ty);
    }

    /**
     * Applies a rotation transformation to the Matrix object.
     * <p>The <code>rotate()</code> method alters the <code>a</code>, <code>b</code>, <code>c</code>, and <code>d</code> properties of the Matrix object.
     * @param angle The rotation angle in radians.
     */
    public void rotate( double angle )
    {
        if (angle!=0)
        {
            double cos = Math.cos(angle);
            double sin = Math.sin(angle);
            double a = this.a;
            double b = this.b;
            double c = this.c;
            double d = this.d;
            double tx = this.tx;
            double ty = this.ty;
            
            this.a  = a*cos  - b*sin;
            this.b  = a*sin  + b*cos;
            this.c  = c*cos  - d*sin;
            this.d  = c*sin  + d*cos;
            this.tx = tx*cos - ty*sin;
            this.ty = tx*sin + ty*cos;
        }
    }
    /**
     * Applies a scaling transformation to the matrix. The <i>x</i> axis is multiplied by <code>sx</code>, and the <i>y</i> axis it is multiplied by <code>sy</code>.
     * <p>The <code>scale()</code> method alters the <code>a</code> and <code>d</code> properties of the Matrix object. In matrix notation, this is the same as concatenating the current matrix with the following matrix:</p>
     * @param sx A multiplier used to scale the object along the <i>x</i> axis.
     * @param sy A multiplier used to scale the object along the <i>y</i> axis.
     */
    public void scale( double sx , double sy )
    {
        if (sx != 1)
        {
            a *= sx;
            c *= sx;
            tx *= sx;
        }
        if (sy != 1)
        {
            b *= sy;
            d *= sy;
            ty *= sy;
        }
    }

    /**
     * Sets the Matrix object with the specified parameters.
     * @param a The value that affects the positioning of pixels along the <i>x</i> axis when scaling or rotating an image.
     * @param b The value that affects the positioning of pixels along the <i>y</i> axis when rotating or skewing an image.
     * @param c The value that affects the positioning of pixels along the <i>x</i> axis when rotating or skewing an image.
     * @param d The value that affects the positioning of pixels along the <i>y</i> axis when scaling or rotating an image..
     * @param tx The distance by which to translate each point along the <i>x</i> axis.
     * @param ty The distance by which to translate each point along the <i>y</i> axis.
     */
    public void setTo( double a, double b, double c, double d, double tx, double ty )
    {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.tx = tx;
        this.ty = ty;
    }

    /**
     * Returns a text value listing the properties of the Matrix object.
     * @return A string containing the values of the properties of the Matrix object: <code>a</code>, <code>b</code>, <code>c</code>, <code>d</code>, <code>tx</code>, and <code>ty</code>.
     */
    public String toString()
    {
        return "[Matrix a:" + a + " b:" + b + " c:" + c + " d:" + d + " tx:" +tx + " ty=" + ty + "]";
    }

    /**
     * Returns the result of applying the geometric transformation represented by the Matrix object to the specified point.
     * @param point The point for which you want to get the result of the Matrix transformation.
     * @return The point resulting from applying the Matrix transformation.
     */
    public Point transformPoint( Point point )
    {
        return new Point(a*point.x + c*point.y + tx, b * point.x + d *point.y + ty );
    }

    /**
     * Translates the matrix along the <i>x</i> and <i>y</i> axes, as specified by the <code>dx</code> and <code>dy</code> parameters.
     * @param dx The amount of movement along the <i>x</i> axis to the right, in pixels.
     * @param dy The amount of movement down along the <i>y</i> axis, in pixels.
     */
    public void translate( double dx , double dy )
    {
        tx += dx;
        ty += dy;
    }

    // -------------

    public void copyFromAndConcat( Matrix copyMatrix , Matrix concatMatrix )
    {
        double a1  = copyMatrix.a;
        double b1  = copyMatrix.b;
        double c1  = copyMatrix.c;
        double d1  = copyMatrix.d;
        double tx1 = copyMatrix.tx;
        double ty1 = copyMatrix.ty;

        double a2  = concatMatrix.a;
        double b2  = concatMatrix.b;
        double c2  = concatMatrix.c;
        double d2  = concatMatrix.d;
        double tx2 = concatMatrix.tx;
        double ty2 = concatMatrix.ty;

        a =   (a1 * a2 + b1 * c2);
        b =   (a1 * b2 + b1 * d2);
        c =   (c1 * a2 + d1 * c2);
        d =   (c1 * b2 + d1 * d2);
        tx =  (tx1 * a2 + ty1 * c2 + tx2);
        ty =  (tx1 * b2 + ty1 * d2 + ty2);
    }

    public void  copyFromAndInvert( Matrix matrix )
    {
        double a  = matrix.a;
        double b  = matrix.b;
        double c  = matrix.c;
        double d  = matrix.d;
        double tx = matrix.tx;
        double ty = matrix.ty;

        double det = a * d - b * c;

        this.a =    (d / det);
        this.b =  - (b / det);
        this.c =  - (c / det);
        this.d =    (a / det);
        this.tx = - (this.a * tx + this.c * ty);
        this.ty = - (this.b * tx + this.d * ty);
    }
}
