package graphics.geom;

/**
 * The EdgeMetrics class specifies the thickness, in pixels, of the four edge regions around a visual component.
 */
public class EdgeMetrics
{
    /**
     * Creates a new Rectangle instance.
     * @param left The width, in pixels, of the left edge region.
     * @param top The height, in pixels, of the top edge region.
     * @param right The width, in pixels, of the right edge region.
     * @param bottom The height, in pixels, of the bottom edge region.
     */
    public EdgeMetrics( double left , double top , double right , double bottom )
    {
        this.bottom = bottom ;
        this.left   = left ;
        this.right  = right ;
        this.top    = top ;
    }

    // -----------------

    /**
     * The bottom component value of the object.
     */
    public double bottom ;

    /**
     * The left component value of the object.
     */
    public double left ;

    /**
     * The right component value of the object.
     */
    public double right ;

    /**
     * The top component value of the object.
     */
    public double top ;

    /**
     * The horizontal, in pixels, of the sum of left and right region.
     * @return The horizontal, in pixels, of the sum of left and right region.
     */
    public double horizontal()
    {
        return left + right;
    }

    /**
     * The vertical, in pixels, of the sum of top and vertical region.
     * @return The vertical, in pixels, of the sum of top and vertical region.
     */
    public double vertical()
    {
        return top + bottom;
    }

    /**
     * Returns a shallow copy of the object.
     * @return a shallow copy of the object.
     */
    public EdgeMetrics clone()
    {
        return new EdgeMetrics( this.left , this.top , this.right , this.bottom ) ;
    }

    /**
     * Copies all of vector data from the source EdgeMetrics object into the calling EdgeMetrics object.
     * @param source The EdgeMetrics object from which to copy the data.
     * @return The current object reference.
     */
    public EdgeMetrics copyFrom( EdgeMetrics source )
    {
        bottom = source.bottom ;
        left   = source.left ;
        right  = source.right ;
        top    = source.top ;
        return this ;
    }

    /**
     * Compares the passed-in object with this object for equality.
     * @return A <code>true</code> value if the the specified object is equal with this object.
     * @param em The EdgeMetrics to evaluates.
     * @return A boolean who indicates if the passed-in object equals the current object.
     */
    public boolean equals( EdgeMetrics em )
    {
        return em.bottom == bottom && em.left == left &&
               em.right  == right && em.top == top ;
    }

    /**
     * Sets the members of EdgeMetrics to the specified values.
     * @param left The width, in pixels, of the left edge region.
     * @param top The height, in pixels, of the top edge region.
     * @param right The width, in pixels, of the right edge region.
     * @param bottom The height, in pixels, of the bottom edge region.
     * @return The current EdgeMetrics reference.
     */
    public EdgeMetrics setTo( double left , double top , double right , double bottom )
    {
        this.left   = Double.isNaN(left)   ? 0 : left ;
        this.top    = Double.isNaN(top)    ? 0 : top ;
        this.bottom = Double.isNaN(bottom) ? 0 : bottom ;
        this.right  = Double.isNaN(right)  ? 0 : right ;
        return this ;
    }

    /**
     * Sets the members of EdgeMetrics to the specified values.
     * @return The current EdgeMetrics reference.
     */
    public EdgeMetrics setTo()
    {
        return setTo( 0 , 0 , 0 , 0 ) ;
    }

    /**
     * Returns the string representation of this instance.
     * @return The string representation of this instance.
     */
    public String toString()
    {
        return "[EdgeMetrics left:" + left + " top:" + top + " right:" + right + " bottom:" + bottom + "]" ;
    }
}
