package graphics.geom;

/**
 * The Dimension object encapsulates the width and height components of an object.
 */
public class Dimension
{
    /**
     * Creates a new Dimension instance.
     * @param width The width component value of the object.
     * @param height The height component value of the object.
     */
    public Dimension( double width , double height )
    {
        this.width = width ;
        this.height = height ;
    }

    /**
     * Creates a new Dimension instance.
     * @param width The width component value of the object.
     */
    public Dimension( double width )
    {
        this.width = width ;
        this.height = 0 ;
    }

    /**
     * Creates a new Dimension instance.
     */
    public Dimension()
    {
        this.width = 0 ;
        this.height = 0 ;
    }

    /**
     * The height component value of the object.
     */
    public double height ;

    /**
     * The width component value of the object.
     */
    public double width ;

    /**
     * Clean the current object.
     * @return A Dimension object.
     */
    public Dimension clear()
    {
        this.width  = 0 ;
        this.height = 0 ;
        return this ;
    }

    /**
     * Returns a new Dimension object that is a clone of this object, with an exact copy of the contained object.
     * @return A Dimension object.
     */
    public Dimension clone()
    {
        return new Dimension(width,height);
    }

    /**
     * Copies all of data from the source Dimension object into the calling Dimension object.
     * @param source The source to copy in the current Dimension object.
     * @return The current Dimension reference.
     */
    public Dimension copyFrom( Dimension source )
    {
        this.width  = source.width ;
        this.height = source.height ;
        return this ;
    }

    /**
     * Decreases the size by a specific width/height values and return its self(this).
     * @param dWidth A number value to decrease the width component of the object.
     * @param dHeight A number value to decrease the height component of the object.
     * @return The current Dimension reference.
     */
    public Dimension decrease( double dWidth , double dHeight )
    {
        width  -= dWidth ;
        height -= dHeight ;
        return this ;
    }

    /**
     * Determines whether two objects are equal. Two dimensions are equal if they have the same <i>width</i> and <i>height</i> values.
     * @param dim The dimension to be compared.
     * @return A value of <code>true</code> if the object is equal to this Point object; <code>false</code> if it is not equal.
     */
    public boolean equals( Dimension dim )
    {
        return width == dim.width && height == dim.height ;
    }

    /**
     * Increases the size by a specific width/height values and return its self(this).
     * @param dWidth A number value to increase the width component of the object.
     * @param dHeight A number value to increase the height component of the object.
     * @return The current Dimension reference.
     */
    public Dimension increase( double dWidth , double dHeight )
    {
        width  += dWidth ;
        height += dHeight ;
        return this ;
    }

    /**
     * Determines whether or not this Rectangle object is empty.
     * @return A value of true if the dimension object's width or height is less than or equal to 0; otherwise false.
     */
    public boolean isEmpty()
    {
        return width <= 0 || height <= 0;
    }

    /**
     * Sets the members of Dimension to the specified values.
     * @param width The width component value to change.
     * @param height The height component value to change.
     * @return The object reference.
     */
    public Dimension setTo( double width , double height )
    {
        this.width  = width ;
        this.height = height ;
        return this ;
    }

    /**
     * Returns the string representation of this object.
     * @return the string representation of this object.
     */
    public String toString()
    {
        return "[Dimension width:" + width + " height:" + height + "]" ;
    }
}
