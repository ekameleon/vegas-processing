package graphics.geom;

/**
 * The Point object represents a location in a two-dimensional coordinate system, where <i>x</i> represents the horizontal axis and <i>y</i> represents the vertical axis.
 */
public class Point
{
    /**
     * Creates a new Point object with the specified parameters.
     * @param x The horizontal coordinate.
     * @param y The vertical coordinate.
     */
    public Point( double x , double y )
    {
        this.x = x ;
        this.y = y ;
    }

    public Point( double x  )
    {
        this.x = x ;
        this.y = 0 ;
    }

    public Point()
    {
        this.x = 0 ;
        this.y = 0 ;
    }

    /**
     * The value that affects the positioning of pixels along the <i>x</i> axis when scaling or rotating an image.
     */
    public double x ;

    /**
     * The value that affects the positioning of pixels along the <i>y</i> axis when rotating or skewing an image.
     */
    public double y ;

    /**
     * Transforms the coordinates of the vector to used absolute value for the x and y properties.
     */
    public void abs()
    {
        x = Math.abs( x ) ;
        y = Math.abs( y ) ;
    }

    /**
     * Adds the coordinates of another point to the coordinates of this point to create a new point.
     * @param v The point to be added.
     * @return The new point.
     */
    public Point add( Point v )
    {
        return new Point(x+v.x, y+v.y);
    }

    /**
     * Returns a new Point object that is a clone of this point, with an exact copy of the contained object.
     * @return A Point object.
     */
    public Point clone()
    {
        return new Point(x,y);
    }

    /**
     * Returns the distance between <code>p1</code> and <code>p2</code>.
     * @param p1 The first point.
     * @param p2 The second point.
     * @return The distance value between two points.
     */
    public static double distance( Point p1 , Point p2 )
    {
        return diagonalLength(p2.x - p1.x , p2.y - p1.y );
    }

    /**
     * Determines whether two points are equal. Two points are equal if they have the same <i>x</i> and <i>y</i> values.
     * @param point The point to be compared.
     * @return A value of <code>true</code> if the object is equal to this Point object; <code>false</code> if it is not equal.
     */
    public boolean equals( Point point )
    {
        return x == point.x && y == point.y;
    }

    /**
     * The length of the line segment from (0,0) to this point.
     * @return The length of the line segment from (0,0) to this point.
     */
    public double getLength()
    {
        return diagonalLength(x, y);
    }

    /**
     * Determines a point between two specified points. The parameter <code>f</code> determines where the new interpolated point is located relative to the two end points specified by parameters <code>pt1</code> and <code>pt2</code>. The closer the value of the parameter <code>f</code> is to <code>1.0</code>, the closer the interpolated point is to the first point (parameter <code>pt1</code>). The closer the value of the parameter <code>f</code> is to 0, the closer the interpolated point is to the second point (parameter <code>pt2</code>).
     * @param p1 The first point.
     * @param p2 The second point.
     * @param value The level of interpolation between the two points. Indicates where the new point will be, along the line between <code>pt1</code> and <code>pt2</code>. If <code>f</code>=1, <code>pt1</code> is returned; if <code>f</code>=0, <code>pt2</code> is returned.
     * @return The new, interpolated point.
     *
     */
    public static Point interpolate( Point p1 , Point p2 , double value )
    {
        return new Point(p1.x * value + p2.x * (1 - value), p1.y * value + p2.y * (1 - value));
    }

    /**
     * Scales the line segment between (0,0) and the current point to a set length.
     * @param thickness The scaling value. For example, if the current point is (0,5), and you normalize it to 1, the point returned is at (0,1).
     */
    public void normalize( double thickness )
    {
        if (x != 0 || y != 0)
        {
            double relativeThickness = thickness / getLength();
            x *= relativeThickness;
            y *= relativeThickness;
        }
    }
    /**
     * Offsets the Point object by the specified amount. The value of <code>dx</code> is added to the original value of <i>x</i> to create the new <i>x</i> value. The value of <code>dy</code> is added to the original value of <i>y</i> to create the new <i>y</i> value.
     * @param dx The amount by which to offset the horizontal coordinate, <i>x</i>.
     * @param dy The amount by which to offset the vertical coordinate, <i>y</i>.
     */
    public void offset( double dx , double dy )
    {
        x += dx;
        y += dy;
    }

    /**
     * Converts a pair of polar coordinates to a polar point coordinate.
     * @param len The length coordinate of the polar pair.
     * @param angle The angle, in radians, of the polar pair.
     * @return The polar point.
     */
    public static Point polar( double len , double angle )
    {
        return new Point(len * Math.cos(angle), len * Math.sin(angle));
    }

    /**
     * Subtracts the coordinates of another point from the coordinates of this point to create a new point.
     * @param point The point to be subtracted.
     * @return The new point.
     */
    public Point subtract( Point point )
    {
        return new Point(x - point.x, y - point.y);
    }

    private static double diagonalLength( double x , double y )
    {
        return x == 0 ? Math.abs(y) : y == 0 ? Math.abs(x) : Math.sqrt(x * x + y * y);
    }
}
