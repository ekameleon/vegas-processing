package graphics.geom;

import java.util.ArrayList;

/**
 * The Rectangle class is used to create and modify Rectangle objects.
 * <p>A Rectangle object is an area defined by its position, as indicated by its top-left corner point <code>(x, y)</code>, and by its width and its height.</p>
 * <p>The <code>x</code>, <code>y</code>, <code>width</code>, and <code>height</code> properties of the Rectangle class are independent of each other; changing the value of one property has no effect on the others.</p>

 */
public class Rectangle extends Dimension
{
    /**
     * Creates a new Rectangle instance.
     * @param x The x component value of the object.
     * @param y The y component value of the object.
     * @param width The width component value of the object.
     * @param height The height component value of the object.
     */
    public Rectangle( double x , double y , double width , double height )
    {
        this.x = x ;
        this.y = y ;
        this.width = width ;
        this.height = height ;
    }

    /**
     * Creates a new Rectangle instance.
     * @param x The x component value of the object.
     * @param y The y component value of the object.
     * @param width The width component value of the object.
     */
    public Rectangle( double x , double y , double width )
    {
        this.x = x ;
        this.y = y ;
        this.width = width ;
        this.height = 0 ;
    }

    /**
     * Creates a new Rectangle instance.
     * @param x The x component value of the object.
     * @param y The y component value of the object.
     */
    public Rectangle( double x , double y )
    {
        this.x = x ;
        this.y = y ;
        this.width = 0 ;
        this.height = 0 ;
    }

    /**
     * Creates a new Rectangle instance.
     * @param x The x component value of the object.
     */
    public Rectangle( double x )
    {
        this.x = x ;
        this.y = 0 ;
        this.width = 0 ;
        this.height = 0 ;
    }

    /**
     * Creates a new Rectangle instance.
     */
    public Rectangle()
    {
        x = 0 ;
        y = 0 ;
        width = 0 ;
        height = 0 ;
    }

    // -----------------

    /**
     * The x component value of the object.
     */
    public double x ;

    /**
     * The y component value of the object.
     */
    public double y ;

    // -----------------

    /**
     * The area of the rectangle.
     * @return The area of the rectangle.
     */
    public double area()
    {
        return width * height ;
    }

    /**
     * The perimeter of the rectangle.
     * @return The perimeter of the rectangle.
     */
    public double perimeter()
    {
        return (width*2) + (height*2);
    }

    // -----------------

    /**
     * The sum of the y and height properties.
     * @return the sum of the y and height properties.
     */
    public double getBottom()
    {
        return y + height ;
    }

    /**
     * Sets the sum of the y and height properties.
     * @param value The bottom value.
     */
    public void setBottom( double value )
    {
        height = value - y ;
    }

    /**
     * The location of the Rectangle object's bottom-left corner, determined by the values of the left and bottom properties.
     * @return The location of the Rectangle object's bottom-let corner, determined by the values of the left and bottom properties.
     */
    public Point getBottomLeft()
    {
        return new Point( x , y + height) ;
    }

    /**
     * Sets the location of the Rectangle object's bottom-left corner, determined by the values of the left and bottom properties.
     * @param point the Point who defines the location of the Rectangle object's bottom-left corner, determined by the values of the left and bottom properties.
     */
    public void setBottomLeft( Point point )
    {
        width = width + (x - point.x) ;
        height = point.y - y ;
        x = point.x ;
    }

    /**
     * The location of the Rectangle object's bottom-right corner, determined by the values of the right and bottom properties.
     * @return The location of the Rectangle object's bottom-right corner, determined by the values of the right and bottom properties.
     */
    public Point getBottomRight()
    {
        return new Point(x + width, y + height ) ;
    }

    /**
     * Sets the location of the Rectangle object's bottom-right corner, determined by the values of the right and bottom properties.
     * @param point the Point who defines the location of the Rectangle object's bottom-right corner, determined by the values of the right and bottom properties.
     */
    public void setBottomRight( Point point )
    {
        width  = point.x - x ;
        height = point.y - y ;
    }

    /**
     * The location of the Rectangle object's center.
     * @return The location of the Rectangle object's center.
     */
    public Point getCenter()
    {
        return new Point( x + width  * 0.5 , y + height * 0.5 );
    }

    /**
     * Sets location of the Rectangle object's center.
     * @param point the Point who defines location of the Rectangle object's center.
     */
    public void setCenter( Point point )
    {
        x = point.x - ( width  * 0.5 ) ;
        y = point.y - ( height * 0.5 ) ;
    }

    /**
     * Returns the x coordinate of the Rectangle center.
     * @return the x coordinate of the Rectangle center.
     */
    public double getCenterX()
    {
        return x + ( width * 0.5 ) ;
    }

    /**
     * Sets the x coordinate of the Rectangle center.
     * @param value the x coordinate value of the Rectangle center.
     */
    public void setCenterX( double value )
    {
        x = value - ( width * 0.5 ) ;
    }

    /**
     * Returns the y coordinate of the Rectangle center.
     * @return the y coordinate of the Rectangle center.
     */
    public double getCenterY()
    {
        return y + ( height * 0.5 ) ;
    }

    /**
     * Sets the y coordinate of the Rectangle center.
     * @param value the y coordinate value of the Rectangle center.
     */
    public void setCenterY( double value )
    {
        y = value - ( height * 0.5 ) ;
    }

    /**
     * Returns the x coordinate of the top-left corner of the rectangle.
     * @return the x coordinate of the top-left corner of the rectangle.
     */
    public double getLeft()
    {
        return x ;
    }

    /**
     * Sets the x coordinate of the top-left corner of the rectangle.
     * @param value the value of the x coordinate of the top-left corner of the rectangle.
     */
    public void setLeft( double value )
    {
        width = width + (x-value) ;
        x = value ;
    }

    /**
     * Returns sum of the x and width properties.
     * @return the sum of the x and width properties.
     */
    public double getRight()
    {
        return x + width ;
    }

    /**
     * Sets the sum of the x and width properties.
     * @param value the value of the right component.
     */
    public void setRight( double value )
    {
        width = value - x ;
    }

    /**
     * The size of the Rectangle object, expressed as a Point object with the values of the width and height properties.
     * @return The size of the Rectangle object, expressed as a Point object with the values of the width and height properties.
     */
    public Point getSize()
    {
        return new Point(width, height) ;
    }

    /**
     * Sets the size of the Rectangle object, expressed as a Point object with the values of the width and height properties.
     * @param point the Point who defines the new width/height components of the rectangle.
     */
    public void setSize( Point point )
    {
        width  = point.x ;
        height = point.y ;
    }


    /**
     * Sets the size of the Rectangle object, expressed as a Point object with the values of the width and height properties.
     * @param width the new width value of the Rectangle.
     * @param height the new height value of the Rectangle.
     */
    public void setSize( double width , double height )
    {
        this.width  = width ;
        this.height = height ;
    }

    /**
     * The y coordinate of the top-left corner of the rectangle.
     * @return the y coordinate of the top-left corner of the rectangle.
     */
    public double getTop()
    {
        return y ;
    }

    /**
     * Sets the y coordinate of the top-left corner of the rectangle.
     * @param value The y position of the top-left corner.
     */
    public void setTop( double value )
    {
        height = height + (y - value);
        y = value ;
    }

    /**
     * Gets location of the Rectangle object's top-left corner, determined by the x and y coordinates of the point.
     * @return location of the Rectangle object's top-left corner, determined by the x and y coordinates of the point.
     */
    public Point getTopLeft()
    {
        return new Point( x , y ) ;
    }

    /**
     * Sets location of the Rectangle object's top-left corner, determined by the x and y coordinates of the point.
     * @param point The position of the top-left corner.
     */
    public void setTopLeft( Point point )
    {
        width  = width  + (x - point.x) ;
        height = height + (y - point.y) ;
        x      = point.x ;
        y      = point.y ;
    }

    /**
     * Gets the location of the Rectangle object's top-right corner, determined by the x and y coordinates of the point.
     * @return the location of the Rectangle object's top-right corner, determined by the x and y coordinates of the point.
     */
    public Point getTopRight()
    {
        return new Point( x + width , y ) ;
    }

    /**
     * Sets the location of the Rectangle object's top-right corner, determined by the x and y coordinates of the point.
     * @param point The position of the top-right corner.
     */
    public void setTopRight( Point point )
    {
        width  = point.x - x;
        height = height + (y - point.y) ;
        y      = point.y ;
    }

    // -----------------

    /**
     * Calculates the Axis Aligned Bounding Box (or aabb) from an array of points.
     * @param points The array of one or more points.
     * @param rec Optional rectangle to store the value in, if not supplied a new Rectangle object will be created.
     * @return The new Rectangle object.
     */
    public static Rectangle aabb( ArrayList<Point> points , Rectangle rec )
    {
        if ( rec == null )
        {
            rec = new Rectangle();
        }
        if( points.size() > 0 )
        {
            double xMax = Double.POSITIVE_INFINITY,
                   xMin = Double.NEGATIVE_INFINITY,
                   yMax = Double.NEGATIVE_INFINITY,
                   yMin = Double.POSITIVE_INFINITY;

            Point point ;

            for( int i = 0 , len = points.size() ; i<len ; i++ )
            {
                point = points.get(i) ;
                if (point.x > xMax)
                {
                    xMax = point.x;
                }
                if (point.x < xMin)
                {
                    xMin = point.x;
                }
                if (point.y > yMax)
                {
                    yMax = point.y;
                }
                if (point.y < yMin)
                {
                    yMin = point.y;
                }
            }
            rec.setTo(xMin, yMin,xMax - xMin,yMax - yMin);
        }
        return rec ;
    }

    public static Rectangle aabb( ArrayList<Point> points )
    {
       return aabb( points , null ) ;
    }

    /**
     * Centers this Rectangle so that the center coordinates match the given x and y values.
     * @param x The x coordinate to place the center of the Rectangle at.
     * @param y The y coordinate to place the center of the Rectangle at.
     * @return The current reference of the object.
     */
    public Rectangle centerOn( double x , double y )
    {
        this.x = x - ( this.width  * 0.5 ) ;
        this.y = y - ( this.height * 0.5 ) ;
        return this;
    }

    /**
     * Clean the current object.
     * @return A Dimension object.
     */
    public Rectangle clear()
    {
        this.x = 0 ;
        this.y = 0 ;
        this.width  = 0 ;
        this.height = 0 ;
        return this ;
    }

    /**
     * Compares all properties of the given rectangle, returning true only if they are equal (with the given accuracy 'e').
     * @param r1 The first rectangle to compare.
     * @param r2 The second rectangle to compare.
     * @return A boolean <code>true</code> value if all properties of the given rectangle, returning true only if they are equal (with the given accuracy 'e').
     */
    public static boolean compare( Rectangle r1, Rectangle r2 )
    {
        return compare( r1, r2 , 0.0001f ) ;
    }

    /**
     * Compares all properties of the given rectangle, returning true only if they are equal (with the given accuracy 'e').
     * @param r1 The first rectangle to compare.
     * @param r2 The second rectangle to compare.
     * @param e The accuracy value to compare the two rectangles.
     * @return A boolean <code>true</code> value if all properties of the given rectangle, returning true only if they are equal (with the given accuracy 'e').
     */
    public static boolean compare( Rectangle r1, Rectangle r2 , float e )
    {
        if (r1 == null)
        {
            return r2 == null ;
        }
        else if (r2 == null)
        {
            return false;
        }
        else
        {
            return r1.x > r2.x - e && r1.x < r2.x + e &&
                    r1.y > r2.y - e && r1.y < r2.y + e &&
                    r1.width  > r2.width  - e && r1.width  < r2.width  + e &&
                    r1.height > r2.height - e && r1.height < r2.height + e;
        }
    }

    /**
     * Returns a new Rectangle object that is a clone of this object, with an exact copy of the contained object.
     * @return A Rectangle object.
     */
    public Rectangle clone()
    {
        return new Rectangle(x,y,width,height);
    }

    /**
     * Determines whether the specified point is contained within the rectangular region defined by this Rectangle object.
     * @param x The x position of the point to check.
     * @param y The y position of the point to check.
     * @return A <code>true</code> value if the x,y coordinates are inside the rectangle area.
     */
    public boolean contains( double x , double y )
    {
        return (this.x <= x) && (this.x + this.width) > x && (this.y <= y) && (this.y + this.height) > y ;
    }

    /**
     * Determines whether the specified point is contained within the rectangular region defined by this Rectangle object.
     * @param point The point to check.
     * @return A <code>true</code> value if the point is inside the rectangle area.
     */
    public boolean contains( Point point )
    {
        return (this.x <= point.x) && (this.x + this.width) > point.x && (this.y <= point.y) && (this.y + this.height) > point.y ;
    }

    /**
     * Determines whether the Rectangle object specified by the rect parameter is contained within this Rectangle object.
     * @param rec The rectangle area to check.
     * @return A <code>true</code> value if the rectangle is inside the rectangle area.
     */
    public boolean contains( Rectangle rec )
    {
        double a = rec.x + rec.width ;
        double b = rec.y + rec.height ;
        double c = this.x + this.width ;
        double d = this.y + this.height ;
        return (rec.x >= this.x && rec.x < c && rec.y >= this.y && rec.y < d && a > this.x && a <= c && b > this.y && b <= d) ;
    }

    /**
     * Copies all of data from the source Rectangle object into the calling Rectangle object.
     * @param source The source to copy in the current Rectangle object.
     * @return The current Rectangle reference.
     */
    public Rectangle copyFrom( Rectangle source )
    {
        this.x = source.x ;
        this.y = source.y ;
        this.width  = source.width ;
        this.height = source.height ;
        return this ;
    }

    /**
     * Copies all of data from the source Dimension object into the calling Rectangle object.
     * @param source The source to copy in the current Rectangle object.
     * @return The current Rectangle reference.
     */
    public Rectangle copyFrom( Dimension source )
    {
        this.width  = source.width ;
        this.height = source.height ;
        return this ;
    }

    /**
     * Copies all of data from the source Point object into the calling Rectangle object.
     * @param source The source to copy in the current Rectangle object.
     * @return The current Rectangle reference.
     */
    public Rectangle copyFrom( Point source )
    {
        this.x = source.x ;
        this.y = source.y ;
        return this ;
    }

    /**
     * Determines whether two objects are equal.
     * @param rect The rectangle to be compared.
     * @return A value of <code>true</code> if the object is equal to this Point object; <code>false</code> if it is not equal.
     */
    public boolean equals( Rectangle rect )
    {
        return x == rect.x && y == rect.y && width == rect.width && height == rect.height ;
    }

    /**
     * Increases the size of the Rectangle object by the specified amounts, in pixels.
     * The center point of the Rectangle object stays the same, and its size increases to the left and right by the dx value, and to the top and the bottom by the dy value.
     * @param dx The value to be added to the left and the right of the Rectangle object. The following equation is used to calculate the new width and position of the rectangle:  x -= dx; width += 2 * dx;
     * @param dy The value to be added to the top and the bottom of the Rectangle. The following equation is used to calculate the new height and position of the rectangle: y -= dy; height += 2 * dy;
     * @return The object reference.
     */
    public Rectangle inflate( double dx , double dy )
    {
        this.x -= dx;
        this.y -= dy;
        this.width  += 2 * dx;
        this.height += 2 * dy;
        return this ;
    }

    /**
     * Increases the size of the Rectangle object. This method is similar to the Rectangle.inflate() method except it takes a Point object as a parameter.
     * @param point The x property of this Point object is used to increase the horizontal dimension of the Rectangle object. The y property is used to increase the vertical dimension of the Rectangle object.
     * @return The object reference.
     */
    public Rectangle inflatePoint( Point point )
    {
        this.x -= point.x;
        this.y -= point.y;
        this.width  += 2 * point.x;
        this.height += 2 * point.y;
        return this ;
    }

    /**
     * If the Rectangle object specified in the toIntersect parameter intersects with this Rectangle object, returns the area of intersection as a Rectangle object. If the rectangles do not intersect, this method returns an empty Rectangle object with its properties set to 0.
     * @param toIntersect The Rectangle object to compare against to see if it intersects with this Rectangle object.
     * @return A Rectangle object that equals the area of intersection. If the rectangles do not intersect, this method returns an empty Rectangle object; that is, a rectangle with its x, y, width, and height properties set to 0.
     */
    public Rectangle intersection( Rectangle toIntersect )
    {
        Rectangle rec = new Rectangle() ;
        if ( isEmpty() || toIntersect.isEmpty() )
        {
            rec.setTo(0,0);
            return rec ;
        }
        rec.x = Math.max( x, toIntersect.x);
        rec.y = Math.max( y, toIntersect.y);
        rec.width  = Math.min( x + width  , toIntersect.x + toIntersect.width  ) - rec.x ;
        rec.height = Math.min( y + height , toIntersect.y + toIntersect.height ) - rec.y ;
        if ( rec.width <= 0 || rec.height <= 0 )
        {
            rec.setTo(0,0);
        }
        return rec ;
    }

    /**
     * Determines whether the object specified in the toIntersect parameter intersects with this Rectangle object. This method checks the x, y, width, and height properties of the specified Rectangle object to see if it intersects with this Rectangle object.
     * @param toIntersect The Rectangle object to compare against this Rectangle object.
     * @return A value of true if the specified object intersects with this Rectangle object; otherwise false.
     */
    public boolean intersects ( Rectangle toIntersect )
    {
        return !this.intersection(toIntersect).isEmpty() ;
    }

    /**
     * Adjusts the location of the object, as determined by its top-left corner, by the specified amounts.
     * @param dx Moves the x value of the Rectangle object by this amount.
     * @param dy Moves the y value of the Rectangle object by this amount.
     * @return The object reference.
     */
    public Rectangle offset( double dx , double dy )
    {
        this.x += dx ;
        this.y += dy ;
        return this ;
    }

    /**
     * Adjusts the location of the Rectangle object using a Point object as a parameter. This method is similar to the Rectangle.offset() method, except that it takes a Point object as a parameter.
     * @param point A Point object to use to offset this Rectangle object.
     * @return The object reference.
     */
    public Rectangle offsetPoint( Point point )
    {
        this.x += point.x ;
        this.y += point.y ;
        return this ;
    }

    /**
     * Sets the width and height members of Rectangle to the specified values.
     * @param width The width of the rectangle, in pixels (default 0).
     * @param height The height of the rectangle, in pixels (default 0).
     * @return The object reference.
     */
    public Rectangle resize( double width , double height )
    {
        this.width = width ;
        this.height = height ;
        return this ;
    }

    /**
     * Sets the members of Rectangle to the specified values.
     * @param x The x component value to change.
     * @param y The y component value to change.
     * @param width The width component value to change.
     * @param height The height component value to change.
     * @return The object reference.
     */
    public Rectangle setTo( double x , double y , double width , double height )
    {
        this.x = x ;
        this.y = y ;
        this.width  = width ;
        this.height = height ;
        return this ;
    }

    /**
     * Returns the string representation of this object.
     * @return the string representation of this object.
     */
    public String toString()
    {
        return "[Rectangle x:" + x + " y:" + y + " width:" + width + " height:" + height + "]" ;
    }

    /**
     * Adds two rectangles together to create a new Rectangle object, by filling in the horizontal and vertical space between the two rectangles.
     * <b>Note:</b> The union() method ignores rectangles with 0 as the height or width value, such as: Rectangle rect2 = new Rectangle(300,300,50,0);
     * @param toUnion The rectangle to unify with the current Rectangle.
     * @return A new Rectangle object that is the union of the two rectangles.
     */
    public Rectangle union( Rectangle toUnion )
    {
        if ( this.width <= 0 || this.height <= 0 )
        {
            return toUnion.clone() ;
        }
        else if ( toUnion.width <= 0 || toUnion.height <= 0 )
        {
            return this.clone() ;
        }
        else
        {
            Rectangle rec = new Rectangle();
            rec.x = Math.min(this.x, toUnion.x);
            rec.y = Math.min(this.y, toUnion.y);
            rec.width  = Math.max(this.x + this.width  , toUnion.x + toUnion.width  ) - rec.x ;
            rec.height = Math.max(this.y + this.height , toUnion.y + toUnion.height ) - rec.y ;
            return rec ;
        }
    }
}