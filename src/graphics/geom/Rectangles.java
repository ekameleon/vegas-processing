package graphics.geom;

public class Rectangles
{
    /**
     * Calculates the bounds of a rectangle after transforming it by a matrix.
     * @param rectangle The rectangle to check.
     * @param matrix The matrix used to transform the rectangle.
     * @return The new Rectangle transformation copy of the original rectangle after transforming by a matrix.
     */
    public static Rectangle getBounds( Rectangle rectangle, Matrix matrix )
    {
        return getBounds( rectangle , matrix ) ;
    }

    /**
     * Calculates the bounds of a rectangle after transforming it by a matrix.
     * @param rectangle The rectangle to check.
     * @param matrix The matrix used to transform the rectangle.
     * @param out The optional output Rectangle reference. If you pass an <code>out</code>-rectangle, the result will be stored in this rectangle instead of creating a new object.
     * @return The new Rectangle transformation copy of the original rectangle after transforming by a matrix.
     */
    public static Rectangle getBounds( Rectangle rectangle, Matrix matrix , Rectangle out )
    {
        if (out == null)
        {
            out = new Rectangle();
        }

        double minX = Double.MAX_VALUE  ;
        double maxX = -Double.MAX_VALUE ;
        double minY = Double.MAX_VALUE  ;
        double maxY = -Double.MAX_VALUE ;

        Point[] positions = getPositions(rectangle, _positions);

        for ( int i = 0 ; i<4 ; ++i )
        {
            Matrixs.transformCoords(matrix, positions[i].x, positions[i].y, _point);

            if (minX > _point.x) minX = _point.x;
            if (maxX < _point.x) maxX = _point.x;
            if (minY > _point.y) minY = _point.y;
            if (maxY < _point.y) maxY = _point.y;
        }

        out.setTo(minX, minY, maxX - minX, maxY - minY);
        return out;
    }

    /**
     * Returns a vector containing the positions of the four edges of the given rectangle.
     * @param rectangle The Rectangle to check.
     * @param out If you pass an <code>out</code>-array of Point objects, the result will be stored in this array instead of creating a new object.
     * @return a vector containing the positions of the four edges of the given rectangle.
     */
    public static Point[] getPositions( Rectangle rectangle , Point[] out )
    {
        if (out == null)
        {
            out = new Point[4];
        }

        for ( int i  = 0 ; i<4 ; ++i )
        {
            if ( out[i] == null )
            {
                out[i] = new Point() ;
            }
        }

        out[0].x = rectangle.getLeft();
        out[0].y = rectangle.getTop();

        out[1].x = rectangle.getRight();
        out[1].y = rectangle.getTop();

        out[2].x = rectangle.getLeft();
        out[2].y = rectangle.getBottom();

        out[3].x = rectangle.getRight();
        out[3].y = rectangle.getBottom();

        return out;
    }

    private static Point   _point     = new Point() ;
    private static Point[] _positions = new Point[] { new Point(), new Point(), new Point(), new Point() } ;
}
