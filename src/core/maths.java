package core;

public class maths
{
    /**
     * This constant change degrees to radians : <pre>Math.PI/180</pre>
     */
    public static double DEG2RAD = Math.PI / 180 ;

    /**
     * This constant defines the radius of the earth in meter : <pre>6371 km</pre>
     */
    public static int EARTH_RADIUS_IN_METERS = 6371000 ;

    /**
     * Represents the smallest positive Single value greater than zero, <pre>EPSILON=0.000000001</pre>
     */
    public static float EPSILON = 0.000000001f;

    /**
     * This constant is the Euler-Mascheroni constant (lambda or C)
     */
    public static float LAMBDA = 0.57721566490143f;

    /**
     * This constant change mile distance to meter : <pre>1 mile = 1609 m</pre>
     */
    public static int MILE_TO_METER = 1609 ;

    /**
     * This constant change radians to degrees : <code>180/Math.PI</code>
     */
    public static double RAD2DEG = 180 / Math.PI ;

    /**
     * Returns the angle in degrees between 2 points with this coordinates passed in argument.
     * @param x1 The x coordinate of the first point.
     * @param y1 The y coordinate of the first point.
     * @param x2 The x coordinate of the second point.
     * @param y2 The y coordinate of the second point.
     * @return the angle in degrees between 2 points with this coordinates passed in argument.
     */
    public static double angleOfLine( double x1 , double y1 , double x2 , double y2 )
    {
        return Math.atan2( (double)(y2 - y1) , (double)(x2 - x1) ) * RAD2DEG ;
    }

    /**
     * Calculates the initial bearing (sometimes referred to as forward azimuth) which if followed in a straight line along a great-circle arc will take you from the start point to the end point (in degrees).
     * <br>
     * <strong>Example:</strong>
     * <pre>
     * // bearing( [Google H] , [San Francisco, CA] )
     * System.out.println( bearing( 37.422045 , -122.084347 , 37.77493 , -122.419416 ) ) ; // 323.1477743368166
     * </pre>
     * @param latitude1 The first latitude coordinate.
     * @param longitude1 The first longitude coordinate.
     * @param latitude2 The second latitude coordinate.
     * @param longitude2 The second longitude coordinate.
     * @return The bearing in degrees from North.
     */
    public static double bearing( double latitude1, double longitude1, double latitude2, double longitude2 )
    {
        latitude1 = latitude1 * DEG2RAD ;
        latitude2 = latitude2 * DEG2RAD ;

        double dLng = ( longitude2 - longitude1 ) * DEG2RAD ;

        double y = Math.sin( dLng ) * Math.cos( latitude2 ) ;
        double x = Math.cos( latitude1 )*Math.sin( latitude2 ) - Math.sin( latitude1 ) * Math.cos( latitude2 ) * Math.cos( dLng ) ;

        return ((Math.atan2(y, x)) * RAD2DEG + 360) % 360 ;
    }

    /**
     * Bounds a numeric value between 2 numbers.
     * <br>
     * <strong>Example:</strong>
     * <pre>
     * double n ;
     *
     * n = maths.clamp(4, 5, 10) ;
     * System.out.println("n : " + n) ; // 5.0
     *
     * n = maths.clamp(12, 5, 10) ;
     * System.out.println("n : " + n) ; // 10.0
     *
     * n = maths.clamp(6, 5, 10) ;
     * System.out.println("n : " + n) ; // 5.0
     * </pre>
     * @param value The value to clamp.
     * @param min The min value of the range.
     * @param max The max value of the range.
     * @return A bound numeric value between 2 numbers.
     */
    public static double clamp( double value , double min , double max )
    {
        return Math.max( Math.min( value, max ), min ) ;
    }

    /**
     * Bounds a numeric value between 2 float numbers.
     * @param value The value to clamp.
     * @param min The min value of the range.
     * @param max The max value of the range.
     * @return A bound numeric value between 2 numbers.
     */
    public static float clamp( float value , float min , float max )
    {
        return Math.max( Math.min( value, max ), min ) ;
    }

    /**
     * Bounds a numeric value between 2 int numbers.
     * @param value The value to clamp.
     * @param min The min value of the range.
     * @param max The max value of the range.
     * @return A bound numeric value between 2 numbers.
     */
    public static int clamp( int value , int min , int max )
    {
        return Math.max( Math.min( value, max ), min ) ;
    }

    /**
     * Converts degrees to radians.
     * @param angle A value, in degrees, to convert to radians.
     * @return an angle in radians.
     */
    public static double degreesToRadians( double angle )
    {
        return angle * DEG2RAD ;
    }

    /**
     * With a number value and a range this method returns the actual value for the interpolated value in that range.
     * <br>
     * <strong>Example:</strong>
     * <pre>
     * System.out.println( maths.interpolate( 0.5f , 0 , 100 ) ) ; // 50
     * </pre>
     * @param value The normal number value to interpolate (value between min and max).
     * @param min The minimum value of the interpolation.
     * @param max The maximum value of the interpolation.
     * @return The actual value for the interpolated value in that range.

     */
    public static float interpolate(  float value , float min , float max  )
    {
        return min + (max - min) * value ;
    }

    /**
     * Indicates if two float (Number) values are equal, give or take <code>epsilon</code>.
     * @param a the first number to evaluates.
     * @param b the second number to evaluates.
     * @return A <code>true</code> value if the two numbers are equivalent.
     */
    public static boolean isEquivalent( double a , double b)
    {
        return isEquivalent( a , b , 0.0001f );
    }

    /**
     * Indicates if two float (Number) values are equal, give or take <code>epsilon</code>.
     * @param a the first number to evaluates.
     * @param b the second number to evaluates.
     * @param epsilon The epsilon value to evaluate the two numbers.
     * @return A <code>true</code> value if the two numbers are equivalent.
     */
    public static boolean isEquivalent( double a , double b, float epsilon)
    {
        return ((a - epsilon) < b) && ((a + epsilon) > b);
    }

    /**
     * Takes a value in a given range (minimum1, maximum1) and finds the corresponding value in the next range(minimum2, maximum2).
     * <br>
     * <strong>Example:</strong>
     * <pre>
     * System.out.println( maths.map( 10,  0, 100, 20, 80  ) ) ; // 26
     * System.out.println( maths.map( 26, 20,  80,  0, 100 ) ) ; // 10
     * </pre>
     * @param value The number value to map.
     * @param min1 The minimum value of the first range of the value.
     * @param max1 The maximum value of the first range of the value.
     * @param min2 The minimum value of the second range of the value.
     * @param max2 The maximum value of the second range of the value.
     * @return A value in a given range (minimum1, maximum1) and finds the corresponding value in the next range(minimum2, maximum2).
     */
    public static float map( float value , float min1 , float max1 , float min2 , float max2 )
    {
        return interpolate( normalize( value, min1, max1 ), min2, max2 );
    }

    /**
     * Takes a value within a given range and converts it to a number between 0 and 1.
     * Actually it can be outside that range if the original value is outside its range.
     * <br>
     * <strong>Example:</strong>
     * <pre>
     * System.out.println( maths.normalize( 10, 0 , 100 ) ) ; // 0.1
     * </pre>
     * @param value The normal number value to interpolate (value between min and max).
     * @param min The minimum value of the interpolation.
     * @param max The maximum value of the interpolation.
     * @return The normalized value between 0 and 1.
     */
    public static float normalize( float value , float min , float max )
    {
        return (value - min) / (max - min) ;
    }

    /**
     * Converts radians to degrees.
     * @param angle A value, in radians, to convert to degrees.
     * @return an angle in degrees.
     */
    public static double radiansToDegrees( double angle )
    {
        return angle * RAD2DEG ;
    }

    /**
     * Rounds and returns a number by a count of floating points.
     * <br>
     * <strong>Example:</strong>
     * <pre>
     * float n ;
     *
     * n = maths.round(4.572525153, 2) ;
     * System.out.println ("n : " + n) ; // 4.57
     *
     * n = core.maths.round(4.572525153, -1) ;
     * System.out.println ("n : " + n) ; // 5.0
     * </pre>
     * @param value The value number to round.
     * @param floatCount The count of number after the point (optional).
     * @return The round of a number by a count of floating points.
     */
    public static float round( double value , int floatCount )
    {
        float r  = 1 ;
        int i  = - 1 ;
        while (++ i < floatCount)
        {
            r *= 10 ;
        }
        return Math.round( value * r ) / r  ;
    }

    public static float round( double n )
    {
        return round( n , 0 ) ;
    }

    /**
     * Works like maths.lerp, but has ease-in and ease-out of the values.
     * <br>
     * <strong>Example:</strong>
     * <pre>
     * System.out.println( maths.smooth( 0 , 1 , 0.5f ) ) ; // 50
     * </pre>
     * @param start the beginning value.
     * @param end The ending value.
     * @param amount The amount to interpolate between the two values where 0.0 equal to the first point, 0.1 is very near the first point, 0.5 is half-way in between, etc.
     * @return The interpolated value between two numbers at a specific increment.
     */
    public static double smooth( double start , double end , double amount )
    {
        if ( start == end )
        {
            return start ;
        }

        amount = clamp( amount , start , end );

        double v1 = (amount - start) / (end - start);
        double v2 = (amount - start) / (end - start);

        return -2 * v1 * v1 * v1 + 3 * v2 * v2;
    }
}