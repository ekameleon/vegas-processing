package core.easings;

/**
 * The basic interface to implements all easing equations.
 */
public interface Easing
{
    /**
     * Calculates the easing changing value with a specific time interval.
     * @param t The current time, between 0 and duration inclusive.
     * @param b The initial value of the animation property.
     * @param c The total change in the animation property.
     * @param d The duration of the motion.
     * @return The value of the interpolated property at the specified time.
     */
    float calculate( float t ,float b , float c, float d ) ;
}
