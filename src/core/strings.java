package core;

import java.text.MessageFormat;

public class strings
{
    /**
     * The empty String <code>""</code>.
     */
    public static final String EMPTY = "";

    /**
     * Represents a failed index search.
     */
    public static final int INDEX_NOT_FOUND = -1;

    /**
     * Quick and fast format of a string using indexed parameters only.
     * <p>Usage :</p>
     * <ul>
     * <li><code>String fastformat( String pattern, Object... args )</code></li>
     * <li><code>String fastformat( String pattern, [arg0:*,arg1:*,arg2:*, ...] )</code></li>
     * </ul>
     *
     * <p>Basic usage</p>
     * <code>
     * import core.strings;
     *
     * System.out.println( strings.fastformat( "hello {0}", "world" ) );
     * //output: "hello world"
     *
     * System.out.println( strings.fastformat( "hello {0} {1} {2}", [ "the", "big", "world" ] ) );
     * //output: "hello the big world"
     * </code>
     * @param pattern The expression to transform.
     * @param args The elements to inject in the expression pattern.
     * @return the formatted string expression.
     */
    public static String fastformat( String pattern , Object... args )
    {
        if( (pattern == null) || (pattern == "") )
        {
            return "" ;
        }
        return new MessageFormat( pattern ).format( args );
    }

    /**
     * Reports the index of the first occurrence in this instance of any character in a specified array of Unicode characters
     * <p>Example: </p>
     * <pre>
     * println(strings.indexOfAny("zzabyycdxx", "za")) ; // 0
     * println(strings.indexOfAny("zzabyycdxx", "by")) ; // 3
     * println(strings.indexOfAny("aba","z")) ; // -1
     * </pre>
     * @return the index of the first occurrence in this instance of any character in a specified array of Unicode characters.
     * @param source The String expression to check.
     * @param anyOf An array of chars.
     */
    public static int indexOfAny( String source, char[] anyOf )
    {
        if (isEmpty(source) || anyOf.length == 0)
        {
            return INDEX_NOT_FOUND;
        }

        int csLen = source.length();
        int csLast = csLen - 1;

        int searchLen = anyOf.length;
        int searchLast = searchLen - 1;

        for ( int i = 0 ; i < csLen ; i++ )
        {
            char ch = source.charAt(i);
            for (int j = 0; j < searchLen; j++)
            {
                if ( anyOf[j] == ch )
                {
                    if ( i < csLast && j < searchLast && chars.isHighSurrogate(ch) )
                    {
                        if (anyOf[j + 1] == source.charAt(i + 1))
                        {
                            return i;
                        }
                    }
                    else
                    {
                        return i;
                    }
                }
            }
        }
        return INDEX_NOT_FOUND;
    }

    /**
     * <p>Search a String to find the first index of any
     * character in the given set of characters.</p>
     *
     * <p>A <code>null</code> String will return <code>-1</code>.
     * A <code>null</code> search string will return <code>-1</code>.</p>
     *
     * <pre>
     * strings.indexOfAny("zzabyycdxx", "za") = 0
     * strings.indexOfAny("zzabyycdxx", "by") = 3
     * strings.indexOfAny("aba","z")          = -1
     * </pre>
     *
     * @param source  the String to check, may be null
     * @param searchChars  the chars to search for, may be null
     * @return the index of any of the chars, -1 if no match or null input
     */
    public static int indexOfAny( String source , String searchChars )
    {
        if (isEmpty(source) || isEmpty(searchChars))
        {
            return INDEX_NOT_FOUND ;
        }
        return indexOfAny( source, searchChars.toCharArray() );
    }

    /**
     * <p>Checks if a String is empty ("") or null.</p>
     *
     * <pre>
     * strings.isEmpty(null)      = true
     * strings.isEmpty("")        = true
     * strings.isEmpty(" ")       = false
     * strings.isEmpty("foo")     = false
     * strings.isEmpty("  foo  ") = false
     * </pre>
     *
     * @param value the String to check, may be null.
     * @return <code>true</code> if the String is empty or null.
     */
    public static boolean isEmpty( String value )
    {
        return value == null || value.length() == 0;
    }
}