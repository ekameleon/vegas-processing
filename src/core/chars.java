package core;

public class chars
{
    /**
     * <p>Checks whether the character is ASCII 7 bit numeric.</p>
     *
     * <pre>
     *   chars.isAsciiAlphanumeric('a')  = true
     *   chars.isAsciiAlphanumeric('A')  = true
     *   chars.isAsciiAlphanumeric('3')  = true
     *   chars.isAsciiAlphanumeric('-')  = false
     *   chars.isAsciiAlphanumeric('\n') = false
     *   chars.isAsciiAlphanumeric('&copy;') = false
     * </pre>
     *
     * @param ch the character to check
     * @return true if between 48 and 57 or 65 and 90 or 97 and 122 inclusive
     */
    public static boolean isAsciiAlphanumeric( char ch )
    {
        return (ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z') || (ch >= '0' && ch <= '9');
    }

    /**
     * Indicates whether {@code ch} is a high- (or leading-) surrogate code unit that is used for representing supplementary characters in UTF-16 encoding.
     * @param ch the character to test.
     * @return {@code true} if {@code ch} is a high-surrogate code unit;
     *         {@code false} otherwise.
     */
    public static boolean isHighSurrogate(char ch)
    {
        return ( '\uD800' <= ch && '\uDBFF' >= ch );
    }
}