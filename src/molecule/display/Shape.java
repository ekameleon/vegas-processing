package molecule.display;

import core.maths;
import graphics.geom.Rectangle;
import processing.core.PShape;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * A Shape display geometry into a variable of type PShape.
 */
public class Shape extends Mesh
{
    /**
     * Creates an empty Shape instance.
     */
    public Shape()
    {
        super() ;
        _shape = _applet.createShape();
        _bounds.setTo(0, 0, _shape.getWidth() , _shape.getHeight() );
        setSize( _shape.width , _shape.height ) ;
        render();
    }

//    /**
//     * Creates a new emoty Shape instance.
//     */
//    public Shape( int type )
//    {
//        super() ;
//        _shape = _applet.createShape( type );
//        _bounds.setTo(0, 0, _shape.getWidth() , _shape.getHeight() );
//        setSize( _shape.width , _shape.height ) ;
//        render();
//    }

//    /**
//     * Creates a new Shape instance.
//     */
//    public Shape( int kind, float... params )
//    {
//        super() ;
//
//        try
//        {
//            Class clazz = _applet.getClass() ;
//            Method method = clazz.getDeclaredMethod("createShape", Integer.class ) ;
//
//            ArrayList<Object> arguments = new ArrayList<>() ;
//
//            arguments.add( kind ) ;
//            arguments.addAll( Arrays.asList(params) ) ;
//
//            Object[] args = arguments.toArray() ;
//
//            try
//            {
//                _shape = (PShape) method.invoke( _applet , new Object[]{ args } );
//            }
//            catch (IllegalAccessException e)
//            {
//                e.printStackTrace();
//            }
//            catch (InvocationTargetException e)
//            {
//                e.printStackTrace();
//            }
//        }
//        catch (NoSuchMethodException e)
//        {
//            e.printStackTrace();
//        }
//
//        _bounds.setTo(0, 0, _shape.getWidth() , _shape.getHeight() );
//        setSize( _shape.width , _shape.height ) ;
//        render();
//    }

    /**
     * Creates a new Shape instance.
     * @param filename Loads geometry into a variable of type PShape.
     * SVG and OBJ files may be loaded. To load correctly, the file must be located in the data directory of the current sketch. In most cases, loadShape() should be used inside setup() because loading shapes inside draw() will reduce the speed of a sketch.
     * Alternatively, the file maybe be loaded from anywhere on the local computer using an absolute path (something that starts with / on Unix and Linux, or a drive letter on Windows), or the filename parameter can be a URL for a file found on a network.
     */
    public Shape( String filename )
    {
        super() ;
        _shape = _applet.loadShape( filename );
        _bounds = new Rectangle();
        setSize( _shape.width , _shape.height ) ;
        render();
    }

    /**
     * The color value of the quad object.
     * @return The color value of the quad object.
     */
    public int getColor()
    {
        return _color;
    }

    /**
     * Set the color value of the quad object.
     * @param value the color value (a number between 0x000000 and 0xFFFFFF).
     * @return the current display object reference.
     */
    public DisplayObject setColor( int value )
    {
        _color = value ;
        _useColor = true ;
        return this ;
    }

    /**
     * Returns the internal PShape reference of the object.
     * @return the internal PShape reference of the object.
     */
    public PShape getShape()
    {
        return _shape ;
    }

    /**
     * Indicates if the shape is filled with a specific color.
     * @return A boolean to indicates if the color is used.
     */
    public boolean getUseColor( boolean flag )
    {
        return _useColor ;
    }

    /**
     * Indicates if the shape is filled with a specific color.
     * @param flag Indicates if the shape use the color.
     * @return the current display object reference.
     */
    public DisplayObject setUseColor( boolean flag )
    {
        _useColor = flag ;
        return this ;
    }

    /**
     * Indicates if the display object is visible.
     * @return A <code>true</code> value if the display object is visible.
     */
    public boolean getVisible()
    {
        return _visible;
    }

    /**
     * Sets if the display object is visible.
     * @param value The flag to indicates if the display object is visible or not.
     * @return The current display object reference.
     */
    public DisplayObject setVisible( boolean value )
    {
        _shape.setVisible( value );
        _visible = value ;
        return this ;
    }

    public void render()
    {
        if( !_visible )
        {
            return ;
        }
        if( _applet != null && _shape != null )
        {
            _shape.resetMatrix() ;

            _bounds.setSize( _shape.width , _shape.height );

            if( _useColor )
            {
                _shape.disableStyle();
                _applet.fill(_color, _alpha * 255 );
                _applet.noStroke();
            }

            if( _rotation != 0 )
            {
                _shape.rotate( (float) (_rotation * maths.DEG2RAD) );
            }

            _applet.shape
            (
                _shape , (float)_bounds.x , (float)_bounds.y ,
                (float) _bounds.width * _scaleX ,
                (float) _bounds.height * _scaleY
            );
        }
    }

    private int _color = -1 ;
    private boolean _useColor = false ;
    private PShape _shape ;
}
