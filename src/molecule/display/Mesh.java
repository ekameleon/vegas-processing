package molecule.display;

import graphics.geom.Matrix;
import graphics.geom.Point;
import graphics.geom.Rectangle;
import graphics.geom.Rectangles;

/**
 * A Mesh object.
 */
public abstract class Mesh extends DisplayObject
{
    @Override
    public Rectangle getBounds( DisplayObject targetSpace, Rectangle out )
    {
        if (out == null)
        {
            out = new Rectangle();
        }

        if ( targetSpace == this )
        {
            out.copyFrom( _bounds );
        }
        else if (targetSpace == _parent && !isRotated())
        {
            float scaleX = getScaleX();
            float scaleY = getScaleY();

            out.setTo
            (
                getX() - getPivotX() * scaleX , getY() - getPivotY() * scaleY ,
                _bounds.width * scaleX     , _bounds.height * scaleY
            );

            if ( scaleX < 0 )
            {
                out.width *= -1 ;
                out.x -= out.width;
            }
            if ( scaleY < 0 )
            {
                out.height *= -1 ;
                out.y -= out.height;
            }
        }
//        else if (is3D() && stage() != null )
//        {
//            stage.getCameraPosition(targetSpace, sPoint3D);
//            getTransformationMatrix3D(targetSpace, sMatrix3D);
//            RectangleUtil.getBoundsProjected(_bounds, sMatrix3D, sPoint3D, out);
//        }
        else
        {
            createTransformationMatrix( targetSpace, _matrix );
            Rectangles.getBounds( _bounds , _matrix, out );
        }

        return out;
    }

    @Override
    public Rectangle getBounds(DisplayObject targetSpace)
    {
        return getBounds( targetSpace , null );
    }


    @Override
    public DisplayObject hitTest( Point localPoint )
    {
        if (! _visible || !_touchable || !hitTestMask(localPoint))
        {
            return null;
        }
        else if (_bounds.contains(localPoint))
        {
            return this;
        }
        else
        {
            return null;
        }
    }

    protected static Matrix _matrix = new Matrix();
}
