package molecule.display;

import graphics.geom.Point;

public class Stage extends DisplayObjectContainer
{
    /**
     * Creates a new Stage instance.
     * @param width The width of the Stage.
     * @param height The height of the Stage.
     * @param color The color of the Stage.
     */
    public Stage( float width , float height , int color )
    {
        super() ;
        _width  = width ;
        _height = height ;
        _color  = color ;
    }

    public Stage( float width , float height )
    {
        this( width , height , 0 ) ;
    }

    public Stage( float width )
    {
        this( width , 0 , 0 ) ;
    }

    public Stage()
    {
        this( 0 , 0 , 0 ) ;
    }

    /**
     * The color value of the stage background.
     * @return The color value.
     */
    public int getColor()
    {
        return _color;
    }

    /**
     * Set the color value of the stage background.
     * @param value the color value (a number between 0x000000 and 0xFFFFFF).
     * @return the current display stage reference.
     */
    public DisplayObject setColor( int value )
    {
        _color = value ;
        return this ;
    }

    /**
     * Returns the object that is found topmost beneath a point in stage coordinates, or the stage itself if nothing else is found.
     */
    public DisplayObject hitTest( Point localPoint )
    {
        if (!_visible || !_touchable)
        {
            return null;
        }
        if (localPoint.x < 0 || localPoint.x > _width || localPoint.y < 0 || localPoint.y > _height)
        {
            return null;
        }
        DisplayObject target = super.hitTest(localPoint);
        return target != null ? target : this ;
    }

    @Override
    public void render()
    {
        if( _applet != null )
        {
            _applet.background( _color ) ;
        }
        for (DisplayObject child : _children)
        {
            if (child._hasVisibleArea) {
                child.render();
            }
        }
    }

    private int _color = 0x2C3E50 ;
}
