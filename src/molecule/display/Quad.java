package molecule.display;

import core.maths;
import graphics.geom.Matrix;
import graphics.geom.Point;
import graphics.geom.Rectangle;
import graphics.geom.Rectangles;
import processing.core.PGraphics;

/**
 * A Quad represents a colored and/or textured rectangle.
 */
public class Quad extends Mesh
{
    /**
     * Creates a new Quad instance.
     * @param width The width value of the quad.
     * @param height The height value of the quad.
     * @param color The color value of the quad.
     * @param alpha The alpha value of the quad.
     */
    public Quad( double width , double height , int color , float alpha )
    {
        super() ;

        _bounds = new Rectangle(0, 0, width, height);
        _width  = (float) width ;
        _height = (float) height ;
        _alpha  = maths.clamp(alpha,0,1) ;
        _color  = color ;

        _graphics = _applet.createGraphics( _applet.width , _applet.height );

        render();
    }

    protected PGraphics _graphics ;

    /**
     * Creates a new Quad instance.
     * @param width The width value of the quad.
     * @param height The height value of the quad.
     * @param color The color value of the quad.
     */
    public Quad( double width , double height , int color )
    {
        this( width , height , color , 1 ) ;
    }

    /**
     * Creates a new Quad instance.
     * @param width The width value of the quad.
     * @param height The height value of the quad.
     */
    public Quad( double width , double height )
    {
        this( width , height , 0xFFFFFF , 1 ) ;
    }

    /**
     * The color value of the quad object.
     * @return The color value of the quad object.
     */
    public int getColor()
    {
        return _color;
    }

    /**
     * Set the color value of the quad object.
     * @param value the color value (a number between 0x000000 and 0xFFFFFF).
     * @return the current display object reference.
     */
    public DisplayObject setColor( int value )
    {
        _color = value ;
        return this ;
    }

    /**
     * Set height of the display object.
     * @param value The height value of the object.
     * @return The current display object reference.
     */
    @Override
    public DisplayObject setHeight( float value )
    {
        _height = value;
        _bounds.height = value ;
        return this ;
    }

    /**
     * Set width of the display object.
     * @param value The width value of the object.
     * @return The current display object reference.
     */
    @Override
    public DisplayObject setWidth( float value )
    {
        _width = value;
        _bounds.width = value ;
        return this ;
    }

    /**
     * Set the x position of the display object.
     * @param value the x position of the display object.
     * @return the current display object reference.
     */
    public DisplayObject setX( float value )
    {
        if( _x != value )
        {
            _bounds.x = _x = value ;
            setOrientationChanged();
        }
        return this ;
    }

    /**
     * Set the y position of the display object.
     * @param value the y position of the display object.
     * @return the current display object reference.
     */
    public DisplayObject setY( float value )
    {
        if( _y != value )
        {
            _bounds.y = _y = value;
            setOrientationChanged();
        }
        return this ;
    }

    /**
     * Set the x and y positions of the display object.
     * @param x the x position of the display object.
     * @param y the x position of the display object.
     * @return the current display object reference.
     */
    public DisplayObject setTo( float x , float y )
    {
        _bounds.x =_x = x ;
        _bounds.y =_y = y ;
        return this ;
    }

    /**
     * Set the x and y positions of the display object with the 0,0 coordinates.
     * @return the current display object reference.
     */
    public DisplayObject setTo()
    {
        _bounds.x = _x = 0 ;
        _bounds.y = _y = 0 ;
        return this ;
    }

    public void render()
    {
        if( _applet != null && _graphics != null )
        {
            Rectangle bounds = getBounds(_parent , __rectangle) ;

            _graphics.beginDraw();

            _graphics.pushMatrix();

            _graphics.clear();

            _graphics.noStroke();
            _graphics.fill(_color);

            if( isRotated() )
            {
                _graphics.resetMatrix();
                _graphics.translate( (float)(bounds.x) , (float)(bounds.y) );

//                _graphics.translate( (float)bounds.x , (float)bounds.y );

//                getTransformationMatrix() ;
//                _graphics.resetMatrix();
//                _graphics.applyMatrix
//                (
//                    (float) _transformationMatrix.a,
//                    (float) _transformationMatrix.c,
//                    (float) _transformationMatrix.tx,
//                    (float) _transformationMatrix.b,
//                    (float) _transformationMatrix.d,
//                    (float) _transformationMatrix.ty
//                );
            }
            else
            {
                _graphics.translate( (float)bounds.x , (float)bounds.y );

            }

            _graphics.rect(0 ,0,(float) bounds.width, (float) bounds.height );
            _graphics.scale( _scaleX , _scaleY ) ;


            _graphics.popMatrix();

            _graphics.endDraw();

            _applet.image(_graphics,0,0);
        }
    }

    protected int _color ;
}
