package molecule.display;

import core.maths;
import graphics.Align;
import graphics.geom.Matrix;
import graphics.geom.Matrixs;
import graphics.geom.Point;
import graphics.geom.Rectangle;
import molecule.Molecule;
import processing.core.PApplet;
import system.events.EventDispatcher;

import java.util.ArrayList;

/**
 * The DisplayObject class is the base class for all objects that can be placed on the display list.
 * The display list manages all objects displayed in the runtimes.
 * Use the DisplayObjectContainer class to arrange the display objects in the display list.
 * DisplayObjectContainer objects can have child display objects, while other display objects, such as Shape and TextField objects, are "leaf" nodes that have only parents and siblings, no children.
 */
public abstract class DisplayObject extends EventDispatcher
{
    /**
     * Creates a new DisplayObject instance.
     */
    public DisplayObject()
    {
        super() ;

        if( _applet == null )
        {
            _applet = Molecule.applet();
        }

        _x = _y = _pivotX = _pivotY = _rotation = _skewX = _skewY = 0.0f ;

        _scaleX = _scaleY = _alpha = 1.0f ;

        _visible = _hasVisibleArea = _touchable = true ;

        _transformationMatrix = new Matrix();
    }

    // -----------

    /**
     * The topmost object in the display tree the object is part of.
     * @return The topmost object in the display tree the object is part of.
     */
    public DisplayObject base()
    {
        DisplayObject currentObject = this;
        while ( currentObject._parent != null )
        {
            currentObject = currentObject._parent;
        }
        return currentObject;
    }

    /**
     * The display object container that contains this display object.
     * @return The parent DisplayObject reference of the current display object.
     */
    public DisplayObjectContainer parent()
    {
        return _parent;
    }

    /**
     * The root object the display object is connected to (i.e. an instance of the class that was passed to the Molecule constructor),
     * or null if the object is not connected to the stage.
     * @return The root DisplayObject reference.
     */
    public DisplayObject root()
    {
        DisplayObject currentObject = this;
        while( currentObject._parent != null )
        {
            if ( currentObject._parent instanceof Stage )
            {
                return currentObject ;
            }
            else
            {
                currentObject = currentObject._parent ;
            }
        }
        return null;
    }

    /**
     * The stage the display object is connected to, or null if it is not connected to the stage.
     * @return The Stage reference.
     */
    public Stage stage()
    {
        return (Stage) this.base() ;
    }

    // ----------- Getters / Setters

    /**
     * The alpha component value of the display object.
     * @return The alpha component value of the display object.
     */
    public float getAlpha()
    {
        return _alpha;
    }

    /**
     * Sets the alpha component value of the display object.
     * @param value The alpha value.
     * @return The current display object reference.
     */
    public DisplayObject setAlpha( float value )
    {
        _alpha = maths.clamp( value ,0.0f , 1.0f ) ;
        return this ;
    }

    /**
     * The height of the display object.
     * @return The height of the display object.
     */
    public float getHeight()
    {
        return (float)getBounds(_parent, __rectangle).height ;
    }

    /**
     * Set height of the display object.
     * @param value The height value of the object.
     * @return The current display object reference.
     */
    public DisplayObject setHeight( float value )
    {
        double actualHeight;
        boolean scaleIsNaN = _scaleY!= _scaleY ;

        if (_scaleY == 0.0 || scaleIsNaN)
        {
            _scaleY = 1.0f;
            actualHeight = getHeight();
        }
        else
        {
            actualHeight = Math.abs( getHeight() / _scaleY );
        }

        if ( actualHeight!= 0 )
        {
            _scaleY = (float)(value / actualHeight) ;
        }

        return this ;
    }

    /**
     * The name of the display object.
     * @return The name of the display object.
     */
    public String getName()
    {
        return _name;
    }

    /**
     * Sets the name of the display object.
     * @param value The name expression of the object.
     * @return The current DisplayObject reference.
     */
    public DisplayObject setName( String value )
    {
        this._name = value ;
        return this ;
    }

    /**
     * The pivotX component value of the display object.
     * @return The pivotX component value of the display object.
     */
    public float getPivotX()
    {
        return _pivotX;
    }

    /**
     * Sets the pivotX component value of the display object.
     * @param value The pivotX value of the object.
     * @return The current DisplayObject reference.
     */
    public DisplayObject setPivotX( float value )
    {
        if( _pivotX != value )
        {
            _pivotX = value;
            setOrientationChanged();
        }
        return this ;
    }

    /**
     * The pivotY component value of the display object.
     * @return The pivotY component value of the display object.
     */
    public float getPivotY()
    {
        return _pivotY;
    }

    /**
     * Sets the pivotY component value of the display object.
     * @param value The pivotY value of the object.
     * @return The current DisplayObject reference.
     */
    public DisplayObject setPivotY( float value )
    {
        if( _pivotY != value )
        {
            _pivotY = value;
            setOrientationChanged();
        }
        return this ;
    }

    /**
     * The rotation component value of the display object.
     * @return The rotation component value of the display object.
     */
    public float getRotation()
    {
        return _rotation;
    }

    /**
     * Sets the rotation component value of the display object.
     * @param value The rotation value of the object.
     * @return The current DisplayObject reference.
     */
    public DisplayObject setRotation( float value )
    {
        if( _rotation != value )
        {
            _rotation = value;
            setOrientationChanged();
        }
        return this ;
    }

    /**
     * The scaleX component value of the display object.
     * @return The scaleX component value of the display object.
     */
    public float getScaleX()
    {
        return _scaleX;
    }

    /**
     * Sets the scaleX component value of the display object.
     * @param value The scaleX value of the object.
     * @return The current DisplayObject reference.
     */
    public DisplayObject setScaleX( float value )
    {
        if( _scaleX != value )
        {
            _scaleX = value;
            setOrientationChanged();
        }
        return this ;
    }

    /**
     * The scaleY component value of the display object.
     * @return The scaleY component value of the display object.
     */
    public float getScaleY()
    {
        return _scaleY;
    }

    /**
     * Sets the scaleY component value of the display object.
     * @param value The scaleY value of the object.
     * @return The current DisplayObject reference.
     */
    public DisplayObject setScaleY( float value )
    {
        if( _scaleY != value )
        {
            _scaleY = value;
            setOrientationChanged();
        }
        return this ;
    }

    public float getSkewX()
    {
        return _skewX;
    }

    public DisplayObject setSkewX( float value )
    {
        if( _skewX != value )
        {
            _skewX = value ;
            setOrientationChanged();
        }
        return this ;
    }

    public float getSkewY()
    {
        return _skewY;
    }

    public DisplayObject setSkewY( float value )
    {
        if( _skewY != value )
        {
            _skewY = value;
            setOrientationChanged();
        }
        return this ;
    }

    /**
     * The transformation matrix of the object relative to its parent.
     * @return The transformation matrix of the object relative to its parent.
     */
    public Matrix getTransformationMatrix()
    {
        if ( _orientationChanged )
        {
            _orientationChanged = false;

            double angle = _rotation * maths.DEG2RAD;

            _transformationMatrix.identity();

            if ( _skewX == 0.0 && _skewY == 0.0 )
            {
                if (_rotation == 0.0)
                {
                    _transformationMatrix.setTo
                    (
                        _scaleX, 0.0, 0.0, _scaleY, _x - _pivotX * _scaleX , _y - _pivotY * _scaleY
                    );
                }
                else
                {
                    double cos = Math.cos( angle );
                    double sin = Math.sin( angle );

                    double a   = _scaleX *  cos;
                    double b   = _scaleX *  sin;
                    double c   = _scaleY * -sin;
                    double d   = _scaleY *  cos;
                    double tx  = _x - _pivotX * a - _pivotY * c;
                    double ty  = _y - _pivotX * b - _pivotY * d;

                    _transformationMatrix.setTo(a, b, c, d, tx, ty);
                }
            }
            else
            {
                _transformationMatrix.scale(_scaleX, _scaleY);

                Matrixs.skew(_transformationMatrix, _skewX, _skewY);

                _transformationMatrix.rotate(angle);
                _transformationMatrix.translate(_x, _y);

                if (_pivotX != 0.0 || _pivotY != 0.0)
                {
                    _transformationMatrix.tx = _x - _transformationMatrix.a * _pivotX - _transformationMatrix.c * _pivotY;
                    _transformationMatrix.ty = _y - _transformationMatrix.b * _pivotX - _transformationMatrix.d * _pivotY;
                }
            }
        }

        return _transformationMatrix;
    }

    /**
     * Set the transform matrix of the object.
     * @param matrix The matrix reference.
     */
    public void setTransformationMatrix( Matrix matrix )
    {
        double PI_Q = Math.PI / 4 ;

        setRequiresRedraw() ;

        _orientationChanged = false;
        _transformationMatrix.copyFrom(matrix);
        _pivotX = _pivotY = 0;

        _x = (float) matrix.tx;
        _y = (float) matrix.ty;

        _skewX = (float) Math.atan(-matrix.c / matrix.d);
        _skewY = (float) Math.atan( matrix.b / matrix.a);

        if (_skewX != _skewX)
        {
            _skewX = 0;
        }
        if (_skewY != _skewY)
        {
            _skewY = 0;
        }

        _scaleY = (float) ((_skewX > -PI_Q && _skewX < PI_Q) ?  matrix.d / Math.cos(_skewX) : -matrix.c / Math.sin(_skewX));
        _scaleX = (float) ((_skewY > -PI_Q && _skewY < PI_Q) ?  matrix.a / Math.cos(_skewY) :  matrix.b / Math.sin(_skewY));

        if ( maths.isEquivalent(_skewX, _skewY))
        {
            _rotation = _skewX;
            _skewX = _skewY = 0;
        }
        else
        {
            _rotation = 0;
        }
    }

    /**
     * Indicates if the display object is visible.
     * @return A <code>true</code> value if the display object is visible.
     */
    public boolean getVisible()
    {
        return _visible;
    }

    /**
     * Sets if the display object is visible.
     * @param value The flag to indicates if the display object is visible or not.
     * @return The current display object reference.
     */
    public DisplayObject setVisible( boolean value )
    {
        _visible = value ;
        return this ;
    }

    /**
     * The width of the display object.
     * @return The width of the display object.
     */
    public float getWidth()
    {
        return (float)getBounds(_parent, __rectangle).width ;
    }

    /**
     * Set width of the display object.
     * @param value The width value of the object.
     * @return The current display object reference.
     */
    public DisplayObject setWidth( float value )
    {
        double actualWidth ;
        boolean scaleIsNaN = _scaleX != _scaleX ;

        if (_scaleX == 0.0 || scaleIsNaN)
        {
            _scaleX = 1.0f;
            actualWidth = getWidth();
        }
        else
        {
            actualWidth = Math.abs( getWidth() / _scaleX );
        }

        if (actualWidth != 0 )
        {
            _scaleX = (float)(value / actualWidth) ;
        }
        return this ;
    }

    /**
     * The x position of the display object.
     * @return The x position of the display object.
     */
    public float getX()
    {
        return _x;
    }

    /**
     * Set the x position of the display object.
     * @param value the x position of the display object.
     * @return the current display object reference.
     */
    public DisplayObject setX( float value )
    {
        if( _x != value )
        {
            _bounds.x = _x = value;
            setOrientationChanged();
        }
        return this ;
    }

    /**
     * The y position of the display object.
     * @return The y position of the display object.
     */
    public float getY()
    {
        return _y;
    }

    /**
     * Set the y position of the display object.
     * @param value the y position of the display object.
     * @return the current display object reference.
     */
    public DisplayObject setY( float value )
    {
        if( _y != value )
        {
            _bounds.y =_y = value;
            setOrientationChanged();
        }
        return this ;
    }

    // ------------------

    /**
     *  Moves the pivot point to a certain position within the local coordinate system of the object.
     */
    public void alignPivot( Align horizontalAlign , Align verticalAlign )
    {
        Rectangle bounds = getBounds(this, __rectangle);

        setOrientationChanged();

        if (horizontalAlign == Align.LEFT)
        {
            _pivotX = (float) bounds.x ;
        }
        else if (horizontalAlign == Align.RIGHT)
        {
            _pivotX = (float) (bounds.x + bounds.width) ;
        }
        else
        {
            _pivotX = (float) (bounds.x + bounds.width*0.5) ;
        }

        if (verticalAlign == Align.TOP)
        {
            _pivotY = (float) bounds.y ;
        }
        else if (verticalAlign == Align.BOTTOM)
        {
            _pivotY = (float) (bounds.y + bounds.height) ;
        }
        else
        {
            _pivotY = (float) (bounds.y + bounds.height*0.5);
        }
    }

    /**
     *  Creates a matrix that represents the transformation from the local coordinate system to another.
     *  If you pass an <code>out</code>-matrix, the result will be stored in this matrix instead of creating a new object.
     *  @param targetSpace The DisplayObject target space reference.
     *  @return The new transformation matrix.
     */
    public Matrix createTransformationMatrix( DisplayObject targetSpace )
    {
        return createTransformationMatrix( targetSpace , null ) ;
    }

    /**
     *  Creates a matrix that represents the transformation from the local coordinate system to another.
     *  If you pass an <code>out</code>-matrix, the result will be stored in this matrix instead of creating a new object.
     *  @param targetSpace The DisplayObject target space reference.
     *  @param out The optional Matrix target reference.
     *  @return The new transformation matrix.
     */
    public Matrix createTransformationMatrix( DisplayObject targetSpace , Matrix out )
    {
        DisplayObject commonParent ;
        DisplayObject currentObject ;

        if ( out != null )
        {
            out.identity();
        }
        else
        {
            out = new Matrix();
        }

        if ( targetSpace == this )
        {
            return out;
        }
        else if (targetSpace == _parent || (targetSpace == null && _parent == null) )
        {
            out.copyFrom(getTransformationMatrix());
            return out;
        }
        else if (targetSpace == null || targetSpace == base() )
        {
            currentObject = this;
            while (currentObject != targetSpace)
            {
                out.concat( currentObject.getTransformationMatrix() );
                currentObject = currentObject._parent;
            }
            return out;
        }
        else if (targetSpace._parent == this) // optimization
        {
            targetSpace.createTransformationMatrix(this , out );
            out.invert();
            return out;
        }

        // 1. find a common parent of this and the target space

        commonParent = findCommonParent(this, targetSpace);

        // 2. move up from this to common parent

        currentObject = this;
        while (currentObject != commonParent)
        {
            out.concat(currentObject.getTransformationMatrix());
            currentObject = currentObject._parent;
        }

        if (commonParent == targetSpace)
        {
            return out;
        }

        // 3. now move up from target until we reach the common parent

        _matrixHelper.identity();

        currentObject = targetSpace;
        while (currentObject != commonParent)
        {
            _matrixHelper.concat(currentObject.getTransformationMatrix());
            currentObject = currentObject._parent;
        }

        // 4. now combine the two matrices

        _matrixHelper.invert();
        out.concat(_matrixHelper);

        return out;
    }

    /**
     * Disposes all resources of the display object.
     * GPU buffers are released, event listeners are removed, filters and masks are disposed.
     * */
    public void dispose()
    {
        //if (_filter) _filter.dispose();
        if ( _mask != null )
        {
            _mask.dispose();
        }
        //removeEventListener(); // all
        _mask = null;
    }

    /**
     * Returns a rectangle that completely encloses the object as it appears in another coordinate system.
     * If you pass an <code>out</code>-rectangle, the result will be stored in this rectangle instead of creating a new object.
     * @param targetSpace The DisplayObject target space reference.
     * @param out The optional Rectangle target reference.
     * @return The Rectangle bounds.
     */
    public abstract Rectangle getBounds( DisplayObject targetSpace, Rectangle out ) ;

    /**
     * Returns a rectangle that completely encloses the object as it appears in another coordinate system.
     * If you pass an <code>out</code>-rectangle, the result will be stored in this rectangle instead of creating a new object.
     * @param targetSpace The DisplayObject target space reference.
     * @return The Rectangle bounds.
     */
    public abstract Rectangle getBounds( DisplayObject targetSpace ) ;

    /**
     * Returns the object that is found topmost beneath a point in local coordinates, or null if the test fails.
     * Untouchable and invisible objects will cause the test to fail.
     * @param localPoint The local position to test.
     * @return the object that is found topmost beneath a point in local coordinates, or null if the test fails.
     */
    public DisplayObject hitTest( Point localPoint )
    {
        if (!_visible || !_touchable)
        {
            return null;
        }

        if ( (_mask != null) && !hitTestMask(localPoint) )
        {
            return null;
        }

        if ( getBounds(this, __rectangle).contains(localPoint) )
        {
            return this;
        }
        else
        {
            return null;
        }
    }

    /**
     * Checks if a certain point is inside the display object's mask. If there is no mask, this method always returns <code>true</code> (because having no mask is equivalent to having one that's infinitely big).
     * @param localPoint The local position to test.
     * @return A <code>true</code> value if the mask is hit test.
     */
    public boolean hitTestMask( Point localPoint )
    {
        if ( _mask != null )
        {
            if ( _mask.stage() != null )
            {
                createTransformationMatrix(_mask, _matrixHelperAlt);
            }
            else
            {
                _matrixHelperAlt.copyFrom( _mask.getTransformationMatrix() );
                _matrixHelperAlt.invert();
            }

            Point helperPoint = localPoint == sHelperPoint ? new Point() : sHelperPoint;

            Matrixs.transformPoint(_matrixHelperAlt, localPoint, helperPoint);

            return _mask.hitTest(helperPoint) != null;
        }
        else
        {
            return true ;
        }
    }

    /**
     * Transforms a point from global (stage) coordinates to the local coordinate system.
     * If you pass an <code>out</code>-point, the result will be stored in this point instead of creating a new object.
     * @param globalPoint The local point to transform.
     * @param out The point to store the result of the transformation.
     * @return The new transformed Point.
     */
    public Point globalToLocal( Point globalPoint , Point out )
    {
        createTransformationMatrix( base() , _matrixHelperAlt );
        _matrixHelperAlt.invert();
        return Matrixs.transformPoint( _matrixHelperAlt , globalPoint, out );
    }

    /**
     * Transforms a point from global (stage) coordinates to the local coordinate system.
     * @param globalPoint The local point to transform.
     * @return The new transformed Point.
     */
    public Point globalToLocal( Point globalPoint )
    {
        createTransformationMatrix( base() , _matrixHelperAlt );
        _matrixHelperAlt.invert();
        return Matrixs.transformPoint( _matrixHelperAlt , globalPoint, null );
    }

    /**
     * Transforms a point from the local coordinate system to global (stage) coordinates.
     * If you pass an <code>out</code>-point, the result will be stored in this point instead of creating a new object.
     * @param localPoint The local point to transform.
     * @param out The point to store the result of the transformation.
     * @return The new transformed Point.
     */
    public Point localToGlobal( Point localPoint , Point out )
    {
        createTransformationMatrix( base() , _matrixHelperAlt );
        return Matrixs.transformPoint(_matrixHelperAlt, localPoint, out);
    }

    /**
     *  Transforms a point from the local coordinate system to global (stage) coordinates.
     *  @param localPoint The local point to transform.
     *  @return The new transformed Point.
     */
    public Point localToGlobal( Point localPoint )
    {
        createTransformationMatrix( base() , _matrixHelperAlt );
        return Matrixs.transformPoint(_matrixHelperAlt, localPoint , null );
    }

    /**
     * Returns the display object that acts as a mask for the current object.
     * @return the display object that acts as a mask for the current object.
     */
    public DisplayObject getMask()
    {
        return _mask;
    }

    /**
     * Defines the display object that acts as a mask for the current object. Assign <code>null</code> to remove it.
     * @param display The the display object that acts as a mask for the current object.
     */
    public void mask( DisplayObject display )
    {
        if ( _mask != display )
        {
            if (_mask != null )
            {
                _mask._maskee = null;
            }

            if ( display != null )
            {
                display._maskee = this;
                display._hasVisibleArea = false;
            }

            _mask = display;

            setRequiresRedraw();
        }
    }

    /**
     * Removes the object from its parent, if it has one, and optionally disposes it.
     * @param dispose Indicates if the child is disposed.
     */
    public void removeFromParent( boolean dispose )
    {
        if ( _parent != null)
        {
            _parent.removeChild(this , dispose ) ;
        }
        else if (dispose)
        {
            this.dispose() ;
        }
    }

    /**
     * Removes the object from its parent, if it has one, and optionally disposes it.
     */
    public void removeFromParent()
    {
        removeFromParent(false ) ;
    }

    /**
     * Renders the display object with the help of a painter object. Never call this method directly, except from within another render method.
     */
    public abstract void render() ;

    /**
     * Indicates if the object needs to be redrawn in the upcoming frame, i.e. if it has changed its location relative to the stage or some other aspect of its appearance since it was last rendered.
     * @return A boolean to indicates if the if the object needs to be redrawn in the upcoming frame.
     */
    public boolean requiresRedraw()
    {
        return true ;
    }

    /**
     * Forces the object to be redrawn in the next frame. this will prevent the object to be drawn from the render cache.
     */
    public void setRequiresRedraw()
    {
        _hasVisibleArea = _alpha != 0.0 && _visible && _maskee == null && _scaleX != 0.0 && _scaleY != 0.0 ;
    }

    /**
     * Sets the scaleY component value of the display object.
     * @param x The scaleX value of the object.
     * @param y The scaleY value of the object.
     * @return The current DisplayObject reference.
     */
    public DisplayObject setScale( float x , float y )
    {
        if( _scaleX != x )
        {
            _scaleX = x;
        }
        if( _scaleY != y )
        {
            _scaleY = y;
        }
        setOrientationChanged();
        return this ;
    }

    /**
     * Sets the scaleY component value of the display object.
     * @param value The scale x/y value of the object.
     * @return The current DisplayObject reference.
     */
    public DisplayObject setScale( float value )
    {
        if( _scaleX != value )
        {
            _scaleX = value;
        }
        if( _scaleY != value )
        {
            _scaleY = value;
        }
        setOrientationChanged();
        return this ;
    }

    /**
     * Set the size (width/height) of the display object.
     * @param width The width value of the object.
     * @param height The height value of the object.
     * @return The current display object reference.
     */
    public DisplayObject setSize( float width , float height )
    {
        double  actualWidth ;
        double  actualHeight ;
        boolean scaleIsNaN ;

        scaleIsNaN = _scaleX != _scaleX ;

        if (_scaleX == 0.0 || scaleIsNaN)
        {
            _scaleX = 1.0f;
            actualWidth = getWidth();
        }
        else
        {
            actualWidth = Math.abs( getWidth() / _scaleX );
        }

        if (actualWidth != 0 )
        {
            _scaleX = (float)(width / actualWidth) ;
        }

        scaleIsNaN = _scaleY != _scaleY ;

        if (_scaleY == 0.0 || scaleIsNaN)
        {
            _scaleY = 1.0f;
            actualHeight = getHeight();
        }
        else
        {
            actualHeight= Math.abs( getHeight() / _scaleY );
        }

        if (actualHeight != 0 )
        {
            _scaleY = (float)(height / actualHeight) ;
        }

        return this ;
    }

    /**
     * Set the x and y positions of the display object.
     * @param x the x position of the display object.
     * @param y the x position of the display object.
     * @return the current display object reference.
     */
    public DisplayObject setTo( float x , float y )
    {
        _bounds.x =_x = x ;
        _bounds.y =_y = y ;
        setOrientationChanged();
        return this ;
    }

    /**
     * Set the x and y positions of the display object with the 0,0 coordinates.
     * @return the current display object reference.
     */
    public DisplayObject setTo()
    {
        _bounds.x = _x = 0 ;
        _bounds.y = _y = 0 ;
        setOrientationChanged();
        return this ;
    }

    /**
     * Returns the String representation of the object.
     * @return The String representation of the object.
     */
    public String toString()
    {
        return "[" + getClass().getName() + "]" ;
    }

    // -------------------- protected

    protected static PApplet _applet ;

    protected boolean isMask()
    {
        return _maskee != null;
    }

    protected boolean isRotated()
    {
        return _rotation != 0.0 || _skewX != 0.0 || _skewY != 0.0;
    }

    protected void setParent( DisplayObjectContainer value )
    {
        DisplayObject ancestor = value;

        while (ancestor != this && ancestor != null)
        {
            ancestor = ancestor._parent;
        }

        if( ancestor == this )
        {
            throw new IllegalArgumentException("An object cannot be added as a child to itself or one of its children (or children's children, etc.)");
        }
        else
        {
            _parent = value;
        }
    }

    // -------------------- private

    protected float _alpha = 1 ;
    protected float _height ;
    protected float _pivotX;
    protected float _pivotY;
    protected float _rotation;
    protected float _scaleX;
    protected float _scaleY;
    protected float _skewX;
    protected float _skewY;
    protected float _width ;
    protected float _x;
    protected float _y;

    protected Rectangle _bounds = new Rectangle();

    protected DisplayObjectContainer _parent ;

    protected Matrix _transformationMatrix ;

    protected DisplayObject _mask ;
    protected DisplayObject _maskee ;
    protected String _name ;

    boolean _hasVisibleArea ;
    boolean _touchable ;
    boolean _orientationChanged = false;
    boolean _visible ;

    protected static ArrayList<DisplayObject> sAncestors = new ArrayList<DisplayObject>();
    protected static Point sHelperPoint = new Point();

    protected static Rectangle __rectangle = new Rectangle();

    protected static Matrix _matrixHelper = new Matrix();
    protected static Matrix _matrixHelperAlt = new Matrix();

    private static DisplayObject findCommonParent( DisplayObject object1 , DisplayObject object2 )
    {
        DisplayObject currentObject = object1 ;

        while (currentObject != null )
        {
            sAncestors.add( currentObject );
            currentObject = currentObject._parent;
        }

        currentObject = object2;
        while ( currentObject != null && sAncestors.indexOf(currentObject) == -1)
        {
            currentObject = currentObject._parent;
        }

        sAncestors.clear() ;

        if ( currentObject != null )
        {
            return currentObject;
        }
        else
        {
            throw new IllegalArgumentException( "Object not connected to target" );
        }
    }

    protected void setOrientationChanged()
    {
        _orientationChanged = true;
        setRequiresRedraw();
    }
}
