package molecule.display;

import graphics.geom.Matrixs;
import graphics.geom.Point;
import graphics.geom.Rectangle;
import system.events.Event;

import java.util.ArrayList;

public abstract class DisplayObjectContainer extends DisplayObject
{
    /**
     * Creates a new DisplayObjectContainer instance.
     */
    public DisplayObjectContainer()
    {
        super() ;
        _children = new ArrayList<>();
    }

    /**
     * Disposes the resources of all children.
     */
    @Override
    public void dispose()
    {
        int len = _children.size()-1 ;
        for ( int i = len ; i>=0 ; --i )
        {
            _children.get(i).dispose();
        }
        super.dispose();
    }

    /**
     * Adds a child to the container. It will be at the frontmost position.
     * @param child The DisplayObject to add at the end of the display list.
     * @return The new DisplayObject added in the Display list.
     */
    public DisplayObject addChild( DisplayObject child )
    {
        return addChildAt( child , _children.size() );
    }

    /**
     * Adds a child to the container at a certain index.
     * @param child The DisplayObject to add at the end of the display list.
     * @param index The specific index of the child to insert in the container.
     * @return The new DisplayObject added in the Display list.
     */
    public DisplayObject addChildAt( DisplayObject child, int index )
    {
        int numChildren = _children.size() ;
        if (index >= 0 && index <= numChildren)
        {
            setRequiresRedraw();
            if ( child._parent == this )
            {
                setChildIndex(child, index);
            }
            else
            {
                _children.add(index, child);

                child.removeFromParent();
                child.setParent(this);
                child.dispatchEvent( new Event( "added" , true ) ) ;

                if ( stage() != null )
                {
                    child.dispatchEvent( new Event("addedToStage" ) );
                }
            }

            return child;
        }
        else
        {
            throw new IllegalArgumentException("Invalid child index");
        }
    }


    /**
     * Determines if a certain object is a child of the container (recursively).
     * @param child The DisplayObject to find in the container.
     * @return A <code>true</code> value if the specific DisplayObject is a child of the container of this sub-containers.
     */
    public boolean contains( DisplayObject child )
    {
        while (child != null )
        {
            if (child == this)
            {
                return true;
            }
            else
            {
                child = child._parent ;
            }
        }
        return false;
    }

    public Rectangle getBounds(DisplayObject targetSpace )
    {
        return getBounds( targetSpace , null ) ;
    }

    public Rectangle getBounds(DisplayObject targetSpace , Rectangle out )
    {
        if (out == null) out = new Rectangle();

        int numChildren = _children.size();

        if (numChildren == 0)
        {
            createTransformationMatrix( targetSpace , _matrixHelper );
            Matrixs.transformCoords( _matrixHelper, 0.0, 0.0, sHelperPoint);
            out.setTo( sHelperPoint.x, sHelperPoint.y, 0, 0 );
        }
        else if (numChildren == 1)
        {
            _children.get(0).getBounds(targetSpace, out);
        }
        else
        {
            double minX =  Double.MAX_VALUE ,
                   maxX = -Double.MAX_VALUE ;

            double minY =  Double.MAX_VALUE ,
                   maxY = -Double.MAX_VALUE ;

            for ( int i = 0 ; i<numChildren; ++i )
            {
                _children.get(i).getBounds(targetSpace, out);

                if (minX > out.x)
                {
                    minX = out.x;
                }

                if (maxX < out.getRight())
                {
                    maxX = out.getRight();
                }

                if (minY > out.y)
                {
                    minY = out.y;
                }

                if (maxY < out.getBottom())
                {
                    maxY = out.getBottom();
                }
            }

            out.setTo(minX, minY, maxX - minX, maxY - minY);
        }

        return out;
    }

    /**
     * Returns a child object at a certain index. If you pass a negative index, '-1' will return the last child, '-2' the second to last child, etc.
     * @param index The index of the child.
     * @return The child object at a certain index.
     */
    public DisplayObject getChildAt( int index )
    {
        int numChildren = _children.size();

        if (index < 0)
        {
            index = numChildren + index;
        }

        if (index >= 0 && index < numChildren)
        {
            return _children.get(index);
        }
        else
        {
            throw new IllegalArgumentException("Invalid child index");
        }
    }

    /**
     * Returns the index of a child within the container, or "-1" if it is not found.
     * @param child The child to check.
     * @return the index of a child within the container, or "-1" if it is not found.
     */
    public int getChildIndex( DisplayObject child )
    {
        return _children.indexOf(child);
    }

    @Override
    public DisplayObject hitTest( Point localPoint)
    {
        if ( !_visible || !_touchable || !hitTestMask(localPoint))
        {
            return null;
        }

        DisplayObject target = null;

        double localX = localPoint.x;
        double localY = localPoint.y;

        int numChildren = _children.size();

        for ( int i = numChildren - 1 ; i >= 0; --i )
        {
            DisplayObject child = _children.get(i);

            if (child.isMask())
            {
                continue;
            }

            _matrixHelper.copyFrom(child.getTransformationMatrix());
            _matrixHelper.invert();

            Matrixs.transformCoords(_matrixHelper, localX, localY, sHelperPoint);
            target = child.hitTest(sHelperPoint);

            if (target != null )
            {
                return _touchGroup ? this : target;
            }
        }

        return null;
    }

    /**
     * The number of children of this container.
     * @return The number of children of this container.
     */
    public int numChildren()
    {
        return _children.size();
    }

    /**
     * Removes a child from the container. If the object is not a child, the method returns <code>null</code>. If requested, the child will be disposed right away.
     * @param child The DisplayObject to remove in the container.
     * @param dispose Indicates if the removed display object must be disposed.
     * @return The DisplayObject reference of the removed child in the container.
     */
    public DisplayObject removeChild( DisplayObject child , boolean dispose )
    {
        int childIndex = getChildIndex(child);
        if (childIndex != -1)
        {
            return removeChildAt( childIndex , dispose ) ;
        }
        else
        {
            return null;
        }
    }

    /**
     * Removes a child from the container. If the object is not a child, the method returns <code>null</code>. If requested, the child will be disposed right away.
     * @param child The DisplayObject to remove in the container.
     * @return The DisplayObject reference of the removed child in the container.
     */
    public DisplayObject removeChild( DisplayObject child )
    {
        return removeChild( child , false ) ;
    }


    /**
     * Removes a child at a certain index. The index positions of any display objects above the child are decreased by 1. If requested, the child will be disposed right away.
     * @param index The index to remove a specific child.
     * @return The removed DisplayObject reference.
     */
    public DisplayObject removeChildAt( int index )
    {
        return removeChildAt( index , false ) ;
    }

    /**
     * Removes a child at a certain index. The index positions of any display objects above the child are decreased by 1. If requested, the child will be disposed right away.
     * @param index The index to remove a specific child.
     * @param dispose Indicates if the removed display object must be disposed.
     * @return The removed DisplayObject reference.
     */
    public DisplayObject removeChildAt( int index , boolean dispose )
    {
        if (index >= 0 && index < _children.size())
        {
            setRequiresRedraw();

            DisplayObject child = _children.get(index);
            child.dispatchEvent( new Event( "removed" , true ) ) ;

            if ( stage() != null )
            {
                DisplayObjectContainer container = (DisplayObjectContainer) child ;

                if (container != null )
                {
                    container.dispatchEvent( new Event( "removedFromStage" ) );
                }
                else
                {
                    child.dispatchEvent( new Event( "removedFromStage" ) );
                }
            }

            child.setParent(null);
            index = _children.indexOf(child); // index might have changed by event handler
            if (index >= 0)
            {
                _children.remove(index);
            }
            if (dispose) child.dispose();

            return child;
        }
        else
        {
            throw new IllegalArgumentException("Invalid child index");
        }
    }

    /**
     * All children will be removed.
     */
    public void removeChildren()
    {
        removeChildren(0,-1,false);
    }

    /**
     * All children will be removed.
     * @param dispose Indicates if the removed display object must be disposed.
     */
    public void removeChildren( boolean dispose )
    {
        removeChildren(0,-1, dispose );
    }

    /**
     * Removes a range of children from the container (endIndex included). If no arguments are given, all children will be removed.
     * @param beginIndex The first index to start to remove a range of children from the container.
     * @param endIndex The last index to remove a range of children from the container.
     */
    public void removeChildren( int beginIndex , int endIndex  )
    {
        if ( endIndex < 0 || endIndex >= numChildren() )
        {
            endIndex = numChildren() - 1 ;
        }

        for( int pos = beginIndex ; pos <= endIndex ; ++pos )
        {
            removeChildAt( beginIndex ) ;
        }
    }

    /**
     * Removes a range of children from the container (endIndex included). If no arguments are given, all children will be removed.
     * @param beginIndex The first index to start to remove a range of children from the container.
     * @param endIndex The last index to remove a range of children from the container.
     * @param dispose Indicates if the removed display object must be disposed.
     */
    public void removeChildren( int beginIndex , int endIndex , boolean dispose )
    {
        if ( endIndex < 0 || endIndex >= numChildren() )
        {
            endIndex = numChildren() - 1 ;
        }

        for( int pos = beginIndex ; pos <= endIndex ; ++pos )
        {
            removeChildAt( beginIndex , dispose ) ;
        }
    }

    @Override
    public void render()
    {
        int numChildren = _children.size();
        for ( int i = 0 ; i<numChildren; ++i )
        {
            DisplayObject child = _children.get(i);
            if( child._hasVisibleArea )
            {
               child.render();
            }
        }
    }

    /**
     * Moves a child to a certain index. Children at and after the replaced position move up.
     * @param child The display object reference.
     * @param index The index in the container.
     */
    public void setChildIndex( DisplayObject child , int index )
    {
        int oldIndex = getChildIndex(child);

        if (oldIndex == index)
        {
            return;
        }

        if (oldIndex == -1)
        {
            throw new IllegalArgumentException("Not a child of this container");
        }

        _children.remove(oldIndex);
        _children.add(index, child);

        setRequiresRedraw();
    }

    /**
     * Swaps the indexes of two children.
     * @param child1 The first child display object to swap.
     * @param child2 The second child display object to swap.
     */
    public void swapChildren( DisplayObject child1, DisplayObject child2 )
    {
        int index1 = getChildIndex(child1);
        int index2 = getChildIndex(child2);
        if (index1 == -1 || index2 == -1)
        {
            throw new IllegalArgumentException("Not a child of this container");
        }
        swapChildrenAt(index1, index2);
    }

    /**
     * Swaps the indexes of two children.
     * @param index1 The index of the first child to swap.
     * @param index2 The index of the second child to swap.
     */
    public void swapChildrenAt( int index1 , int index2 )
    {
        DisplayObject child1 = getChildAt(index1);
        DisplayObject child2 = getChildAt(index2);
        _children.set(index1,child2);
        _children.set(index2,child1);
        setRequiresRedraw();
    }

    // ---------- private

    protected ArrayList<DisplayObject> _children;

    private boolean _touchGroup ;
}
