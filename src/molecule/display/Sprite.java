package molecule.display;

/**
 * A Sprite is the most lightweight, non-abstract container class.
 * Use it as a simple means of grouping objects together in one coordinate system.
 */
public class Sprite extends DisplayObjectContainer
{
    /**
     * Creates a new Sprite instance.
     */
    public Sprite()
    {
        super() ;
    }
}
