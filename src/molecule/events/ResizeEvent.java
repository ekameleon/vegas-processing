package molecule.events;

import graphics.geom.Dimension;
import system.events.Event;

public class ResizeEvent extends Event
{
    /**
     * Creates a new ResizeEvent instance.
     * @param type The type of the event.
     */
    public ResizeEvent( String type )
    {
        super(type);
    }

    /**
     * Creates a new ResizeEvent instance.
     * @param type The type of the event.
     * @param bubbles Determines whether the Event object participates in the bubbling stage of the event flow. The default value is <code>false</code>.
     */
    public ResizeEvent( String type , boolean bubbles )
    {
        super(type,bubbles);
    }

    /**
     * Creates a new Event instance.
     * @param type The type of the event.
     * @param bubbles Determines whether the Event object participates in the bubbling stage of the event flow. The default value is <code>false</code>.
     * @param cancelable Determines whether the Event object can be canceled. The default values is <code>false</code>.
     */
    public ResizeEvent( String type , boolean bubbles , boolean cancelable )
    {
        super( type , bubbles , cancelable ) ;
    }

    /**
     * Creates a new ResizeEvent instance.
     * @param type The type of the event.
     * @param width The updated width value.
     * @param height The updated height value.
     */
    public ResizeEvent( String type , double width , double height )
    {
        super(type);
        _dimension.setTo( width , height ) ;
    }

    /**
     * Creates a new ResizeEvent instance.
     * @param type The type of the event.
     * @param width The updated width value.
     * @param height The updated height value.
     * @param bubbles Determines whether the Event object participates in the bubbling stage of the event flow. The default value is <code>false</code>.
     */
    public ResizeEvent( String type , double width , double height , boolean bubbles )
    {
        super(type,bubbles);
        _dimension.setTo( width , height ) ;
    }

    /**
     * Creates a new Event instance.
     * @param type The type of the event.
     * @param width The updated width value.
     * @param height The updated height value.
     * @param bubbles Determines whether the Event object participates in the bubbling stage of the event flow. The default value is <code>false</code>.
     * @param cancelable Determines whether the Event object can be canceled. The default values is <code>false</code>.
     */
    public ResizeEvent( String type , double width , double height , boolean bubbles , boolean cancelable )
    {
        super( type , bubbles , cancelable ) ;
        _dimension.setTo( width , height ) ;
    }

    /**
     * Event type for a "resize" event.
     */
    public static String RESIZE = "resize" ;

    /**
     * Returns a shallow copy of the object.
     * @return a shallow copy of the object.
     */
    public Event clone()
    {
        return new ResizeEvent( _type , _dimension.width , _dimension.height , _bubbles , _cancelable ) ;
    }

    /**
     * Returns the updated dimension.
     * @return the updated dimension.
     */
    public Dimension getDimension()
    {
        return _dimension.clone();
    }

    /**
     * Returns the updated width value.
     * @return the updated width value.
     */
    public double getWidth()
    {
        return _dimension.width ;
    }

    /**
     * Returns the updated height value.
     * @return the updated height value.
     */
    public double getHeight()
    {
        return _dimension.height ;
    }

    /**
     * Returns the String representation of the object.
     * @return The String representation of the object.
     */
    public String toString()
    {
        return "[ResizeEvent " + _type + " width:" + _dimension.width + " height: " + _dimension.height + "]" ;
    }

    private Dimension _dimension = new Dimension() ;
}
