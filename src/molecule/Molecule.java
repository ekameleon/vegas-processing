package molecule;

import graphics.geom.Rectangle;

import molecule.display.DisplayObjectContainer;
import molecule.display.Sprite;
import molecule.display.Stage;

import molecule.events.ResizeEvent;
import processing.awt.PSurfaceAWT;
import processing.core.PApplet;
import system.events.Event;
import system.events.EventDispatcher;

import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.lang.reflect.Field;

public class Molecule extends EventDispatcher implements ComponentListener
{
    /**
     * The Molecule class represents the core of the Molecule rendering framework.
     * Creates a new Molecule instance.
     * @param applet The main processing object reference for all sketches in Processing.
     * @param root The main DisplayObjectContainer of the application.
     * @param viewPort A rectangle describing the area into which the content will be rendered. Default: stage size
     * @param color The color of the background.
     */
    public Molecule( PApplet applet , DisplayObjectContainer root , Rectangle viewPort , int color )
    {
        _applet = applet ;
        _frame  = getFrame() ;

        if (viewPort == null)
        {
            viewPort = new Rectangle(0, 0, applet.width, applet.height);
        }

        makeCurrent();

        _root = root ;
        _viewPort = viewPort;
        _previousViewPort = new Rectangle();

        // ----------

        _stage = new Stage( (float)viewPort.width , (float)viewPort.height , color ) ;

        // ----------

        _applet.getSurface().setResizable(true);
        _frame.addComponentListener(this );

        // ----------

        initializeRoot(); ;
    }

    /**
     * The Molecule class represents the core of the Molecule rendering framework.
     * Creates a new Molecule instance.
     * @param applet The main processing object reference for all sketches in Processing.
     * @param root The main DisplayObjectContainer of the application.
     * @param viewPort A rectangle describing the area into which the content will be rendered. Default: stage size
     */
    public Molecule( PApplet applet , DisplayObjectContainer root , Rectangle viewPort )
    {
        this( applet , root , viewPort , DEFAULT_BACKGROUND_COLOR ) ;
    }

    /**
     * The Molecule class represents the core of the Molecule rendering framework.
     * Creates a new Molecule instance.
     * @param applet The main processing object reference for all sketches in Processing.
     * @param root The main DisplayObjectContainer of the application.
     */
    public Molecule( PApplet applet , DisplayObjectContainer root )
    {
        this( applet , root , null , DEFAULT_BACKGROUND_COLOR ) ;
    }

    /**
     * The Molecule class represents the core of the Molecule rendering framework.
     * Creates a new Molecule instance.
     * @param applet The main processing object reference for all sketches in Processing.
     * @param root The main DisplayObjectContainer of the application.
     * @param color The color of the background.
     */
    public Molecule( PApplet applet , DisplayObjectContainer root , int color )
    {
        this( applet , root , null , color ) ;
    }

    /**
     * The Molecule class represents the core of the Molecule rendering framework.
     * Creates a new Molecule instance.
     * @param applet The main processing object reference for all sketches in Processing.
     * @param color The color of the background.
     */
    public Molecule( PApplet applet , int color )
    {
        this( applet , new Sprite() , null , color ) ;
    }

    /**
     * The Molecule class represents the core of the Molecule rendering framework.
     * Creates a new Molecule instance.
     * @param applet The main processing object reference for all sketches in Processing.
     */
    public Molecule( PApplet applet )
    {
        this( applet , new Sprite() , null , DEFAULT_BACKGROUND_COLOR ) ;
    }


    // -----------

    /**
     * The main processing object reference for all sketches in Processing.
     * @return the main processing object reference for all sketches in Processing.
     */
    public static PApplet applet()
    {
        return _current != null ? _current._applet : null ;
    }

    /**
     * The current singleton reference of the Molecule class.
     * @return the current singleton reference of the Molecule class.
     */
    public static Molecule current()
    {
        return _current ;
    }

    // -----------

    /**
     * Returns the main DisplayObjectContainer of the application.
     * @return the main DisplayObjectContainer of the application.
     */
    public DisplayObjectContainer root()
    {
        return _root;
    }

    /**
     * Sets the main DisplayObjectContainer of the application.
     * @param container the main DisplayObjectContainer reference of the application.
     * @throws IllegalArgumentException If the root is already instantiated or if the applet reference is not defined.
     */
    public void setRoot( DisplayObjectContainer container )
    {
        if ( _root != null )
        {
            throw new IllegalArgumentException("Root may not change after root has been instantiated") ;
        }
        else
        {
            if( _applet == null )
            {
                throw new IllegalArgumentException("Root may not initialize if the applet reference is not defined.") ;
            }
            _root = container ;
            initializeRoot();
        }
    }

    /**
     * Returns the Stage of the application.
     * @return the Stage of the application.
     */
    public DisplayObjectContainer stage()
    {
        return _stage;
    }

    // -----------

    /**
     * The default background color value.
     */
    public static int DEFAULT_BACKGROUND_COLOR = 0x2C3E50 ;

    // -----------

    Frame getFrame()
    {
        Frame frame = null;
        try
        {
            Field f = ((PSurfaceAWT) _applet.getSurface()).getClass().getDeclaredField("frame");
            f.setAccessible(true);
            frame  = (Frame) (f.get(((PSurfaceAWT) _applet.getSurface())));
        }
        catch(Exception ignore)
        {
            //
        }
        return frame;
    }

    /**
     * Make this Molecule instance the <code>current</code> one.
     */
    public void makeCurrent()
    {
        _current = this;
    }

    /**
     * Renders the complete display list.
     */
    public void render()
    {
        makeCurrent();
        updateViewPort();
        boolean doRedraw = _stage.requiresRedraw() ;
        if ( doRedraw )
        {
            dispatchEvent( new Event( "render" ) ) ;
            _stage.render();
        }
    }

    /**
     * Makes sure that the next frame is actually rendered.
     */
    public void setRequiresRedraw()
    {
        _stage.setRequiresRedraw();
    }


    /**
     * Returns the String representation of the object.
     * @return The String representation of the object.
     */
    public String toString()
    {
        return "[Molecule]" ;
    }

    // -----------

    private PApplet _applet ;
    private Frame _frame ;

    protected DisplayObjectContainer _root ;

    private Stage _stage ;

    private Rectangle _previousViewPort;
    private Rectangle _clippedViewPort;
    private Rectangle _viewPort;

    private static Molecule _current ;

    private boolean _supportHighResolutions = false ;

    private void initializeRoot()
    {
        if ( _root != null && _stage != null )
        {
            _stage.addChildAt( _root , 0 );

            dispatchEvent( new Event( "rootCreated" , false ) ) ;
        }
    }

    private void updateViewPort()
    {
        updateViewPort(false );
    }

    private void updateViewPort( boolean forceUpdate )
    {
        if (forceUpdate || !Rectangle.compare(_viewPort, _previousViewPort) )
        {
            _previousViewPort.setTo(_viewPort.x, _viewPort.y, _viewPort.width, _viewPort.height);

            _clippedViewPort = _viewPort.intersection
            (
                new Rectangle(0, 0, _applet.width, _applet.height )
            );

            _applet.pixelDensity( _supportHighResolutions ? _applet.displayDensity() : 1 );
        }
    }

    @Override
    public void componentMoved( ComponentEvent e )
    {
        //System.out.println( this + " componentMoved" );
    }

    @Override
    public void componentHidden( ComponentEvent e )
    {
        //System.out.println( this + " componentHidden" );
    }

    @Override
    public void componentResized( ComponentEvent e )
    {
        if ( e.getSource() == _frame )
        {
            makeCurrent();
            _stage.dispatchEvent( new ResizeEvent( ResizeEvent.RESIZE, _applet.width, _applet.height ) );
        }
    }

    @Override
    public void componentShown( ComponentEvent e )
    {
        //System.out.println( this + " componentShown" );
    }
}