import system.logging.targets.PrintTarget ;
import system.logging.Log ;
import system.logging.Logger ;
import system.logging.LoggerLevel ;

import core.maths ;

public class Vegas
{
    public static void main( String[] args )
    {
        PrintTarget target = new PrintTarget() ;

        target.setFilters( new String[]{ "*" } ) ; // default

        target.setLevel( LoggerLevel.ALL ) ;

        target.useColor = true ; // only in the JAVA console
        target.useDark  = false ; // only in the JAVA console

        target.includeDate    = true ;
        target.includeTime    = true ;
        target.includeLevel   = true ;
        target.includeChannel = true ;
        target.includeLines   = true ;

        Logger logger = Log.getLogger("system.logging") ;

        logger.log      ( "hello {0} {1}" , "log"      , "!" ) ;
        logger.debug    ( "hello {0} {1}" , "debug"    , "!" ) ;
        logger.info     ( "hello {0} {1}" , "info"     , "!" ) ;
        logger.warning  ( "hello {0} {1}" , "warning"  , "!" ) ;
        logger.error    ( "hello {0} {1}" , "error"    , "!" ) ;
        logger.critical ( "hello {0} {1}" , "critical" , "!" ) ;
        logger.wtf      ( "hello {0} {1}" , "wtf"      , "!" ) ;

        logger.log( maths.interpolate( 0.5f , 0 , 100 ) ) ; // 50.0
        logger.log( maths.map( 10,  0, 100, 20, 80  ) ) ; // 26.0
        logger.log( maths.map( 26, 20,  80,  0, 100 ) ) ; // 10.0
        logger.log( maths.normalize( 10, 0 , 100 ) ) ; // 0.1

        float n ;
        n = maths.round(4.572525153, 2) ;
        logger.log("n : " + n) ; // 4.57

        n = core.maths.round(4.572525153, -1) ;
        logger.log("n : " + n) ; // 5
    }
}