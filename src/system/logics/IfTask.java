package system.logics;

import system.process.Action;
import system.process.Task;
import system.rules.Rule;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Perform some tasks based on whether a given condition holds true or not.
 */
public class IfTask extends Task
{
    /**
     * Creates a new IfTask instance.
     */
    public IfTask()
    {
        super() ;
    }

    /**
     * Creates a new IfTask instance.
     * @param rule The conditional rule of the task. Can be a Rule object or a Boolean value.
     */
    public IfTask( Rule rule )
    {
        super() ;
        _rule = rule ;
    }

    /**
     * Creates a new IfTask instance.
     * @param rule The conditional rule of the task. Can be a Rule object or a Boolean value.
     * @param thenTask The Action reference to defines the 'then' block in the 'if' conditional task.
     */
    public IfTask( Rule rule , Action thenTask )
    {
        super() ;
        _rule = rule ;
        _thenTask = thenTask ;
    }

    /**
     * Creates a new IfTask instance.
     * @param rule The conditional rule of the task. Can be a Rule object or a Boolean value.
     * @param thenTask The Action reference to defines the 'then' block in the 'if' conditional task.
     * @param elseTask The Action reference to defines the 'else' block in the 'if' conditional task.
     */
    public IfTask( Rule rule , Action thenTask, Action elseTask )
    {
        super() ;
        _rule = rule ;
        _thenTask = thenTask  ;
        _elseTask = elseTask  ;
    }

    /**
     * Creates a new IfTask instance.
     * @param rule The conditional rule of the task. Can be a Rule object or a Boolean value.
     * @param thenTask The Action reference to defines the 'then' block in the 'if' conditional task.
     * @param elseTask The Action reference to defines the 'else' block in the 'if' conditional task.
     * @param elseIfs The optional ElseIf instances to initialize the 'elseif' blocks in the 'if' conditional task.
     */
    public IfTask( Rule rule , Action thenTask, Action elseTask , ElseIf... elseIfs )
    {
        super() ;
        _rule = rule ;
        _thenTask = thenTask  ;
        _elseTask = elseTask  ;
        if( elseIfs.length > 0 )
        {
            Collections.addAll( _elseIfTasks , elseIfs ) ;
        }
    }

    /**
     * Indicates if the class throws errors or notify a finished event when the task failed.
     */
    public boolean throwError ;

    /**
     * Defines the action when the condition block use the else condition.
     * @param action The action to defines with the else condition in the IfTask reference.
     * @return The current IfTask reference.
     * @throws Error if an 'else' action is already register.
     */
    public IfTask addElse( Action action )
    {
        if ( _elseTask != null )
        {
            throw new Error( this + " addElse failed, you must not nest more than one <else> into <if>");
        }
        else
        {
            _elseTask = action ;
        }
        return this ;
    }

    /**
     * Defines an action when the condition block use the elseif condition.
     * @param values A serie of ElseIf instances or Rule/Task pairs.
     * @return The current IfTask reference.
     * @throws Error The condition and action reference not must be null.
     */
    public IfTask addElseIf( ElseIf... values )
    {
        if ( values.length > 0 )
        {
            int len = values.length ;
            for( int i = 0 ; i<len ; i++ )
            {
                _elseIfTasks.add( values[i] ) ;
            }
        }
        return this ;
    }

    /**
     * Defines the main conditional rule of the task.
     * @param rule The main Rule of the task.
     * @return The current IfTask reference.
     * @throws Error if a 'condition' is already register.
     */
    public IfTask addRule( Rule rule )
    {
        if ( _rule != null )
        {
            throw new Error( this + " addRule failed, you must not nest more than one <condition> into <if>");
        }
        else
        {
            _rule = rule ;
        }
        return this ;
    }

    /**
     * Defines the action when the condition block success and must run the 'then' action.
     * @param action Defines the 'then' action in the IfTask reference.
     * @return The current IfTask reference.
     * @throws Error if the 'then' action is already register.
     */
    public IfTask addThen( Action action )
    {
        if ( _thenTask != null )
        {
            throw new Error( this + " addThen failed, you must not nest more than one <then> into <if>");
        }
        else
        {
            _thenTask = action ;
        }
        return this ;
    }

    /**
     * Returns the shallow copy of the object.
     * @return the shallow copy of the object.
     */
    public IfTask clone()
    {
        IfTask copy = null ;

        Class clazz = this.getClass() ;
        try
        {
            Constructor ctr = clazz.getConstructor() ;
            copy = (IfTask)(ctr.newInstance()) ;
        }
        catch ( NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e)
        {
            e.printStackTrace();
        }

        if( copy != null )
        {
            copy.addRule(_rule) ;
            copy.addThen(_thenTask) ;
            copy.addElse(_elseTask) ;
            if ( _elseIfTasks.size() > 0 )
            {
                for ( ElseIf ei : _elseIfTasks )
                {
                    copy.addElseIf( ei ) ;
                }
            }
        }

        return copy ;
    }


    /**
     * Removes all the 'elseIf' action.
     * @return The current IfTask reference.
     */
    public IfTask deleteAllElseIf()
    {
        _elseIfTasks.clear() ;
        return this ;
    }

    /**
     * Removes the 'else' action.
     * @return The current IfTask reference.
     */
    public IfTask deleteElse()
    {
        _elseTask = null ;
        return this ;
    }

    /**
     * Removes the 'rule' of the task.
     * @return The current IfTask reference.
     */
    public IfTask deleteRule()
    {
        _rule = null ;
        return this ;
    }

    /**
     * Removes the 'then' action.
     * @return The current IfTask reference.
     */
    public IfTask deleteThen()
    {
        _thenTask = null ;
        return this ;
    }

    @Override
    public void run( Object... args )
    {
        if ( _running )
        {
            return ;
        }

        _done = false ;

        notifyStarted() ;

        if ( throwError && _rule == null )
        {
            throw new Error( this + " run failed, the 'conditional rule' of the task not must be null.") ;
        }

        if ( _rule != null && _rule.eval() )
        {
            if( _thenTask != null )
            {
                _execute( _thenTask ) ;
            }
            else if ( throwError )
            {
                throw new Error( this + " run failed, the 'then' action not must be null.") ;
            }
        }
        else
        {
            if ( _elseIfTasks.size() > 0 )
            {
                ElseIf ei ;
                int len = _elseIfTasks.size() ;
                for ( int i = 0 ; (i<len) && !_done ; i++ )
                {
                    ei = _elseIfTasks.get(i) ;
                    if ( ei.eval() )
                    {
                        _execute( ei.then ) ;
                    }
                }
            }

            if( !_done && _elseTask != null )
            {
                _execute( _elseTask ) ;
            }
        }

        if( !_done )
        {
            if ( throwError )
            {
                throw new Error( this + " run failed, the 'then' action not must be null.") ;
            }
            else
            {
                notifyFinished() ;
            }
        }
    }

    private boolean _done ;
    protected ArrayList<ElseIf> _elseIfTasks = new ArrayList<>();
    private Action _elseTask ;
    private Rule _rule;
    private Action _thenTask;

    private void _execute( Action action )
    {
        if ( action != null )
        {
            _done = true ;
            action.finishIt().connect( this , "_finishTask" , 1 , true ) ;
            action.run() ;
        }
    }

    private void _finishTask( Action action )
    {
        notifyFinished() ;
    }
}