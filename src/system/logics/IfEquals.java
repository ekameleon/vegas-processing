/*
  Version: MPL 2.0/GPL 2.0/LGPL 2.1

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/

  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.

  The Original Code is [VEGAS framework].

  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com> and Zwetan Kjukov <zwetan@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2003-2018
  the Initial Developers. All Rights Reserved.

  Contributor(s):

  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package system.logics;

import system.process.Action;
import system.rules.EmptyString;
import system.rules.Equals;

import java.util.Collections;

/**
 * Perform some tasks based on whether a given condition holds equality of two values.
 */
public class IfEquals extends IfTask
{
    /**
     * Creates a new IfEquals instance.
     */
    public IfEquals()
    {
        super() ;
    }

    /**
     * Creates a new IfEquals instance.
     * @param value1 The first value to evaluate.
     * @param value2 The second value to evaluate.
     */
    public IfEquals( Object value1 , Object value2 )
    {
        super( new Equals(value1,value2) ) ;
    }

    /**
     * Creates a new IfEquals instance.
     * @param value1 The first value to evaluate.
     * @param value2 The second value to evaluate.
     * @param thenTask The Action reference to defines the 'then' block in the 'if' conditional task.
     */
    public IfEquals( Object value1 , Object value2 , Action thenTask )
    {
        super( new Equals(value1,value2), thenTask , null ) ;
    }

    /**
     * Creates a new IfEquals instance.
     * @param value1 The first value to evaluate.
     * @param value2 The second value to evaluate.
     * @param thenTask The Action reference to defines the 'then' block in the 'if' conditional task.
     * @param elseTask The Action reference to defines the 'else' block in the 'if' conditional task.
     */
    public IfEquals( Object value1 , Object value2 , Action thenTask, Action elseTask )
    {
        super( new Equals(value1,value2), thenTask , elseTask ) ;
    }

    /**
     * Creates a new IfEquals instance.
     * @param value1 The first value to evaluate.
     * @param value2 The second value to evaluate.
     * @param thenTask The Action reference to defines the 'then' block in the 'if' conditional task.
     * @param elseTask The Action reference to defines the 'else' block in the 'if' conditional task.
     * @param elseIfs The optional ElseIf instances to initialize the 'elseif' blocks in the 'if' conditional task.
     */
    public IfEquals( Object value1 , Object value2 , Action thenTask, Action elseTask , ElseIf... elseIfs )
    {
        super( new Equals(value1,value2), thenTask , elseTask ) ;
        if( elseIfs.length > 0 )
        {
            Collections.addAll( _elseIfTasks , elseIfs ) ;
        }
    }
}