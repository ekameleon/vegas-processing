/*
  Version: MPL 2.0/GPL 2.0/LGPL 2.1

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/

  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.

  The Original Code is [VEGAS framework].

  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com> and Zwetan Kjukov <zwetan@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2003-2018
  the Initial Developers. All Rights Reserved.

  Contributor(s):

  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package system.rules;

import java.util.ArrayList;

/**
 * Used to perform a logical disjunction on two conditions or more.
 * <p>Example: </p>
 * <pre>
 * {@code
 * import system.rules.*;
 *
 * public class Vegas
 * {
 *     public static void main( String[] args )
 *     {
 *         Rule rule1 = new Even(1) ;
 *         Rule rule2 = new Even(5) ;
 *         Rule rule3 = new Even(4) ;
 *         Rule rule4 = new Even(8) ;
 *
 *         System.out.println( new Or().eval() ); // false
 *
 *         System.out.println( new Or( rule1 ).eval() ); // false
 *         System.out.println( new Or( rule1 , rule2 ).eval() ); // false
 *         System.out.println( new Or( rule1 , rule3 ).eval() ); // true
 *
 *         System.out.println( new Or( rule3 ).eval() ); // true
 *         System.out.println( new Or( rule3 , rule4 ).eval() ); // true
 *         System.out.println( new Or( rule3 , rule1 ).eval() ); // true
 *     }
 * }
 * }
 * </pre>
 */
public class Or implements Rule
{
    /**
     * Creates a new Or instance.
     * @param conditions The rules to defines the condition.
     */
    public Or(Rule... conditions )
    {
        int size = conditions.length ;
        if( size > 0 )
        {
            for( int i = 0 ; i<size ; i++)
            {
                rules.add( conditions[i] ) ;
            }
        }
    }

    public ArrayList<Rule> rules = new ArrayList<>() ;

    @Override
    public boolean eval()
    {
        if( rules != null && rules.size() > 0 )
        {
            boolean b = rules.get(0).eval() ;
            int     l = rules.size() ;
            for ( int i = 1 ; i<l ; i++ )
            {
                b = (b || rules.get(i).eval()) ;
            }
            return b ;
        }
        else
        {
            return false ;
        }
    }
}
