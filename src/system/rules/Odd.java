/*
  Version: MPL 2.0/GPL 2.0/LGPL 2.1

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/

  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.

  The Original Code is [VEGAS framework].

  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com> and Zwetan Kjukov <zwetan@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2003-2018
  the Initial Developers. All Rights Reserved.

  Contributor(s):

  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package system.rules;

/**
 * Evaluates if the value is odd.
 * <p>Example: </p>
 * <pre>
 * {@code
 * import system.rules.*;
 *
 * public class Vegas
 * {
 *     public static void main( String[] args )
 *     {
 *         System.out.println( new Odd().eval() ); // false
 *         System.out.println( new Odd(0).eval() ); // false
 *         System.out.println( new Odd(1).eval() ); // true
 *         System.out.println( new Odd(2).eval() ); // false
 *         System.out.println( new Odd(3).eval() ); // true
 *     }
 * }
 * }
 * </pre>
 */
public class Odd implements Rule
{
    /**
     * Creates a new Odd instance.
     */
    public Odd()
    {
        this.value = null ;
    }

    /**
     * Creates a new Odd instance.
     * @param value The value to evaluate.
     */
    public Odd( Object value )
    {
        this.value = value ;
    }

    /**
     * The value to evaluate.
     */
    public Object value ;

    @Override
    public boolean eval()
    {
        if( value instanceof Number )
        {
            if( value instanceof Integer )
            {
                return (int) value % 2 != 0 ;
            }
            else if( value instanceof Double )
            {
                return (double) value % 2 != 0 ;
            }
            else if( value instanceof Float )
            {
                return (float) value % 2 != 0 ;
            }
        }
        return false ;
    }
}
