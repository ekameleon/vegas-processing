/*
  Version: MPL 2.0/GPL 2.0/LGPL 2.1

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/

  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.

  The Original Code is [VEGAS framework].

  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com> and Zwetan Kjukov <zwetan@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2003-2018
  the Initial Developers. All Rights Reserved.

  Contributor(s):

  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package system.rules;

/**
 * Used to perform a logical conjunction on two conditions and more.
 * <p>Example: </p>
 * <pre>
 * {@code
 * import system.rules.*;
 *
 * public class Vegas
 * {
 *     public static void main( String [] args )
 *     {
 *         new Vegas().init() ;
 *     }
 *
 *     public init()
 *     {
 *         System.out.println( new Equals().eval() ); // false
 *
 *         System.out.println( new Equals(1,1).eval() ); // true
 *         System.out.println( new Equals(1,2).eval() ); // false
 *
 *         Point o1 = new Point(1,2);
 *         Point o2 = new Point(1,2);
 *         Point o3 = new Point(4,5);
 *
 *         System.out.println( new Equals(o1,o1).eval() ); // true
 *         System.out.println( new Equals(o1,o2).eval() ); // true
 *         System.out.println( new Equals(o1,o3).eval() ); // false
 *         System.out.println( new Equals(o2,o3).eval() ); // false
 *
 *         Rule rule1 = new Odd(1) ;
 *         Rule rule2 = new Odd(1) ;
 *         Rule rule3 = new Odd(2) ;
 *
 *         System.out.println( new Equals(rule1,rule1).eval() ); // true
 *         System.out.println( new Equals(rule1,rule2).eval() ); // true
 *         System.out.println( new Equals(rule1,rule3).eval() ); // false
 *     }
 *
 *     public class Point
 *     {
 *         public Point( float x, float y )
 *         {
 *             this.x = x ;
 *             this.y = y ;
 *         }
 *
 *         float x = 0 ;
 *         float y = 0 ;
 *
 *         public boolean equals( Object o )
 *         {
 *             if( o == this )
 *             {
 *                 return true ;
 *             }
 *             if( o instanceof Point)
 *             {
 *                 return x == ((Point)o).x && y == ((Point)o).y ;
 *             }
 *             return false ;
 *         }
 *     }
 * }
 * }
 * </pre>
 */
public class Equals implements Rule
{
    /**
     * Creates a new Equals instance.
     * @param value1 The first value to evaluate.
     * @param value2 The second value to evaluate.
     */
    public Equals( Object value1 , Object value2 )
    {
        this.value1 = value1;
        this.value2 = value2;
    }

    /**
     * Creates a new Equals instance.
     */
    public Equals() {}

    /**
     * The first value to evaluate.
     */
    public Object value1 ;

    /**
     * The second value to evaluate.
     */
    public Object value2 ;

    @Override
    public boolean eval()
    {
        if( value1 == null || value2 == null )
        {
            return false ;
        }
        else if( value1 == value2 )
        {
            return true ;
        }
        else if( value1 instanceof Rule && value2 instanceof Rule )
        {
            return ((Rule) value1).eval() == ((Rule) value2).eval() ;
        }
        else
        {
            return value1.equals(value2);
        }
    }
}
