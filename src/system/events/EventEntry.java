/*
  Version: MPL 2.0/GPL 2.0/LGPL 2.1

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/

  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.

  The Original Code is [VEGAS framework].

  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com> and Zwetan Kjukov <zwetan@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2003-2018
  the Initial Developers. All Rights Reserved.

  Contributor(s):

  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package system.events ;

public class EventEntry implements Comparable<EventEntry>
{
    /**
     * The event listener reference.
     */
    public EventListener listener ;

    /**
     * The priority value.
     */
    public int priority;

    /**
     * The type value.
     */
    public String type ;

    /**
     * The useCapture flag.
     */
    public boolean useCapture;

    /**
     * Creates a new EventEntry instance.
     * @param type The type of the event to handle.
     * @param listener The EventListener reference.
     * @param useCapture Indicates if the dispatcher use the capture propagation.
     * @param priority The value to sort the listener.
     */
    public EventEntry( String type , EventListener listener, boolean useCapture , int priority )
    {
        this.listener = listener ;
        this.priority = priority ;
        this.type = type ;
        this.useCapture = useCapture ;
    }

    /**
     * Compares the object with the other object.
     * @param entry The object to compare.
     * @return -1, 0 or 1.
     */
    public int compareTo( EventEntry entry )
    {
        if ( priority > entry.priority )
        {
            return -1;
        }
        else if ( priority < entry.priority )
        {
            return 1;
        }
        else
        {
            return 0 ;
        }
    }
}