/*
  Version: MPL 2.0/GPL 2.0/LGPL 2.1

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/

  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.

  The Original Code is [VEGAS framework].

  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com> and Zwetan Kjukov <zwetan@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2003-2018
  the Initial Developers. All Rights Reserved.

  Contributor(s):

  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package system.events ;

import java.util.ArrayList ;
import java.util.Collections ;
import java.util.HashMap;

/**
 * The W3C Event model dispatcher implementation.
 * <br>
 * <strong>Example:</strong>
 * <pre>
 * import system.events.EventDispatcher ;
 * import system.events.EventListener ;
 *
 * void setup()
 * {
 *     size(800, 500);
 *     background(100);
 *
 *     EventDispatcher dispatcher = new EventDispatcher() ;
 *
 *     Console console1 = new Console( "console1" ) ;
 *     Console console2 = new Console( "console2" ) ;
 *     Console console2 = new Console( "console3" ) ;
 *
 *     dispatcher.addEventListener( "click" , console1 ) ;
 *     dispatcher.addEventListener( "click" , console2 , false , 1000 ) ;
 *     dispatcher.addEventListener( "click" , console3 ) ;
 *
 *     dispatcher.dispatchEvent( new Event( "click" ) ) ;
 *
 *     dispatcher.removeEventListener( "click" , console2 ) ;
 *
 *     dispatcher.dispatchEvent( new Event( "click" ) ) ;
 * }
 *
 * public class Console implements EventListener
 * {
 *     public String name ;
 *
 *     public Console( String name )
 *     {
 *         this.name = name ;
 *     }
 *
 *     public void handleEvent( Event event )
 *     {
 *         println( this.name + " : " + event.getType() ) ;
 *     }
 *
 *     public String toString()
 *     {
 *         return "[Console " + name + "]" ;
 *     }
 * }
 * </pre>
 */
public class EventDispatcher implements EventTarget
{
    private EventTarget _target ;
    private HashMap<String,ArrayList<EventEntry>> _captureListeners = new HashMap<String,ArrayList<EventEntry>>();
    private HashMap<String,ArrayList<EventEntry>> _listeners = new HashMap<String,ArrayList<EventEntry>>( );

    /**
     * Creates a new Event instance.
     */
    public EventDispatcher()
    {
        _target = this ;
    }

    /**
     * Creates a new Event instance.
     * @param target The proxy EventTarget reference of this dispatcher.
     */
    public EventDispatcher( EventTarget target )
    {
        _target = target ;
    }

    /**
     * Registers an event listener object with a target so that the listener receives notification of an event.
     * @param type The type of the event.
     * @param listener The EventListener to register.
     */
    public void addEventListener( String type, EventListener listener )
    {
        addEventListener( type , listener, false , 0 ) ;
    }

    /**
     * Registers an event listener object with a target so that the listener receives notification of an event.
     * @param type The type of the event.
     * @param listener The EventListener to register.
     * @param useCapture The useCapture flag.
     */
    public void addEventListener( String type, EventListener listener, boolean useCapture )
    {
        addEventListener( type , listener, useCapture , 0 ) ;
    }

    /**
     * Registers an event listener object with a target so that the listener receives notification of an event.
     * @param type The type of the event.
     * @param listener The EventListener to register.
     * @param useCapture The useCapture flag.
     * @param priority The priority flag to sort the listener.
     */
    public void addEventListener( String type, EventListener listener, boolean useCapture , int priority )
    {
        HashMap<String,ArrayList<EventEntry>> collection = useCapture ? _captureListeners : _listeners ;

        EventEntry entry = new EventEntry( type , listener , useCapture , priority ) ;

        ArrayList<EventEntry> list  ;

        if ( collection.containsKey( type ) )
        {
            list = collection.get(type) ;
        }
        else
        {
            list = new ArrayList<EventEntry>() ;
            collection.put( type , list ) ;
        }

        list.add( entry ) ;

        Collections.sort( list ) ;
    }

    /**
     * Dispatches an event into the event flow. The event target is the target upon which <code>dispatchEvent()</code> is called.
     * @param event The Event to dispatch.
     * @return <code>true</code> if the dispatch succeed.
     */
    public boolean dispatchEvent( Event event )
    {
        event = event.withTarget( _target );

        ArrayList<EventDispatcher> ancestors = createAncestorChain();

        event.setEventPhase( EventPhase.CAPTURING_PHASE ) ;

        EventDispatcher.internalHandleCapture( event , ancestors );

        if ( !event.isPropagationStopped() )
        {
            event.setEventPhase( EventPhase.AT_TARGET ) ;
            event.withCurrentTarget( event.getTarget() );
            if ( _listeners.containsKey( event.getType() ) )
            {
                EventDispatcher.processListeners( event, _listeners.get( event.getType() ) );
            }
        }

        if ( event.getBubbles() && !event.isPropagationStopped() )
        {
            event.setEventPhase( EventPhase.BUBBLING_PHASE ) ;
            EventDispatcher.internalHandleBubble( event , ancestors ) ;
        }

        return !event.isDefaultPrevented() ;
    }

    /**
     * Checks whether the target has any listeners registered for a specific type of event.
     * @param type The type of the event to handle.
     * @return <code>true</code> if the EventListener is registered with the specific type.
     */
    public boolean hasEventListener( String type )
    {
        return _listeners.containsKey(type) || _captureListeners.containsKey(type) ;
    }

    /**
     * Removes a listener from the target.
     * @param type The type of the event to handle.
     * @param listener The EventListener to unregister.
     */
    public void removeEventListener( String type, EventListener listener )
    {
        removeEventListener( type , listener , false ) ;
    }

    /**
     * Removes a listener from the target.
     * @param type The type of the event.
     * @param listener The EventListener to unregister.
     * @param useCapture The useCapture flag.
     */
    public void removeEventListener( String type, EventListener listener, boolean useCapture )
    {
        HashMap<String,ArrayList<EventEntry>> collection = useCapture ? _captureListeners : _listeners ;

        ArrayList<EventEntry> listeners = collection.get(type);

        if ( listeners.size() > 0 )
        {
            int len = listeners.size() ;
            for ( int i = 0 ; i < len ; ++i )
            {
                if ( listeners.get(i).listener == listener )
                {
                    if ( len == 1 )
                    {
                        listeners.clear() ;
                        collection.remove(type) ;
                    }
                    else
                    {
                        listeners.remove(i);
                    }
                    break;
                }
            }
        }
    }

    /**
     * Returns the String representation of the object.
     * @return The String representation of the object.
     */
    public String toString()
    {
        return "[" +  getClass().getName() + "]" ;
    }

    /**
     * Checks whether an event listener is registered with this EventDispatcher object or any of its ancestors for the specified event type.
     * @param type The type to check.
     * @return a boolean value.
     */
    public boolean willTrigger( String type )
    {
        return true ;
    }

    //// ----- private

    /**
     * Overrides this method.
     * @return The list of dispatcher to use in the bubbling/capture phases.
     */
    protected ArrayList<EventDispatcher> createAncestorChain()
    {
        return null;
    }


    protected void processCapture( Event event )
    {
        event.withCurrentTarget( _target ) ;
        ArrayList<EventEntry> listeners = _captureListeners.get( event.getType() ) ;
        if ( listeners.size() > 0 )
        {
            EventDispatcher.processListeners( event , listeners );
        }
    }

    protected void processBubble( Event event )
    {
        event.withCurrentTarget( _target );
        ArrayList<EventEntry> listeners = _listeners.get( event.getType() ) ;
        if ( listeners.size() > 0 )
        {
            EventDispatcher.processListeners( event, listeners ) ;
        }
    }

    //// ----- static

    protected static void processListeners( Event event , ArrayList<EventEntry> listeners )
    {
        if( listeners.size() > 0 )
        {
            int len = listeners.size() ;
            for ( int i = 0 ; i < len ; ++i )
            {
                EventListener listener = listeners.get(i).listener ;
                // Boolean flag = listener.handleEvent( event ) ;

                listener.handleEvent( event ) ;

                // if( flag == false  // FIXME use a try/catch handler
                // {
                //     event.stopPropagation();
                //     event.preventDefault();
                // }

                if ( event.isImmediatePropagationStopped() )
                {
                    break;
                }
            }
        }
    }

    protected static void internalHandleCapture( Event event , ArrayList<EventDispatcher> ancestors )
    {
        if ( ancestors == null || ancestors.size() < 1 )
        {
            return ;
        }
        EventDispatcher dispatcher ;
        int len = ancestors.size() - 1 ;
        for ( int i = len ; i >= 0 ; i-- )
        {
            dispatcher = ancestors.get(i) ;
            dispatcher.processCapture( event );
            if ( event.isPropagationStopped() )
            {
                break ;
            }
        }
    }

    protected static void internalHandleBubble( Event event , ArrayList<EventDispatcher> ancestors )
    {
        if ( ancestors == null || ancestors.size() < 1 )
        {
            return;
        }
        EventDispatcher dispatcher ;
        int len = ancestors.size() ;
        for ( int i = 0 ; i < len ; i++ )
        {
            dispatcher = ancestors.get(i);
            dispatcher.processBubble( event ) ;
            if ( event.isPropagationStopped() )
            {
                break;
            }
        }
    }
}