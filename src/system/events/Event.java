/*
  Version: MPL 2.0/GPL 2.0/LGPL 2.1

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/

  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.

  The Original Code is [VEGAS framework].

  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com> and Zwetan Kjukov <zwetan@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2003-2018
  the Initial Developers. All Rights Reserved.

  Contributor(s):

  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package system.events ;

/**
 * A W3C event object.
 */
public class Event
{
    protected boolean _bubbles = false ;

    protected boolean _cancelable = false ;

    protected EventTarget _currentTarget ;

    private boolean _defaultPrevented = false ;

    protected int _eventPhase = EventPhase.NONE ;

    private boolean _propagationStopped = false ;

    private boolean _immediatePropagationStopped = false ;

    protected EventTarget _target ;

    protected String _type ;

    /**
     * Creates a new Event instance.
     * @param type The type of the event.
     */
    public Event( String type )
    {
        _type = type ;
    }

    /**
     * Creates a new Event instance.
     * @param type The type of the event.
     * @param bubbles Determines whether the Event object participates in the bubbling stage of the event flow. The default value is <code>false</code>.
     */
    public Event( String type , boolean bubbles )
    {
        _type = type ;
        _bubbles = bubbles ;
    }

    /**
     * Creates a new Event instance.
     * @param type The type of the event.
     * @param bubbles Determines whether the Event object participates in the bubbling stage of the event flow. The default value is <code>false</code>.
     * @param cancelable Determines whether the Event object can be canceled. The default values is <code>false</code>.
     */
    public Event( String type , boolean bubbles , boolean cancelable )
    {
        _type = type ;
        _bubbles = bubbles ;
        _cancelable = cancelable ;
    }

    /**
     * Returns a shallow copy of the object.
     * @return a shallow copy of the object.
     */
    public Event clone()
    {
        return new Event( _type , _bubbles ) ;
    }

    /**
     * Indicates whether an event is a bubbling event. If the event can bubble, this value is <code>true</code>; otherwise it is <code>false</code>.
     * @return <code>true</code> if the event is a bubbling event.
     */
    public boolean getBubbles()
    {
        return _bubbles ;
    }

    /**
     * Indicates whether the behavior associated with the event can be prevented.
     * If the behavior can be canceled, this value is <code>true</code>; otherwise it is <code>false</code>.
     * @return <code>true</code> if the event is a cancelable event.
     */
    public boolean getCancelable()
    {
        return _cancelable ;
    }

    /**
     * The object that is actively processing the Event object with an event listener. For example, if a user clicks an OK button, the current target could be the node containing that button or one of its ancestors that has registered an event listener for that event.
     * @return The object that is actively processing the Event object with an event listener.
     */
    public EventTarget getCurrentTarget()
    {
        return _currentTarget ;
    }

    /**
     * The current phase in the event flow. This property can contain the following numeric values:
     * <ul>
     * <li>The capture phase (<code>EventPhase.NONE === 0</code>).</li>
     * <li>The capture phase (<code>EventPhase.CAPTURING_PHASE === 1</code>).</li>
     * <li>The target phase (<code>EventPhase.AT_TARGET === 2</code>).</li>
     * <li>The bubbling phase (<code>EventPhase.BUBBLING_PHASE === 3</code>).</li>
     * </ul>
     * @return The event phase value.
     */
    public int getEventPhase()
    {
        return _eventPhase ;
    }

    /**
     * Indicates the event target of event (is case-sensitive).
     * @return the target of event (is case-sensitive).
     */
    public EventTarget getTarget()
    {
        return _target ;
    }

    /**
     * Indicates the type of event (is case-sensitive).
     * @return The type of the event.
     */
    public String getType()
    {
        return _type ;
    }

    /**
     * Checks whether the <code>preventDefault()</code> method has been called on the event. If the <code>preventDefault()</code> method has been called, returns <code>true</code>; otherwise, returns <code>false</code>.
     * @return <code>true</code> if the <code>preventDefault()</code> method has been called on the event.
     */
    public boolean isDefaultPrevented()
    {
        return _defaultPrevented ;
    }

    /**
     * Check if event.stopImmediatePropagation() was called.
     * @return <code>true</code> if event.stopImmediatePropagation() was called.
     */
    public boolean isImmediatePropagationStopped()
    {
        return _immediatePropagationStopped ;
    }

    /**
     * Check if event.stopPropagation() was called.
     * @return <code>true</code> if event.stopPropagation() was called.
     */
    public boolean isPropagationStopped()
    {
        return _propagationStopped ;
    }

    /**
     * Cancels an event's default behavior if that behavior can be canceled.
     */
    public void preventDefault()
    {
        if( _cancelable )
        {
            _defaultPrevented = true;
        }
    }

    /**
     * Prevents processing of any event listeners in the current node and any subsequent nodes in the event flow.
     */
    public void stopImmediatePropagation()
    {
        _immediatePropagationStopped = true;
    }

    /**
     * Prevents processing of any event listeners in nodes subsequent to the current node in the event flow.
     */
    public void stopPropagation()
    {
        _propagationStopped = true;
    }

    /**
     * Returns the String representation of the object.
     * @return The String representation of the object.
     */
    public String toString()
    {
        return "[Event " + _type + "]" ;
    }

    // --------- protected

    protected void setEventPhase( int phase )
    {
        _eventPhase = phase ;
    }

    protected Event withTarget( EventTarget target )
    {
        Event event = (this.getTarget() != null) ? this.clone() : this ;
        event._target = target;
        return event ;
    }

    protected Event withCurrentTarget( EventTarget currentTarget )
    {
        _currentTarget = currentTarget ;
        return this;
    }
}