/*
  Version: MPL 2.0/GPL 2.0/LGPL 2.1

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/

  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.

  The Original Code is [VEGAS framework].

  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com> and Zwetan Kjukov <zwetan@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2003-2018
  the Initial Developers. All Rights Reserved.

  Contributor(s):

  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package system.events ;

/**
 * The EventTarget interface is implemented by all Nodes in an implementation which supports the DOM Event Model.
 */
public interface EventTarget
{
    /**
     * Registers an event listener object with a target so that the listener receives notification of an event.
     * @param type The type of the event.
     * @param listener The EventListener to register.
     */
    void addEventListener( String type, EventListener listener ) ;

    /**
     * Registers an event listener object with a target so that the listener receives notification of an event.
     * @param type The type of the event.
     * @param listener The EventListener to register.
     * @param useCapture The useCapture flag.
     */
    void addEventListener( String type, EventListener listener, boolean useCapture ) ;

    /**
     * Registers an event listener object with a target so that the listener receives notification of an event.
     * @param type The type of the event.
     * @param listener The EventListener to register.
     * @param useCapture The useCapture flag.
     * @param priority The priority flag to sort the listener.
     */
    void addEventListener( String type, EventListener listener, boolean useCapture , int priority ) ;

    /**
     * Dispatches an event into the event flow. The event target is the target upon which <code>dispatchEvent()</code> is called.
     * @param event The Event to dispatch.
     * @return <code>true</code> if the dispatch succeed.
     */
    boolean dispatchEvent( Event event ) ;

    /**
     * Checks whether the target has any listeners registered for a specific type of event.
     * @param type The type of the event to handle.
     * @return <code>true</code> if the EventListener is registered with the specific type.
     */
    boolean hasEventListener( String type ) ;

    /**
     * Removes a listener from the target.
     * @param type The type of the event.
     * @param listener The EventListener to unregister.
     */
    void removeEventListener( String type, EventListener listener ) ;

    /**
     * Removes a listener from the target.
     * @param type The type of the event.
     * @param listener The EventListener to unregister.
     * @param useCapture The useCapture flag.
     */
    void removeEventListener( String type, EventListener listener, boolean useCapture ) ;

    /**
     * Checks whether an event listener is registered with this EventDispatcher object or any of its ancestors for the specified event type.
     * @param type The type to check.
     * @return a boolean value.
     */
    boolean willTrigger( String type ) ;
}