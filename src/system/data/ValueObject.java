/*
  Version: MPL 2.0/GPL 2.0/LGPL 2.1

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/

  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.

  The Original Code is [VEGAS framework].

  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com> and Zwetan Kjukov <zwetan@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2003-2018
  the Initial Developers. All Rights Reserved.

  Contributor(s):

  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package system.data;

import java.util.ArrayList;

/**
 * Defines a value object.
 * The value object are use for example in the models or to exchange datas between a client and a server.
 */
public class ValueObject implements Identifiable
{
    /**
     * Creates a new ValueObject instance.
     */
    public ValueObject() {}

    /**
     * Creates a new ValueObject instance.
     * @param id The unique identifier of the object.
     */
    public ValueObject( Object id )
    {
        _id = id ;
    }

    // ------ Properties

    protected Object _id ;

    // ------ Methods

    /**
     * Returns a shallow copy of the object.
     * @return a shallow copy of the object.
     */
    @Override
    public ValueObject clone()
    {
        return new ValueObject( _id ) ;
    }

    /**
     * A utility function for implementing the <code>toString()</code> method in custom classes. Overriding the <code>toString()</code> method is recommended, but not required.
     * @param className The name of the class.
     * @param rest The properties to display in the toString method.
     * @return The formatted string representation of the object.
     */
    public String formatToString( String className , Object... rest )
    {
        if( className == null || className.equals("") )
        {
            className = this.getClass().getName() ;
        }

        ArrayList<String> list = new ArrayList<>() ;

        list.add( className ) ;

        for( Object arg : rest )
        {
            list.add( (String)arg ) ;
        }

        return "[" + String.join(" " , list ) + "]" ;
    }

    /**
     * Returns the unique identifier value of the object.
     * @return The unique identifier value of the object.
     */
    @Override
    public Object getID()
    {
        return _id;
    }

    /**
     * Defines the unique identifier value of the object.
     */
    @Override
    public void setID( Object id )
    {
        _id = id ;
    }

    /**
     * Returns the String representation of the object.
     * @return The String representation of the object.
     */
    public String toString()
    {
        if( _id != null )
        {
            return formatToString(null , "id:" + _id.toString() );
        }
        else
        {
            return formatToString(null ) ;
        }
    }
}