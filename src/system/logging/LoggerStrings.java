package system.logging;

/**
 * The enumeration of all string expressions in the logging engine.
 */
public enum LoggerStrings
{
    /**
     * The static field used when throws an Error when a character is invalid.
     */
    CHARS_INVALID ( "The following characters are not valid." ) ,

    /**
     * The static field used when throws an Error when the character placement failed.
     */
    CHAR_PLACEMENT ( "'*' must be the right most character."   ) ,

    /**
     * The static field used when throws an Error if the filter is empty or null.
     */
    EMPTY_FILTER ( "filter must not be null or empty." ) ,

    /**
     * The static field used when throws an Error when filter failed.
     */
    ERROR_FILTER ( "Error for filter '{0}'." ) ,

    /**
     * The string representation of all the illegal characters.
     */
    ILLEGALCHARACTERS ( "[]~$^&/\\(){}<>+=`!#%?,:;'\"@" ) ,

    /**
     * The static field used when throws an Error when a character is invalid.
     */
    INVALID_CHARS ( "Channels can not contain any of the following characters : []~$^&/\\(){}<>+=`!#%?,:;'\"@" ) ,

    /**
     * The static field used when throws an Error when the length of one character is invalid.
     */
    INVALID_LENGTH ( "Channels must be at least one character in length." ) ,

    /**
     * The static field used when throws an Error when the specified target is invalid.
     */
    INVALID_TARGET ( "Log, Invalid target specified." ) ;

    private final String value ;

    LoggerStrings(String value )
    {
        this.value = value ;
    }

    /**
     * Returns the String representation of the object.
     * @return the String representation of the object.
     */
    public String toString()
    {
        return value;
    }
}
