package system.logging;

/**
 * Represents the log information for a single logging notification.
 * The logging system dispatches a single message each time a process requests information be logged.
 * This entry can be captured by any object for storage or formatting.
 */
public class LoggerEntry
{
    /**
     * Creates a new LoggerEntry.
     * @param message The context or message of the log.
     * @param level The level of the log.
     * @param channel The channel of the log message.
     */
    public LoggerEntry( Object message , LoggerLevel level , String channel )
    {
        this.channel = channel ;
        this.message = message ;
        this.level   = (level == null) ? LoggerLevel.ALL : level ;
    }

    public LoggerEntry( Object message , LoggerLevel level )
    {
        this( message , level , null ) ;
    }

    public LoggerEntry( Object message )
    {
        this( message , null , null ) ;
    }

    public LoggerEntry()
    {
        this( null , null , null ) ;
    }

    /**
     * Provides access to the channel for this log entry.
     */
    public String channel ;

    /**
     * Provides access to the level for this log entry.
     */
    public LoggerLevel level ;

    /**
     * Provides access to the message that was entry.
     */
    public Object message ;
}
