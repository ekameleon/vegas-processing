package system.logging;

import system.signals.Signal;

/**
 * API for sending log output.
 */
public class Logger extends Signal
{
    private final LoggerEntry _entry = new LoggerEntry() ;

    /**
     * Creates a new Logger instance.
     * @param channel The channel value of the logger.
     */
    public Logger( String channel )
    {
        super( LoggerEntry.class ) ;
        _entry.channel = channel ;
    }

    /**
     * Indicates the channel value for the logger.
     * @return the channel of the logger.
     */
    public String channel()
    {
        return _entry.channel ;
    }

    /**
     * Logs the specified data using the LogEventLevel.CRITICAL level.
     * @param context The information to log. This string can contain special marker characters of the form {x}, where x is a zero based index that will be replaced with the additional parameters found at that index if specified.
     * @param rest Additional parameters that can be substituted in the str parameter at each "{x}" location, where x is an integer (zero based) index value into the Array of values specified.
     */
    public void critical( Object context , Object... rest )
    {
        _log( LoggerLevel.CRITICAL , context , rest ) ;
    }

    /**
     * Logs the specified data using the LogEventLevel.DEBUG level.
     * @param context The information to log. This string can contain special marker characters of the form {x}, where x is a zero based index that will be replaced with the additional parameters found at that index if specified.
     * @param rest Additional parameters that can be substituted in the str parameter at each "{x}" location, where x is an integer (zero based) index value into the Array of values specified.
     */
    public void debug( Object context , Object... rest )
    {
        _log( LoggerLevel.DEBUG , context , rest ) ;
    }

    /**
     * Logs the specified data using the LogEventLevel.ERROR level.
     * @param context The information to log. This string can contain special marker characters of the form {x}, where x is a zero based index that will be replaced with the additional parameters found at that index if specified.
     * @param rest Additional parameters that can be substituted in the str parameter at each "{x}" location, where x is an integer (zero based) index value into the Array of values specified.
     */
    public void error( Object context , Object... rest )
    {
        _log( LoggerLevel.ERROR , context , rest ) ;
    }

    /**
     * Logs the specified data using the LogEvent.INFO level.
     * @param context The information to log. This string can contain special marker characters of the form {x}, where x is a zero based index that will be replaced with the additional parameters found at that index if specified.
     * @param rest Additional parameters that can be substituted in the str parameter at each "{x}" location, where x is an integer (zero based) index value into the Array of values specified.
     */
    public void info( Object context , Object... rest )
    {
        _log( LoggerLevel.INFO , context , rest ) ;
    }

    /**
     * Logs the specified data using the LogEvent.ALL level.
     * @param context The information to log. This string can contain special marker characters of the form {x}, where x is a zero based index that will be replaced with the additional parameters found at that index if specified.
     * @param rest Additional parameters that can be substituted in the str parameter at each "{x}" location, where x is an integer (zero based) index value into the Array of values specified.
     */
    public void log( Object context , Object... rest )
    {
        _log( LoggerLevel.ALL , context , rest ) ;
    }

    /**
     * Logs the specified data using the LogEventLevel.WARN level.
     * @param context The information to log. This string can contain special marker characters of the form {x}, where x is a zero based index that will be replaced with the additional parameters found at that index if specified.
     * @param rest Additional parameters that can be substituted in the str parameter at each "{x}" location, where x is an integer (zero based) index value into the Array of values specified.
     */
    public void warning( Object context , Object... rest )
    {
        _log( LoggerLevel.WARNING , context , rest ) ;
    }

    /**
     * What a Terrible Failure: Report an exception that should never happen.
     * @param context The information to log. This string can contain special marker characters of the form {x}, where x is a zero based index that will be replaced with the additional parameters found at that index if specified.
     * @param rest Additional parameters that can be substituted in the str parameter at each "{x}" location, where x is an integer (zero based) index value into the Array of values specified.
     */
    public void wtf( Object context , Object... rest )
    {
        _log( LoggerLevel.WTF , context , rest ) ;
    }

    /**
     * @private
     */
    private void _log( LoggerLevel level , Object context , Object[] options )
    {
        if( connected() )
        {
            if ( context instanceof String )
            {
                int len = options.length ;
                for( int i = 0 ; i<len ; i++ )
                {
                    context = ((String)context).replaceAll( "\\{" + i + "\\}" , options[i].toString() ) ;
                }
            }
            _entry.message = context ;
            _entry.level   = level ;
            emit( _entry ) ;
        }
    }
}
