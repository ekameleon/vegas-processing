package system.logging;

/**
 * The logger levels that is used within the logging framework.
 */
public enum LoggerLevel
{
    /**
     * Intended to force a target to process all messages (1).
     */
    ALL ( 1 , "ALL" ) ,

    /**
     * Designates events that are very harmful and will eventually lead to application failure (16).
     */
    CRITICAL ( 16 , "CRITICAL" ) ,

    /**
     * Designates informational level messages that are fine grained and most helpful when debugging an application (2).
     */
    DEBUG ( 2 , "DEBUG" ) ,

    /**
     * Designates error events that might still allow the application to continue running (8).
     */
    ERROR ( 8 , "ERROR" ) ,

    /**
     * Designates informational messages that highlight the progress of the application at coarse-grained level (4).
     */
    INFO ( 4 , "INFO" ) ,

    /**
     * A special level that can be used to turn off logging (0).
     */
    NONE ( 0 , "NONE" ) ,

    /**
     * Designates events that could be harmful to the application operation (6).
     */
    WARNING ( 6 , "WARNING" ) ,

    /**
     * What a Terrible Failure: designates an exception that should never happen. (32).
     */
    WTF ( 32 , "WTF" );

    private String _name  ;
    private int    _value ;

    LoggerLevel( int value , String name )
    {
        _name  = name ;
        _value = value ;
    }

    /**
     * Returns the String representation of the object.
     * @return the String representation of the object.
     */
    public String getName()
    {
        return _name ;
    }

    /**
     * Returns the value of the object.
     * @return the value of the object.
     */
    public int getValue()
    {
        return _value ;
    }
}
