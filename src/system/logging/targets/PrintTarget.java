package system.logging.targets;

import system.logging.LoggerLevel;

/**
 * The basic System.out.println log target.
 * <p>Example: </p>
 * <pre>
 *  import system.logging.targets.PrintTarget ;
 *  import system.logging.Log ;
 *  import system.logging.Logger ;
 *  import system.logging.LoggerLevel ;
 *
 *  public class PrintTargetExample
 *  {
 *     public static void main(String [ ] args)
 *     {
 *         PrintTarget target = new PrintTarget() ;
 *
 *         target.setFilters( new String[]{ "system.logging" } ) ;
 *
 *         target.useColor = true ; // only into the JAVA console
 *         target.useDark  = true ; // only into the JAVA console
 *
 *         target.setLevel( LoggerLevel.ALL ) ;
 *
 *         target.includeDate    = true ;
 *         target.includeTime    = true ;
 *         target.includeLevel   = true ;
 *         target.includeChannel = true ;
 *         target.includeLines   = true ;
 *
 *         Logger logger = Log.getLogger("system.logging") ;
 *
 *         logger.debug    ( "hello {0} {1}" , "debug"    , "!" ) ;
 *         logger.info     ( "hello {0} {1}" , "info"     , "!" ) ;
 *         logger.warning  ( "hello {0} {1}" , "warning"  , "!" ) ;
 *         logger.error    ( "hello {0} {1}" , "error"    , "!" ) ;
 *         logger.critical ( "hello {0} {1}" , "critical" , "!" ) ;
 *         logger.wtf      ( "hello {0} {1}" , "wtf"      , "!" ) ;
 *      }
 *  }
 * </pre>
 */
public class PrintTarget extends LineFormattedTarget
{
    /**
     * Creates a new PrintTarget instance.
     */
    public PrintTarget()
    {
        super() ;
    }

    public static final String ANSI_RESET  = "\u001B[0m";
    public static final String ANSI_BLACK  = "\u001B[30m";
    public static final String ANSI_RED    = "\u001B[31m";
    public static final String ANSI_GREEN  = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE   = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN   = "\u001B[36m";
    public static final String ANSI_WHITE  = "\u001B[37m";

    /**
     * Indicates if the target use colors.
     */
    public boolean useColor = false ;

    /**
     * Indicates if the default color is dark (only if the useColor property is true).
     */
    public boolean useDark = false ;

    @Override
    public void internalLog( Object message, LoggerLevel level )
    {
        if( useColor )
        {
            String prefix = "" ;

            if( level == LoggerLevel.DEBUG )
            {
                prefix = ANSI_BLUE ;
            }
            else if ( level == LoggerLevel.INFO )
            {
                prefix = ANSI_GREEN ;
            }
            else if ( level == LoggerLevel.WARNING )
            {
                prefix = ANSI_YELLOW ;
            }
            else if ( level == LoggerLevel.ERROR )
            {
                prefix = ANSI_RED ;
            }
            else if ( level == LoggerLevel.CRITICAL )
            {
                prefix = ANSI_PURPLE ;
            }
            else if ( level == LoggerLevel.WTF )
            {
                prefix = ANSI_CYAN ;
            }
            else
            {
                prefix = useDark ? ANSI_BLACK : ANSI_WHITE ;
            }

            System.out.println( prefix + message + ANSI_RESET );
        }
        else
        {
            System.out.println( message );
        }
    }
}
