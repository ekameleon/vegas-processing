package system.logging.targets;

import system.logging.LoggerEntry;
import system.logging.LoggerLevel;
import system.logging.LoggerTarget;

import java.util.Calendar;

public abstract class LineFormattedTarget extends LoggerTarget
{
    /**
     * Creates a new LineFormattedTarget instance.
     */
    public LineFormattedTarget()
    {
        super();
    }

    /**
     * Indicates if the channel for this target should added to the trace.
     */
    public boolean includeChannel ;

    /**
     * Indicates if the date should be added to the trace.
     */
    public boolean includeDate ;

    /**
     * Indicates if the level for the event should added to the trace.
     */
    public boolean includeLevel ;

    /**
     * Indicates if the line for the event should added to the trace.
     */
    public boolean includeLines ;

    /**
     * Indicates if the milliseconds should be added to the trace. Only relevant when includeTime is <code class="prettyprint">true</code>.
     */
    public boolean includeMilliseconds ;

    /**
     * Indicates if the time should be added to the trace.
     */
    public boolean includeTime ;

    /**
     * The separator string.
     */
    public String separator = " " ;


    /**
     * Descendants of this class should override this method to direct the specified message to the desired output.
     * @param message String containing preprocessed log message which may include time, date, channel, etc.
     * based on property settings, such as <code>includeDate</code>, <code>includeChannel</code>, etc.
     * @param level The LoggerLevel of the current log.
     */
    public abstract void internalLog( Object message , LoggerLevel level ) ;


    @Override
    public void logEntry( LoggerEntry entry )
    {
        String message = formatMessage
        (
                entry.message,
                entry.level.toString() ,
                entry.channel ,
                Calendar.getInstance()
        ) ;
        internalLog( message , entry.level ) ;
    }

    /**
     * Resets the internal line number value (set to 1).
     */
    public void resetLineNumber()
    {
        _lineNumber = 1 ;
    }

    /**
     * This method format the channel of the target.
     * @param channel The channel to format.
     * @return The formatted channel expression.
     */
    public String formatChannel( String channel )
    {
        return "[" + channel + "]" ;
    }

    /**
     * This method format the passed Calendar reference in arguments.
     * @param calendar The Calendar reference to format.
     * @return The formatted date expression.
     */
    public String formatDate( Calendar calendar )
    {
        String str = "" ;
        str  += calendar.get(Calendar.YEAR);
        str  += "/" + getDigit(calendar.get(Calendar.MONTH) + 1) ;
        str  += "/" + getDigit( calendar.get(Calendar.DAY_OF_WEEK)) ;
        return str  ;
    }

    /**
     * This method format the passed level in arguments.
     * @param level The level expression to format.
     * @return The formatted level expression.
     */
    public String formatLevel( String level )
    {
        return '[' + level + ']' ;
    }

    /**
     * This method format the current line value.
     * @return The formatted lines expression.
     */
    public String formatLines()
    {
        return "[" + _lineNumber++ + "]" ;
    }

    /**
     * This method format the log message.
     * @param message The message to log.
     * @param level The level of the message.
     * @param channel The channel to format.
     * @param calendar The calendar reference to format.
     * @return The formatted message expression.
     */
    public String formatMessage( Object message , String level , String channel , Calendar calendar )
    {
        String msg = "" ;
        if (includeLines)
        {
            msg += formatLines() + separator ;
        }
        if (includeDate)
        {
            msg += formatDate( calendar ) + separator ;
        }
        if (includeTime)
        {
            msg += formatTime( calendar ) + separator ;
        }
        if (includeLevel)
        {
            msg += formatLevel( level ) + separator ;
        }
        if ( includeChannel )
        {
            msg += formatChannel(channel) + separator ;
        }
        msg += message.toString() ;
        return msg ;
    }

    /**
     * This method format the current Date passed in argument.
     * @param calendar The Calendar reference to format.
     * @return The formatted time expression.
     */
    public String formatTime( Calendar calendar )
    {
        String time= "" ;
        time += getDigit(calendar.get(Calendar.HOUR_OF_DAY)) ;
        time += ":" + getDigit(calendar.get(Calendar.MINUTE)) ;
        time += ":" + getDigit(calendar.get(Calendar.SECOND)) ;
        if( includeMilliseconds )
        {
            time += ":" + getDigit( calendar.get(Calendar.MILLISECOND) ) ;
        }
        return time ;
    }

    /**
     * Returns the string representation of a number and use digit conversion.
     * @return the string representation of a number and use digit conversion.
     * @param value The numeric value to format.
     */
    public String getDigit( int value )
    {
        return ((value < 10) ? "0" : "") + value ;
    }

    /**
     * @private
     */
    private int _lineNumber = 1 ;
}
