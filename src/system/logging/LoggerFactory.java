package system.logging;

import system.exceptions.InvalidChannelException;

import java.util.*;

import core.strings ;
/**
 * This factory provides pseudo-hierarchical logging capabilities with multiple format and output options.
 * <p>This class in an internal class in the package system.logging you can use the Log singleton to deploy all the loggers in your application.</p>
 */
public class LoggerFactory
{
    private HashMap<String,Logger> _loggers = new HashMap<String,Logger>() ;

    private LoggerLevel _targetLevel = LoggerLevel.NONE ;

    private ArrayList<LoggerTarget> _targets = new ArrayList<LoggerTarget>() ;

    /**
     * Creates a new LoggerFactory instance.
     */
    public LoggerFactory() {}

    /**
     * Allows the specified target to begin receiving notification of log events.
     * @param target The specific target that should capture log events.
     */
    public void addTarget( LoggerTarget target )
    {
        String[] filters = target.getFilters() ;

        for ( Map.Entry<String, Logger> entry : _loggers.entrySet() )
        {
            String channel = entry.getKey() ;
            Logger logger  = entry.getValue() ;
            if( _channelMatchInFilterList( channel, filters ) )
            {
                target.addLogger( logger ) ;
            }
        }

        _targets.add( target ) ;

        if ( ( _targetLevel == LoggerLevel.NONE ) || ( target.getLevel().getValue() < _targetLevel.getValue() ) )
        {
            _targetLevel = target.getLevel() ;
        }
    }

    /**
     * This method removes all of the current loggers from the cache of the factory.
     * Subsquent calls to the <code class="prettyprint">getLogger()</code> method return new instances of loggers rather than any previous instances with the same category.
     * This method is intended for use in debugging only.
     */
    public void flush()
    {
        _loggers.clear() ;
        _targets.clear() ;
        _targetLevel = LoggerLevel.NONE ;
    }

    /**
     * The internal private singleton reference.
     */
    private static LoggerFactory __instance__ ;

    /**
     * Returns the singleton reference of the class.
     * @return the singleton reference of the class.
     */
    public static LoggerFactory getInstance()
    {
        if( __instance__ == null )
        {
            __instance__ = new LoggerFactory() ;
        }
        return __instance__ ;
    }

    /**
     * Returns the logger associated with the specified channel.
     * If the category given doesn't exist a new instance of a logger will be returned and associated with that channel.
     * Channels must be at least one character in length and may not contain any blanks or any of the following characters:
     * []~$^&amp;\/(){}&lt;&gt;+=`!#%?,:;'"&#64;
     * This method will throw an <code class="prettyprint">InvalidChannelError</code> if the category specified is malformed.
     * @param channel The channel of the logger that should be returned.
     * @return An instance of a logger object for the specified name. If the name doesn't exist, a new instance with the specified name is returned.
     */
    public Logger getLogger( String channel )
    {
        _checkChannel( channel ) ;

        Logger result ;

        if( ! _loggers.containsKey( channel ) )
        {
            result = new Logger( channel ) ;
            _loggers.put( channel , result ) ;
        }
        else
        {
            result = _loggers.get(channel) ;
        }

        LoggerTarget target ;

        int len = _targets.size() ;
        for( int i = 0 ; i<len ; i++ )
        {
            target = _targets.get(i) ;
            if( _channelMatchInFilterList( channel , target.getFilters() ) )
            {
                target.addLogger(result);
            }
        }
        return result ;
    }

    /**
     * This method checks the specified string value for illegal characters.
     * @param value The String to check for illegal characters. The following characters are not valid: []~$^&amp;\/(){}&lt;&gt;+=`!#%?,:;'"&#64;
     * @return <code class="prettyprint">true</code> if there are any illegal characters found, <code class="prettyprint">false</code> otherwise.
     */
    public boolean hasIllegalCharacters( String value )
    {
        return strings.indexOfAny( value , LoggerStrings.ILLEGALCHARACTERS.toString() ) != -1 ;
    }

    /**
     * Indicates whether a 'all' level log event will be processed by a log target.
     * @return true if a 'all' level log event will be logged; otherwise false.
     */
    public boolean isAll()
    {
        return _targetLevel.getValue() == LoggerLevel.ALL.getValue() ;
    }

    /**
     * Indicates whether a debug level log event will be processed by a log target.
     * @return true if a debug level log event will be logged; otherwise false.
     */
    public boolean isDebug()
    {
        return _targetLevel.getValue() <= LoggerLevel.DEBUG.getValue() ;
    }

    /**
     * Indicates whether an error level log event will be processed by a log target.
     * @return true if an error level log event will be logged; otherwise false.
     */
    public boolean isError()
    {
        return _targetLevel.getValue() <= LoggerLevel.ERROR.getValue() ;
    }

    /**
     * Indicates whether a critical level log event will be processed by a log target.
     * @return true if a critical level log event will be logged; otherwise false.
     */
    public boolean isCritical()
    {
        return _targetLevel.getValue() <= LoggerLevel.CRITICAL.getValue() ;
    }

    /**
     * Indicates whether an info level log event will be processed by a log target.
     * @return true if an info level log event will be logged; otherwise false.
     */
    public boolean isInfo()
    {
        return _targetLevel.getValue() <= LoggerLevel.INFO.getValue() ;
    }

    /**
     * Indicates whether a warn level log event will be processed by a log target.
     * @return true if a warn level log event will be logged; otherwise false.
     */
    public boolean isWarn()
    {
        return _targetLevel.getValue() <= LoggerLevel.WARNING.getValue() ;
    }

    /**
     * Indicates whether a wtf level log event will be processed by a log target.
     * @return true if a wtf level log event will be logged; otherwise false.
     */
    public boolean isWtf()
    {
        return _targetLevel.getValue() <= LoggerLevel.WTF.getValue() ;
    }

    /**
     * Stops the specified target from receiving notification of log events.
     * @param target The specific target that should capture log events.
     */
    public void removeTarget( LoggerTarget target )
    {
        String[] filters = target.getFilters() ;

        for ( Map.Entry<String, Logger> entry : _loggers.entrySet())
        {
            String channel = entry.getKey() ;
            Logger logger  = entry.getValue() ;
            if( _channelMatchInFilterList( channel, filters ) )
            {
                target.removeLogger( logger ) ;
            }
        }

        Iterator<LoggerTarget> it = _targets.iterator() ;

        while( it.hasNext() )
        {
            LoggerTarget next = it.next() ;
            if( target == next )
            {
                it.remove();
            }
        }

        _resetTargetLevel() ;
    }

    /**
     * This method checks that the specified category matches any of the filter expressions provided in the <code>filters</code> Array.
     * @param channel The channel to match against.
     * @param filters A list of Strings to check category against.
     * @return <code>true</code> if the specified category matches any of the filter expressions found in the filters list, <code>false</code> otherwise.
     */
    private boolean _channelMatchInFilterList( String channel , String[] filters )
    {
        if( channel == null || channel == "" )
        {
            return false ;
        }
        String filter ;
        int index = -1;
        int len = filters.length ;
        for( int i = 0 ; i<len ; i++ )
        {
            filter = filters[i] ;
            index  = filter.indexOf("*") ;

            if( index == 0 )
            {
                return true ;
            }
            else if ( index < 0 )
            {
                index = channel.length() ;
            }
            else
            {
                index = index - 1 ;
            }

            if( channel.substring( 0 , index).equals( filter.substring( 0 , index ) ) )
            {
                return true ;
            }
        }
        return false ;
    }

    /**
     * This method will ensure that a valid category string has been specified.
     * If the category is not valid an <code class="prettyprint">InvalidCategoryError</code> will be thrown.
     * Categories can not contain any blanks or any of the following characters: []`*~,!#$%^&amp;()]{}+=\|'";?&gt;&lt;./&#64; or be less than 1 character in length.
     */
    private void _checkChannel( String channel)
    {
        if( channel == null || channel == "" )
        {
            throw new InvalidChannelException( LoggerStrings.INVALID_LENGTH.toString() );
        }
        if( hasIllegalCharacters( channel ) || ( channel.indexOf("*") != -1 ) )
        {
            throw new InvalidChannelException( LoggerStrings.INVALID_CHARS.toString() ) ;
        }
    }

    /**
     * This method resets the Log's target level to the most verbose log level for the currently registered targets.
     */
    private void _resetTargetLevel()
    {
        LoggerTarget target ;
        LoggerLevel min = LoggerLevel.NONE ;
        int len = _targets.size() ;
        for ( int i = 0 ; i < len ; i++ )
        {
            target = _targets.get(i) ;
            if ( ( min == LoggerLevel.NONE ) || ( target.getLevel().getValue() < min.getValue() ) )
            {
                min = target.getLevel() ;
            }
        }
        _targetLevel = min ;
    }
}
