package system.logging;

import core.strings ;
import java.util.HashMap;
import java.util.Set;

import system.exceptions.InvalidFilterException;

/**
 * This class provides the basic functionality required by the logging framework for a logger target implementation.
 * It handles the validation of filter expressions and provides a default level property.
 */
public abstract class LoggerTarget
{
    /**
     * Creates a new LoggerTarget instance.
     */
    public LoggerTarget()
    {
        _level   = LoggerLevel.ALL;
        _factory = LoggerFactory.getInstance() ;
        _filters = new HashMap<String,Boolean>();
        _filters.put( "*" , PRESENT ) ;
        setFactory( LoggerFactory.getInstance() ) ;
    }

    // ---------- Getters/Setters

    /**
     * Determinates the LoggerFactory reference of the target, by default the target use the <code>system.logging.Log</code> singleton.
     * @return the LoggerFactory reference of the target.
     */
    public LoggerFactory getFactory()
    {
        return _factory ;
    }

    /**
     * Sets the ObjectFactory of this target instance.
     * @param factory The LoggerFactory reference.
     */
    public void setFactory( LoggerFactory factory )
    {
        if ( _factory != null )
        {
            _factory.removeTarget( this ) ;
        }
        _factory = (factory != null) ? factory : LoggerFactory.getInstance() ;
        _factory.addTarget( this ) ;
    }

    /**
     * Indicates the filters array representation of this target.
     * @return The array representation of all filters of this target.
     */
    public String[] getFilters()
    {
        Set<String> set = _filters.keySet() ;
        return set.toArray(new String[set.size()]) ;
    }

    /**
     * Sets the the filters array representation of this target.
     * @param values The filters to register.
     */
    public void setFilters( String[] values )
    {
        int size = values.length ;

        if ( size > 0 )
        {
            for( int i = 0 ; i<size ; i++ )
            {
                _checkFilter( values[i] ) ;
            }
        }

        if ( _count > 0 )
        {
            _factory.removeTarget( this ) ;
        }

        _filters.clear() ;

        if( size > 0 )
        {
            for (int i = 0 ; i<size ; i++)
            {
                _filters.put(values[i], PRESENT);
            }
        }
        else
        {
            _filters.put( "*" , PRESENT ) ;
        }

        if ( _count > 0 )
        {
            _factory.addTarget( this ) ;
        }
    }

    /**
     * Indicates the level of this target.
     * @return the level of this target.
     */
    public LoggerLevel getLevel()
    {
        return _level ;
    }

    /**
     * Sets the LoggerLevel of this target.
     * @param value The LoggerLevel reference value.
     */
    public void setLevel( LoggerLevel value )
    {
        _factory.removeTarget( this ) ;
        _level = (value == null) ? LoggerLevel.ALL : value;
        _factory.addTarget( this ) ;
    }

    /**
     * Insert a channel in the fllters if this channel don't exist.
     * @param channel The channel to register.
     * @return <code>true</code> boolean if the channel is added in the list.
     */
    public boolean addFilter( String channel )
    {
        if ( channel != "" && !_filters.containsKey(channel) )
        {
            _checkFilter( channel ) ;
            _filters.put(channel,PRESENT) ;
            return true ;
        }
        else
        {
            return false ;
        }
    }

    /**
     * Sets up this target with the specified logger.
     * Note : this method is called by the framework and should not be called by the developer.
     * @param logger The logger to register.
     */
    public void addLogger( Logger logger )
    {
        logger.connect( this , "receive" ) ;
        _count++ ;
    }

    /**
     * Clear all filters and use only the wildcard filter "*".
     * @return <code>true</code> boolean if all custom filters are cleared.
     */
    public boolean clearFilter()
    {
        if ( _filters.size() >  0 )
        {
            _filters.clear() ;
            _filters.put( "*" , PRESENT ) ;
            return true ;
        }
        else
        {
            return false ;
        }
    }

    /**
     *  This method receive a <code class="prettyprint">LoggerEntry</code> from an associated logger.
     *  A target uses this method to translate the event into the appropriate format for transmission, storage, or display.
     *  This method will be called only if the event's level is in range of the target's level.
     *  <b><i>Descendants need to override this method to make it useful.</i></b>
     *  @param entry The LoggerEntry to display.
     */
    public abstract void logEntry( LoggerEntry entry );

    /**
     * Receive a new LoggerEntry.
     * @param entry The LoggerEntry reference.
     */
    public void receive( LoggerEntry entry )
    {
        if ( _level == LoggerLevel.NONE )
        {
            return ; // logging off
        }
        else if ( entry.level.getValue() >= _level.getValue() )
        {
            logEntry( entry ) ;
        }
    }

    /**
     * Remove a channel in the fllters if this channel exist.
     * @param channel The channel filter to unregistered.
     * @return a boolean if the channel is removed.
     */
    public boolean removeFilter( String channel )
    {
        if ( _filters.containsKey(channel) )
        {
            _filters.remove( channel ) ;
            return true ;
        }
        else
        {
            return false ;
        }
    }

    /**
     * Stops this target from receiving events from the specified logger.
     * @param logger the Logger to unregister.
     */
    public void removeLogger( Logger logger )
    {
        if( logger.connect( this , "receive") )
        {
            logger.disconnect(this, "receive");
            _count--;
        }
    }

    // ------ Private

    /**
     * Count of the number of loggers this target is listening to.
     * When this value is zero changes to the filters property shouldn't do anything.
     */
    private int _count = 0 ;

    private LoggerFactory _factory ;

    private HashMap<String,Boolean> _filters ;

    private LoggerLevel _level ;

    private static Boolean PRESENT = true ;

    private void _checkFilter( String filter )
    {
        if ( filter == null )
        {
            throw new InvalidFilterException( LoggerStrings.EMPTY_FILTER.toString()  ) ;
        }

        if ( _factory.hasIllegalCharacters( filter ) )
        {
            throw new InvalidFilterException( strings.fastformat( LoggerStrings.ERROR_FILTER.toString() , filter ) + LoggerStrings.CHARS_INVALID.toString() ) ;
        }

        int index = filter.indexOf("*") ;
        if ( (index >= 0) && ( index != (filter.length() -1) ) )
        {
            throw new InvalidFilterException( strings.fastformat( LoggerStrings.ERROR_FILTER.toString() , filter ) + LoggerStrings.CHAR_PLACEMENT.toString() ) ;
        }
    }
}
