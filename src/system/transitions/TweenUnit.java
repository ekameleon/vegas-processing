package system.transitions;

import core.easings.Easing;
import core.easings.Linear;

/**
 * The TweenUnit class interpolate in time a value between <code>0</code> and <code>1</code>.
 * <p>Example : </p>
 * <pre>
 * {@code
 * import core.easings.Elastic;
 * import system.transitions.TweenUnit;
 *
 * public class Vegas
 * {
 *     public static void main( String [ ] args )
 *     {
 *         Vegas application = new Vegas() ;
 *         application.init() ;
 *     }
 *
 *     public void init()
 *     {
 *         Console finish   = new Console("finish") ;
 *         Console change   = new Console("change") ;
 *         Console start    = new Console("start") ;
 *
 *         TweenUnit tween = new TweenUnit( Elastic.easeOut , 30 ) ;
 *
 *         // tween.setFPS( 24 )
 *         //      .setUseSeconds( true )
 *         //      .setDuration( 2 ) ;
 *         //      .setEasing( Linear.ease );
 *
 *         tween.finishIt().connect( finish , "receive" ) ;
 *         tween.changeIt().connect( change , "receive" ) ;
 *         tween.startIt().connect( start , "receive" ) ;
 *
 *         tween.run() ;
 *     }
 *
 *     public class Console
 *     {
 *         String name ;
 *
 *         public Console( String name )
 *         {
 *             this.name = name ;
 *         }
 *
 *         public void receive( Object object )
 *         {
 *             TweenUnit tween = (TweenUnit) object ;
 *             System.out.println( this + " tween:" + tween + " position:" + tween.position() ); ;
 *         }
 *
 *         public String toString()
 *         {
 *             return "[Console " + name + "]" ;
 *         }
 *     }
 * }
 * }
 * </pre>
 */
public class TweenUnit extends Motion implements Cloneable
{
    /**
     * Creates a new TweenUnit instance.
     */
    public TweenUnit()
    {
        this( Linear.ease , 0 , false , false ) ;
    }

    /**
     * Creates a new TweenUnit instance.
     * @param easing The Easing interpolation helper reference.
     */
    public TweenUnit( Easing easing )
    {
        this( easing , 0 , false , false ) ;
    }

    /**
     * Creates a new TweenUnit instance.
     * @param easing The Easing interpolation helper reference.
     * @param duration The duration of the interpolation.
     */
    public TweenUnit( Easing easing , int duration )
    {
        this( easing , duration , false , false ) ;
    }

    /**
     * Creates a new TweenUnit instance.
     * @param easing The Easing interpolation helper reference.
     * @param duration The duration of the interpolation.
     * @param useSeconds Indicates if the tween use the seconds.
     */
    public TweenUnit( Easing easing , int duration , boolean useSeconds )
    {
        this( easing , duration , useSeconds , false ) ;
    }

    /**
     * Creates a new TweenUnit instance.
     * @param easing The Easing interpolation helper reference.
     * @param duration The duration of the interpolation.
     * @param useSeconds Indicates if the tween use the seconds.
     * @param auto Indicates if the tween is autorun.
     */
    public TweenUnit( Easing easing , int duration , boolean useSeconds , boolean auto )
    {
        super() ;
        _easing     = easing == null ? Linear.ease : easing ;
        _duration   = duration ;
        _useSeconds = useSeconds ;
        if( auto )
        {
            run() ;
        }
    }

    private Easing _easing = Linear.ease;
    private float _position = 0 ;

    /**
     * Returns the shallow copy of this object.
     * @return the shallow copy of this object.
     */
    public TweenUnit clone()
    {
        return new TweenUnit( _easing , _duration , _useSeconds ) ;
    }

    /**
     * Returns the easing equation used to interpolate the object.
     * @return the easing equation used to interpolate the object.
     */
    public Easing getEasing()
    {
        return _easing ;
    }

    /**
     * The current position of the interpolation.
     * @return the current position.
     */
    public float position()
    {
        return _position ;
    }

    /**
     * Sets the easing equation used to interpolate the object.
     * @param easing the Easing equation reference.
     * @return the current reference.
     */
    public TweenUnit setEasing( Easing easing )
    {
        _easing = easing ;
        return this ;
    }

    @Override
    protected void update()
    {
        if( _easing != null )
        {
            int limit = _duration ;
            if( _useSeconds )
            {
                limit *= _fps ;
            }
            _position = _easing.calculate( _time, 0 , 1 , limit ) ;
            notifyChanged() ;
        }
        else
        {
            _position = 0 ;
        }
    }
}
