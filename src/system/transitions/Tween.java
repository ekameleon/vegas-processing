package system.transitions;

import core.easings.Easing;
import core.easings.Linear;
import system.reflection.Methods;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * The TweenUnit class interpolate in time a value between <code>0</code> and <code>1</code>.
 * <p>Example : </p>
 * <pre>
 * {@code
 * import core.easings.Elastic;
 * import system.transitions.Tween;
 *
 * public class Vegas
 * {
 *     public static void main( String [ ] args )
 *     {
 *         Vegas application = new Vegas() ;
 *         application.init() ;
 *     }
 *
 *     public Point point = new Point( 10 , 20 , 30 ) ;
 *
 *     public void init()
 *     {
 *         Console finish = new Console("finish") ;
 *         Console change = new Console("change") ;
 *         Console start  = new Console("start") ;
 *
 *         Tween tween = new Tween( point , Elastic.easeOut , 60 ) ;
 *
 *         tween.from("x" , 15 )
 *              .from("z" , 35 ) ; // getter/setter property
 *
 *         tween.to("x" , 100 )
 *              .to("y" , 300 )
 *              .to("z" , 500 ) ; // getter/setter property
 *
 *         tween.finishIt().connect( finish , "receive" ) ;
 *         tween.changeIt().connect( change , "receive" ) ;
 *         tween.startIt().connect( start , "receive" ) ;
 *
 *         tween.run() ;
 *     }
 *
 *     public class Console
 *     {
 *         String name ;
 *
 *         public Console( String name )
 *         {
 *             this.name = name ;
 *         }
 *
 *         public void receive( Object object )
 *         {
 *             System.out.println( this + " point:" + point );
 *         }
 *
 *         public String toString()
 *         {
 *             return "[Console " + name + "]" ;
 *         }
 *     }
 *
 *     public class Point
 *     {
 *         public float x = 0 ;
 *         public float y = 0 ;
 *         private float z = 0 ;
 *
 *         public Point( float x , float y , float z )
 *         {
 *             this.x = x ;
 *             this.y = y ;
 *             this.z = z ;
 *         }
 *
 *         public function getZ() { return z ; }
 *         public function setZ( Float value ) { z = value ; }
 *
 *         public String toString()
 *         {
 *             return "[Point x:" + x + " y:" + y + "]" ;
 *         }
 *     }
 * }
 * }
 * </pre>
 */
public class Tween extends Motion implements Cloneable
{
    /**
     * Creates a new Tween instance.
     */
    public Tween()
    {
        this(null , Linear.ease , 0 , false , false ) ;
    }

    /**
     * Creates a new Tween instance.
     * @param target The target object to interpolate.
     */
    public Tween( Object target )
    {
        this( target , Linear.ease , 0 , false , false ) ;
    }

    /**
     * Creates a new Tween instance.
     * @param target The target object to interpolate.
     * @param easing The Easing interpolation helper reference.
     */
    public Tween( Object target ,  Easing easing )
    {
        this( target , easing , 0 , false , false ) ;
    }

    /**
     * Creates a new Tween instance.
     * @param target The target object to interpolate.
     * @param easing The Easing interpolation helper reference.
     * @param duration The duration of the interpolation.
     */
    public Tween( Object target ,  Easing easing , int duration )
    {
        this( target , easing , duration , false , false ) ;
    }

    /**
     * Creates a new Tween instance.
     * @param target The target object to interpolate.
     * @param easing The Easing interpolation helper reference.
     * @param duration The duration of the interpolation.
     * @param useSeconds Indicates if the tween use the seconds.
     */
    public Tween( Object target , Easing easing , int duration , boolean useSeconds )
    {
        this( target , easing , duration , useSeconds , false ) ;
    }

    /**
     * Creates a new Tween instance.
     * @param target The target object to interpolate.
     * @param easing The Easing interpolation helper reference.
     * @param duration The duration of the interpolation.
     * @param useSeconds Indicates if the tween use the seconds.
     * @param auto Indicates if the tween is autorun.
     */
    public Tween( Object target , Easing easing , int duration , boolean useSeconds , boolean auto )
    {
        super() ;
        _target = target ;
        _easing = easing == null ? Linear.ease : easing ;
        _duration = duration ;
        _useSeconds = useSeconds ;
        if( auto )
        {
            run() ;
        }
    }

    /**
     * Clear the tween settings.
     */
    public void clear()
    {
        _easings.clear() ;
        _from.clear() ;
        _change.clear() ;
        _to.clear() ;
        _changed = true ;
        notifyCleared();
    }

    /**
     * Returns the shallow copy of this object.
     * @return the shallow copy of this object.
     */
    public Tween clone()
    {
        return new Tween( _target , _easing , _duration , _useSeconds ) ;
    }

    /**
     * Determinates the key/value pair to change a register a specific easing over a target property interpolation.
     * @param name the name of the property.
     * @param easing the specific easing apply over a target property interpolation.
     * @return the current reference.
     */
    public Tween easing( String name , Easing easing )
    {
        _easings.put( name , easing ) ;
        _changed = true ;
        return this ;
    }

    /**
     * Determinates the key/value pair to change a specific property over the target.
     * @param name the name of the property.
     * @param value the final value to apply.
     * @return the current reference.
     */
    public Tween from( String name , float value )
    {
        _from.put( name , value ) ;
        _changed = true ;
        return this ;
    }

    /**
     * Determinates the key/value pair to change a specific property over the target.
     * @param name the name of the property.
     * @param value the final value to apply.
     * @return the current reference.
     */
    public Tween from( String name , Float value )
    {
        _from.put( name , value ) ;
        _changed = true ;
        return this ;
    }

    /**
     * Returns the easing equation used to interpolate the object.
     * @return the easing equation used to interpolate the object.
     */
    public Easing getEasing()
    {
        return _easing ;
    }

    /**
     * Returns the target reference.
     * @return the target reference.
     */
    public Object getTarget()
    {
        return _target ;
    }

    /**
     * Sets the easing equation used to interpolate the object.
     * @param easing the Easing equation reference.
     * @return the current reference.
     */
    public Tween setEasing( Easing easing )
    {
        _easing = easing == null ? Linear.ease : easing ;
        return this ;
    }

    /**
     * Sets the target reference.
     * @param target The object to register.
     * @return the current object reference.
     */
    public Tween setTarget( Object target )
    {
        if( target == _target )
        {
            return this ;
        }
        _target = target ;
        _changed = true ;
        return this ;
    }

    /**
     * Determinates the key/value pair to defines a specific final property numeric value.
     * @param name the name of the property.
     * @param value the final value to target at the end of the interpolation.
     * @return the current reference.
     */
    public Tween to( String name , float value )
    {
        _to.put( name , value ) ;
        _changed = true ;
        return this ;
    }

    /**
     * Determinates the key/value pair to defines a specific final property numeric value.
     * @param name the name of the property.
     * @param value the final value to target at the end of the interpolation.
     * @return the current reference.
     */
    public Tween to( String name , Float value )
    {
        _to.put( name , value ) ;
        _changed = true ;
        return this ;
    }

    @Override
    protected void update()
    {
        if( _target == null )
        {
            return ;
        }

        if( _changed )
        {
            _change.clear() ;
            _changed = false ;

            if( _to.size() > 0 )
            {
                Class clazz = _target.getClass();
                for (Map.Entry<String, Float> entry : _to.entrySet())
                {
                    String name   = entry.getKey();
                    Float    to   = entry.getValue();
                    Easing easing = _easings.getOrDefault( name , _easing ) ;
                    try
                    {
                        Field field = clazz.getField(name);
                        if ( _from.containsKey(name) )
                        {
                            field.setFloat( _target , _from.get(name) );
                        }
                        float begin  = field.getFloat(_target);
                        float change = to - begin;
                        _change.put( name , new MemberEntry( field , begin , change , easing ) );

                    }
                    catch ( Exception ignored )
                    {
                        Method get = Methods.findGetter( _target , name ) ;
                        Method set = Methods.findSetter( _target , name , Float.class ) ;
                        if( set == null )
                        {
                            set = Methods.findSetter( _target , name , float.class ) ;
                        }
                        if( get != null && set != null )
                        {
                            if ( _from.containsKey(name) )
                            {
                                Methods.invokeSetter( _target , name , _from.get(name) );
                            }
                            float begin  = (float) Methods.invokeGetter( _target , name ) ;
                            float change = to - begin;
                            _change.put( name , new MemberEntry( set , begin , change , easing ) );
                        }
                    }
                }
            }
        }

        if( _easing != null )
        {
            int limit = _duration ;
            if( _useSeconds )
            {
                limit *= _fps ;
            }
            for ( Map.Entry<String,MemberEntry> entry:_change.entrySet() )
            {
                MemberEntry e = entry.getValue() ;
                float current = e.easing.calculate(_time, e.from, e.change, limit) ;
                if( e.member != null )
                {
                    if( e.member instanceof Field )
                    {
                        try
                        {
                            ((Field)(e.member)).set(_target, current);
                        } catch (IllegalAccessException x) {
                            x.printStackTrace();
                        }
                    }
                    else if ( e.member instanceof Method )
                    {
                        try
                        {
                            ((Method)(e.member)).invoke( _target , current );
                        }
                        catch( IllegalAccessException | InvocationTargetException | IllegalArgumentException x )
                        {
                            x.printStackTrace();
                        }
                    }
                }

            }

            notifyChanged() ;
        }
    }

    private boolean _changed = false ;
    private Easing  _easing  = Linear.ease ;
    private Object  _target  = null ;

    private HashMap<String,Easing>    _easings = new HashMap<>() ;
    private HashMap<String,Float>        _from = new HashMap<>() ;
    private HashMap<String,MemberEntry> _change = new HashMap<>() ;
    private HashMap<String,Float>          _to = new HashMap<>() ;

    private final class MemberEntry
    {
        private MemberEntry( Member member , float begin , float change , Easing easing )
        {
            this.change = change ;
            this.easing = easing ;
            this.from   = begin ;
            this.member = member ;
        }

        private float  change ;
        private Easing easing ;
        private float    from ;
        private Member member ;
    }
}
