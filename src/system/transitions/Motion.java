package system.transitions;

import system.process.Progress;
import system.process.Ticker;

/**
 * The abstract Motion class to creates easing interpolations.
 */
public abstract class Motion extends Transition
{
    public Motion()
    {
        setTicker(null) ;
    }

    public Motion( Object id )
    {
        super(id) ;
        setTicker(null) ;
    }

    protected int   _duration = 0 ;
    protected int        _fps = 60 ;
    protected float     _time = 0 ;

    protected boolean _useSeconds = false ;

    private float _prevTime  = 0 ;
    private long _startTime = 0 ;

    private boolean _stopped = false ;

    private Ticker _ticker ;

    /**
     * Indicates the duration of the tweened animation in frames or seconds (default 0).
     * @return the duration of the tweened animation in frames or seconds (default 0).
     */
    public float duration()
    {
        return _duration ;
    }

    /**
     * Forwards the tweened animation to the next frame.
     */
    public void nextFrame()
    {
        nextFrame( null ) ;
    }

    /**
     * Invoked when the next frame is invoked.
     * @param progress The optional progress argument.
     */
    public void nextFrame( Progress progress )
    {
        setTime( _time + 1 ) ;
    }

    /**
     * Directs the tweened animation to the frame previous to the current frame.
     */
    public void prevFrame()
    {
        setTime( _time - 1 )  ;
    }

    @Override
    public void reset()
    {
        if( running() )
        {
            stop() ;
        }
        _time = 0 ;
    }

    @Override
    public void resume()
    {
        if ( _stopped )
        {
            _stopped = false ;
            fixTime() ;
            notifyResumed() ;
            startInterval() ;
        }
        else
        {
            run() ;
        }
    }

    /**
     * Rewinds a tweened animation to the beginning of the tweened animation.
     * @param time The current time of the animation to rewind.
     */
    public void rewind( float time )
    {
        _time = time > 0 ? time : 0 ;
        fixTime() ;
        update() ;
    }

    /**
     * Rewinds a tweened animation to the beginning of the tweened animation.
     */
    public void rewind()
    {
        rewind(0 ) ;
    }

    @Override
    public void run( Object... args )
    {
        if( !_running )
        {
            if ( _stopped )
            {
                resume();
            }
            else
            {
                _stopped = false ;
                _running = true ;
                rewind();
                notifyStarted();
                startInterval();
            }
        }
    }

    /**
     * Sets the duration of the tweened animation in frames or seconds (default 0).
     * @param value The duration number value.
     * @return The current reference.
     */
    public Motion setDuration( int value )
    {
        _duration = ( value < 0 ) ? 0 : value ;
        return this ;
    }

    /**
     * Sets the duration of the tweened animation in frames or seconds (default 0).
     * @param ticker The Ticker reference.
     */
    public void setTicker( Ticker ticker )
    {
        if ( _ticker != null && _ticker.running() )
        {
            throw new IllegalStateException( this + ".setTicker() failed, this method can't be invoked during the running phase.") ;
        }

        if ( _ticker != null )
        {
            if( _ticker == ticker )
            {
                return ;
            }
            _ticker.progressIt().disconnect( this , "nextFrame" ) ;
        }

        _ticker = ticker == null ? new Ticker() : ticker ;
        _ticker.progressIt().connect( this , "nextFrame" ) ;
    }

    /**
     * Sets the current time within the duration of the animation.
     * @param time  The time value to rewind the motion.
     */
    public void setTime( float time )
    {
        _prevTime = _time ;
        int max = _duration ;
        if( _useSeconds )
        {
            max *= _fps ;
        }

        if ( time > max )
        {
            _time = max ;
            if ( looping )
            {
                rewind(0);
                notifyLooped() ;
            }
            else
            {
                update() ;
                _ticker.reset();
                notifyFinished() ;
            }
        }
        else if ( time < 0 )
        {
            rewind(0) ;
        }
        else
        {
            _time = time ;
            update() ;
        }
    }

    /**
     * Sets the fps value.
     * @param value The boolean flag to indicates if the delay value is in seconds.
     * @throws IllegalStateException if the process is running.
     * @return The current reference.
     */
    public Motion setFPS( int value )
    {
        if ( _running )
        {
            throw new IllegalStateException( this + ".setUseSeconds() failed, this method can't be invoked during the running phase.") ;
        }
        _fps = value ;
        return this ;
    }

    /**
     * Indicates if the tween use seconds.
     * @param value The boolean flag to indicates if the delay value is in seconds.
     * @throws IllegalStateException if the process is running.
     * @return The current reference.
     */
    public Motion setUseSeconds( boolean value )
    {
        if ( _running )
        {
            throw new IllegalStateException( this + ".setUseSeconds() failed, this method can't be invoked during the running phase.") ;
        }
        _useSeconds = value ;
        return this ;
    }

    @Override
    public void start()
    {
        run() ;
    }

    /**
     * Starts the internal interval of the tweened animation.
     */
    private void startInterval()
    {
        _ticker.reset() ;
        _ticker.setDelay( 1000 / _fps ) ;
        _ticker.start();
    }

    @Override
    public void stop()
    {
        if( _running )
        {
            _stopped = true ;
            _running = false;
            _ticker.reset();
            notifyStopped() ;
        }
    }

    /**
     * Indicates true if the timer is stopped.
     * @return <code>true</code> if the process is stopped.
     */
    public boolean stopped()
    {
        return _stopped ;
    }

    /**
     * Returns the current time of the motion interpolation.
     * @return the current time of the motion interpolation.
     */
    public float time()
    {
        if( _useSeconds )
        {
            return (float) Math.floor((System.currentTimeMillis() - _startTime) / 1000) ;
        }
        else
        {
            return _time ;
        }
    }

    /**
     * Indicates the ticker delay value is in seconds.
     * @return <code>true</code> if the ticker delay value is in seconds.
     */
    public boolean useSeconds()
    {
        return _useSeconds ;
    }

    private void fixTime()
    {
        if ( _useSeconds )
        {
            _startTime = System.currentTimeMillis() ;
        }
    }

    /**
     * Update the current object.
     */
    protected abstract void update() ;
}
