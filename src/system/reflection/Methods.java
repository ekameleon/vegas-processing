/*
  Version: MPL 2.0/GPL 2.0/LGPL 2.1

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/

  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.

  The Original Code is [VEGAS framework].

  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com> and Zwetan Kjukov <zwetan@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2003-2018
  the Initial Developers. All Rights Reserved.

  Contributor(s):

  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package system.reflection;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier ;
import java.util.*;

/**
 * The static helper class to check and manipulates the methods.
 */
public class Methods
{
    /**
     * Find a getter method for the give object's property name and try to call it.
     * @param obj The object to check.
     * @param property The name of the setter property to find.
     * @param defaultValue The value returned by the method if the getter method is not valid (default null).
     * @return The return value of the getter method.
     */
    public static Object invokeGetter( Object obj , String property , Object defaultValue )
    {
        Method met = null ;
        String key = String.format( "%s.%s" , obj.getClass().getName() , property );

        if( GETTERS.containsKey(key) )
        {
            met = GETTERS.get(key) ;

        }
        else
        {
            met = findGetter( obj , property ) ;
            GETTERS.put( key , met ) ;
        }

        if( met != null )
        {
            try
            {
                return met.invoke(obj);
            }
            catch( IllegalAccessException | IllegalArgumentException | InvocationTargetException x )
            {
                x.printStackTrace();
            }
        }

        return defaultValue ;
    }

    /**
     * Find a getter method for the give object's property name and try to call it.
     * @param obj The object to check.
     * @param property The name of the setter property to find.
     * @return The return value of the getter method.
     */
    public static Object invokeGetter( Object obj , String property )
    {
        return invokeGetter( obj , property , null ) ;
    }

    /**
     * Find a setter method for the give object's property and try to call it.
     * @param obj The object to check.
     * @param property The name of the setter property to find.
     * @param value The value to apply on the setter method.
     * @return <code>true</code> if the setter method is find and if the value is passed-in.
     */
    public static boolean invokeSetter( Object obj , String property , Object value )
    {
        Method method = null ;

        String key = String.format( "%s.%s(%s)" , obj.getClass().getName(), property, value.getClass().getName() );

        if( SETTERS.containsKey(key) )
        {
            method = SETTERS.get(key) ;

        }
        else
        {
            method = findSetter( obj , property , value ) ;
            SETTERS.put( key , method ) ;
        }

        if( method != null )
        {
            try
            {
                method.invoke(obj, value);
                return true ;
            }
            catch( IllegalAccessException | InvocationTargetException | IllegalArgumentException x )
            {
                x.printStackTrace();
            }
        }

        return false ;
    }

    public static synchronized Method findGetter( Object obj , String property )
    {
        Class<?> clazz = obj.getClass() ;
        String getter = String.format( "get%C%s" , property.charAt(0), property.substring(1) ) ;
        try
        {
            return clazz.getMethod( getter );
        }
        catch ( NoSuchMethodException ignored )
        {
            //
        }
        return null ;
    }

    /**
     * List all the getter and setters methods of the specific class.
     * @param clazz The Class to check
     * @return The list of all getter/setter methods of the specific class.
     */
    public static ArrayList<Method> findGettersSetters( Class<?> clazz )
    {
        ArrayList<Method> list = new ArrayList<Method>() ;
        Method[] methods = clazz.getDeclaredMethods();
        for( Method method : methods )
        {
            if (isGetter(method) || isSetter(method) )
            {
                list.add(method) ;
            }
        }
        return list ;
    }

    public static synchronized Method findSetter( Object obj , String property )
    {
        Class<?> clazz = obj.getClass() ;
        String setter = String.format( "set%C%s" , property.charAt(0) , property.substring(1) ) ;
        try
        {
            return clazz.getMethod( setter );
        }
        catch ( NoSuchMethodException ignored )
        {
            ignored.printStackTrace();
        }
        return null ;
    }

    public static synchronized Method findSetter( Object obj , String property , Class paramType )
    {
        Class<?> clazz = obj.getClass() ;
        String setter = String.format( "set%C%s" , property.charAt(0) , property.substring(1) ) ;
        while( paramType != null )
        {
            try
            {
                return clazz.getMethod( setter , paramType );
            }
            catch ( NoSuchMethodException x )
            {
                for ( Class i : paramType.getInterfaces() )
                {
                    try
                    {
                        return clazz.getMethod( setter , i ) ;
                    }
                    catch( NoSuchMethodException ignored )
                    {

                    }
                }
                paramType = paramType.getSuperclass() ;
            }
        }
        return null ;
    }

    public static synchronized Method findSetter( Object obj , String property , Object value )
    {
        Class<?> clazz = obj.getClass() ;
        String setter = String.format( "set%C%s" , property.charAt(0) , property.substring(1) ) ;
        Class paramType = value.getClass() ;
        while( paramType != null )
        {
            try
            {
                return clazz.getMethod( setter , paramType );
            }
            catch ( NoSuchMethodException x )
            {
                for ( Class i : paramType.getInterfaces() )
                {
                    try
                    {
                        return clazz.getMethod( setter , i ) ;
                    }
                    catch( NoSuchMethodException ignored )
                    {

                    }
                }
                paramType = paramType.getSuperclass() ;
            }
        }
        return null ;
    }

    /**
     * Indicates if the specific method is an accessor method.
     * @param method The method reference to check.
     * @return <code>true</code> if the specific method is a getter method.
     */
    public static boolean isGetter( Method method )
    {
        if ( !Modifier.isPublic( method.getModifiers() ) )
        {
            return false ;
        }
        if( !method.getName().startsWith("get") )
        {
            return false ;
        }
        if( method.getParameterTypes().length != 0 )
        {
            return false ;
        }
        return !void.class.equals(method.getReturnType()) ;
    }

    /**
     * Indicates if the specific method is a mutator method.
     * @param method The method reference to check.
     * @return <code>true</code> if the specific method is a setter method.
     */
    public static boolean isSetter( Method method )
    {
        return Modifier.isPublic(method.getModifiers())
            && method.getName().startsWith("set")
            && method.getParameterTypes().length == 1 ;
    }

    private static HashMap<String,Method> GETTERS = new HashMap<>();
    private static HashMap<String,Method> SETTERS = new HashMap<>();
}