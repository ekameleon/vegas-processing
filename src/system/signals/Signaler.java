/*
  Version: MPL 2.0/GPL 2.0/LGPL 2.1

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/

  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.

  The Original Code is [VEGAS framework].

  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com> and Zwetan Kjukov <zwetan@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2003-2018
  the Initial Developers. All Rights Reserved.

  Contributor(s):

  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package system.signals;

/**
 * The Signaler interface defines the primary method for emit messages.
 */
public interface Signaler
{
    /**
     * Connects a specific object to receive informations with a specific method.
     * @param receiver The Receiver to connect.
     * @return <code>true</code> if the Receiver is connected.
     */
    boolean connect( Receiver receiver ) ;

    /**
     * Connects a specific object to receive informations with a specific method.
     * @param receiver The Receiver to connect.
     * @param priority Determinates the priority level of the receiver (default 0).
     * @return <code>true</code> if the slot is connected.
     */
    boolean connect( Receiver receiver , int priority  ) ;

    /**
     * Connects a specific object to receive informations with a specific method.
     * @param receiver The Receiver to connect.
     * @param priority Determinates the priority level of the receiver (default 0).
     * @param autoDisconnect Apply a disconnect after the first trigger.
     * @return <code>true</code> if the slot is connected.
     */
    boolean connect( Receiver receiver , int priority , boolean autoDisconnect ) ;

    /**
     * Connects a specific object to receive informations with a specific method.
     * @param target The generic object to connect.
     * @param callback the callback method, as a String, to invoke when this signal is emitted.
     * @return <code>true</code> if the slot is connected.
     */
    boolean connect( Object target , String callback ) ;

    /**
     * Connects a specific object to receive informations with a specific method.
     * @param target The generic object to connect.
     * @param callback the callback method, as a String, to invoke when this signal is emitted.
     * @param priority Determinates the priority level of the receiver (default 0).
     * @return <code>true</code> if the slot is connected.
     */
    boolean connect( Object target , String callback , int priority  ) ;

    /**
     * Connects a specific object to receive informations with a specific method.
     * @param target The generic object to connect.
     * @param callback the callback method, as a String, to invoke when this signal is emitted.
     * @param priority Determinates the priority level of the receiver (default 0).
     * @param autoDisconnect Apply a disconnect after the first trigger.
     * @return <code>true</code> if the slot is connected.
     */
    boolean connect( Object target , String callback , int priority , boolean autoDisconnect ) ;

    /**
     * Returns <code>true</code> if one or more slots are connected.
     * @return <code>true</code> if one or more slots are connected.
     */
    boolean connected() ;

    /**
     * Returns <code>true</code> if the specified slot is connected.
     * @param target The generic object connected.
     * @param callback the callback method, as a String, to invoke when this signal is emitted.
     * @return {boolean} <code>true</code> if the specified slot is connected.
     */
    boolean contains( Object target , String callback ) ;

    /**
     * Disconnect the all the slots register in the signal.
     * @return <code>true</code> if the specified slot exist and can be unregister.
     */
    boolean disconnect() ;

    /**
     * Disconnect the passed-in Receiver.
     * @param receiver The Receiver to disconnect.
     * @return <code>true</code> if the specified Receiver exist and can be unregistered.
     */
    boolean disconnect( Receiver receiver ) ;

    /**
     * Disconnect the specified slot.
     * @param target The generic object to disconnect.
     * @param callback the callback method, as a String, to invoke when this signal is emitted.
     * @return <code>true</code> if the specified slot exist and can be unregister.
     */
    boolean disconnect( Object target , String callback ) ;

    /**
     * Emits the specified values to the slots.
     * @param args All values to emit to the slots.
     */
    void emit( Object... args ) ;

    /**
     * The number of receivers or slots register in the signal object.
     * @return The number of slots connected.
     */
    int size();
}
