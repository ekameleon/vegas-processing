/*
  Version: MPL 2.0/GPL 2.0/LGPL 2.1

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/

  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.

  The Original Code is [VEGAS framework].

  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com> and Zwetan Kjukov <zwetan@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2003-2018
  the Initial Developers. All Rights Reserved.

  Contributor(s):

  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package system.signals ;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method ;

import java.util.ArrayList;
import java.util.Arrays ;
import java.util.Collections;
import java.util.Iterator;

/**
 * The Signal object emit messages and notifications.
 * Each receivers object registers directly to the object, no constant event type identifiers are required.
 * <br>
 * <strong>Example:</strong>
 * <pre>
 * import system.signals.Signal ;
 *
 * void setup()
 * {
 *     size(800, 500);
 *     background(100);
 *
 *     Console console1 = new Console( "console1" ) ;
 *     Console console2 = new Console( "console2" ) ;
 *     Console console3 = new Console( "console3" ) ;
 *
 *     Signal signal = new Signal( String.class ) ;
 *
 *     signal.connect( console1 , "receive" ) ;
 *     signal.connect( console2 , "receive" , 1000 , true ) ;
 *     signal.connect( console3 , "receive" ) ;
 *
 *     println("-----") ;
 *
 *     println( "connected: " + signal.connected() ) ;
 *     println( "size: " + signal.size() ) ;
 *
 *     println("-----") ;
 *
 *     signal.emit( "hello" ) ;
 *
 *     println("-----") ;
 *
 *     signal.emit( "world" ) ;
 *
 *     signal.disconnect( console3 , "receive" ) ;
 *
 *     println("-----") ;
 *
 *     println( "contains console1.receive: " + signal.contains( console1 , "receive" ) ) ;
 *     println( "contains console2.receive: " + signal.contains( console2 , "receive" ) ) ;
 *     println( "contains console3.receive: " + signal.contains( console3 , "receive" ) ) ;
 *
 *     println("-----") ;
 *
 *     signal.emit( "!" ) ;
 *
 *     signal.disconnect() ; // disconnect all
 *
 *     println("-----") ;
 *
 *     println( "connected: " + signal.connected() ) ;
 *     println( "size: " + signal.size() ) ;
 *
 *     println("-----") ;
 *
 *     signal.emit( "Finally nothing" ) ;
 * }
 *
 * public class Console
 * {
 *     public String name ;
 *
 *     public Console( String name )
 *     {
 *         this.name = name ;
 *     }
 *
 *     public void receive( String message )
 *     {
 *         println( this.name + " message: " + message ) ;
 *     }
 *
 *     public String toString()
 *     {
 *         return "[Console " + name + "]" ;
 *     }
 * }
 * </pre>
 */
public class Signal implements Signaler
{
    private Class<?>[] parameterTypes;

    private ArrayList<Slot> _receivers = new ArrayList<Slot>();

    /**
     * Creates a new Signal instance.
     * @param parameterTypes the parameter types (as a Class instance) that this signal will dispatch as event data.
     */
    public Signal( Class<?>... parameterTypes )
    {
        this.parameterTypes = parameterTypes;
    }

    /**
     * Connects a specific object to receive informations with a specific method.
     * @param receiver The Receiver to connect.
     * @return <code>true</code> if the Receiver is connected.
     */
    public boolean connect( Receiver receiver )
    {
        return connect( receiver ,"receive" , 0 , false ) ;
    }

    /**
     * Connects a specific object to receive informations with a specific method.
     * @param receiver The Receiver to connect.
     * @param priority Determinates the priority level of the receiver (default 0).
     * @return <code>true</code> if the Receiver is connected.
     */
    public boolean connect( Receiver receiver , int priority  )
    {
        return connect( receiver ,"receive" , priority , false ) ;
    }

    /**
     * Connects a specific object to receive informations with a specific method.
     * @param receiver The Receiver to connect.
     * @param priority Determinates the priority level of the receiver (default 0).
     * @param autoDisconnect Apply a disconnect after the first trigger.
     * @return <code>true</code> if the slot is connected.
     */
    public boolean connect( Receiver receiver , int priority , boolean autoDisconnect )
    {
        return connect( receiver ,"receive" , priority , autoDisconnect ) ;
    }

    /**
     * Connects a specific object to receive informations with a specific method.
     * @param target The generic object to connect.
     * @param callback the callback method, as a String, to invoke when this signal is emitted.
     * @return <code>true</code> if the slot is connected.
     */
    public boolean connect( Object target , String callback )
    {
        return connect( target , callback , 0 , false ) ;
    }

    /**
     * Connects a specific object to receive informations with a specific method.
     * @param target The generic object to connect.
     * @param callback the callback method, as a String, to invoke when this signal is emitted.
     * @param priority Determinates the priority level of the receiver (default 0).
     * @return <code>true</code> if the slot is connected.
     */
    public boolean connect( Object target , String callback , int priority  )
    {
        return connect( target , callback , priority , false ) ;
    }

    /**
     * Connects a specific object to receive informations with a specific method.
     * @param target The generic object to connect.
     * @param callback the callback method, as a String, to invoke when this signal is emitted.
     * @param priority Determinates the priority level of the receiver (default 0).
     * @param autoDisconnect Apply a disconnect after the first trigger.
     * @return <code>true</code> if the slot is connected.
     */
    public boolean connect( Object target , String callback , int priority , boolean autoDisconnect )
    {
        priority = priority > 0 ? (priority - (priority % 1)) : 0 ;

        Method method ;

        try
        {
            method = target.getClass().getMethod( callback , parameterTypes ) ;
        }
        catch (SecurityException e)
        {
            throw new SignalException( "Could not access method `" + target.getClass().getName() + "." + callback + "`" , e );
        }
        catch ( NoSuchMethodException e )
        {
            throw new SignalException( "Could not find method `" + target.getClass().getName() + "." + callback + "`" , e);
        }

        Slot slot ;

        int size = _receivers.size() ;
        if( size > 0 )
        {
            for( int i = 0 ; i<size ; i++ )
            {
                slot = _receivers.get(i) ;
                if( (slot.getTarget() == target) && slot.getMethod().equals(method) )
                {
                    return false ;
                }
            }
        }

        slot = new Slot( target , method , priority , autoDisconnect ) ;

        if( _receivers.contains(slot) )
        {
            return false ;
        }

        _receivers.add( slot ) ;

        Collections.sort( _receivers ) ;

        return true ;
    }

    /**
     * Returns <code>true</code> if one or more slots are connected.
     * @return <code>true</code> if one or more slots are connected.
     */
    public boolean connected()
    {
        return _receivers.size() > 0 ;
    }

    /**
     * Returns <code>true</code> if the specified slot is connected.
     * @param target The generic object connected.
     * @param callback the callback method, as a String, to invoke when this signal is emitted.
     * @return {boolean} <code>true</code> if the specified slot is connected.
     */
    public boolean contains( Object target , String callback )
    {
        if ( _receivers.size() > 0 )
        {
            Method method ;
            try
            {
                method = target.getClass().getMethod( callback , parameterTypes ) ;
            }
            catch (SecurityException e)
            {
                throw new SignalException( "Could not access method `" + target.getClass().getName() + "." + callback + "`" , e );
            }
            catch ( NoSuchMethodException e )
            {
                throw new SignalException( "Could not find method `" + target.getClass().getName() + "." + callback + "`" , e);
            }

            Iterator<Slot> it = _receivers.iterator();
            Slot slot ;
            while ( it.hasNext() )
            {
                slot = it.next() ;
                if ( slot.getTarget() == target && slot.getMethod().equals(method) )
                {
                    return true ;
                }
            }
        }
        return false ;
    }

    /**
     * Disconnect the all the slots register in the signal.
     * @return <code>true</code> if the signal collection is cleared.
     */
    public boolean disconnect()
    {
        if ( _receivers.size() > 0 )
        {
            _receivers.clear() ;
            return true ;
        }
        return false ;
    }

    /**
     * Disconnect the passed-in Receiver.
     * @param receiver The Receiver to disconnect.
     * @return <code>true</code> if the specified Receiver exist and can be unregistered.
     */
    public boolean disconnect( Receiver receiver )
    {
        return disconnect( receiver , "receive" ) ;
    }

    /**
     * Disconnect the specified slot.
     * @param target The generic object to disconnect.
     * @param callback the callback method, as a String, to invoke when this signal is emitted.
     * @return <code>true</code> if the specified slot exist and can be unregistered.
     */
    public boolean disconnect( Object target , String callback )
    {
        if ( _receivers.size() > 0 )
        {
            Method method ;
            try
            {
                method = target.getClass().getMethod( callback , parameterTypes ) ;
            }
            catch (SecurityException e)
            {
                throw new SignalException( "Could not access method `" + target.getClass().getName() + "." + callback + "`" , e );
            }
            catch ( NoSuchMethodException e )
            {
                throw new SignalException( "Could not find method `" + target.getClass().getName() + "." + callback + "`" , e);
            }
            Iterator<Slot> it = _receivers.iterator();
            Slot slot ;
            while ( it.hasNext() )
            {
                slot = it.next() ;
                if ( slot.getTarget() == target && slot.getMethod().equals(method) )
                {
                    it.remove() ;
                    return true ;
                }
            }
        }
        return false ;
    }

    /**
     * Emits the specified values to the slots.
     * @param args All values to emit to the slots.
     */
    public void emit( Object... args )
    {
        int size = _receivers.size() ;

        ArrayList<Slot> copy = (ArrayList<Slot>) _receivers.clone();

        ArrayList<Slot> blacklist = new ArrayList<Slot>() ;

        for( int i = 0 ; i<size ; i++ )
        {
            Slot slot = _receivers.get(i) ;
            if( slot.getAutoDisconnect() )
            {
                blacklist.add( slot ) ;
            }
        }

        int l = blacklist.size() ;
        for( int j = 0 ; j<l ; j++ )
        {
            _receivers.remove( blacklist.get(j) ) ;
        }

        for( int k = 0 ; k<size ; k++ )
        {
            Slot slot = copy.get(k) ;
            try
            {
                slot.getMethod().invoke( slot.getTarget() , args );
            }
            catch( IllegalArgumentException e )
            {
                throw new SignalException( "Method " + slot.getMethod() + " received an invalid argument " + Arrays.deepToString(args) , e );
            }
            catch( IllegalAccessException e )
            {
                throw new SignalException( "Could not access method " + slot.getMethod() , e ) ;
            }
            catch( InvocationTargetException e )
            {
                throw new SignalException( "Could not invoke method " + slot.getMethod() , e ) ;
            }
        }
    }

    /**
     * The number of receivers or slots register in the signal object.
     * @return The number of slots connected.
     */
    public int size()
    {
        return _receivers.size() ;
    }

    /**
     * Returns the String representation of the object.
     * @return The String representation of the object.
     */
    public String toString()
    {
        return "[Signal]" ;
    }
}