/*
  Version: MPL 2.0/GPL 2.0/LGPL 2.1

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/

  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.

  The Original Code is [VEGAS framework].

  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com> and Zwetan Kjukov <zwetan@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2003-2018
  the Initial Developers. All Rights Reserved.

  Contributor(s):

  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package system.signals ;

import java.lang.reflect.Method;

public class Slot implements Comparable<Slot>
{
    /**
     * The auto disconnect flag value.
     */
    private boolean auto ;

    /**
     * The method reference.
     */
    private Method delegate ;

    /**
     * The priority value.
     */
    private int priority ;

    /**
     * The target reference.
     */
    private Object target ;

    /**
     * Creates a new Slot instance.
     * @param target The scope reference.
     * @param delegate The method name.
     */
    public Slot( Object target , Method delegate )
    {
        this( target , delegate , 0 , false ) ;
    }

    /**
     * Creates a new Slot instance.
     * @param target The scope reference.
     * @param delegate The method name.
     * @param priority The priority level of the slot.
     */
    public Slot( Object target , Method delegate , int priority )
    {
        this( target , delegate , priority , false ) ;
    }

    /**
     * Creates a new Slot instance.
     * @param target The scope reference.
     * @param delegate The method name.
     * @param priority The priority level of the slot.
     * @param auto Indicates if the slot is auto disconnect.
     */
    public Slot( Object target , Method delegate , int priority , boolean auto )
    {
        this.auto     = auto ;
        this.delegate = delegate ;
        this.priority = priority ;
        this.target   = target ;
    }

    /**
     * Returns the auto boolean value.
     * @return The auto disconnect boolean flag.
     */
    public boolean getAutoDisconnect()
    {
        return auto ;
    }

    /**
     * Returns the delegate Method reference.
     * @return The method reference of the slot.
     */
    public Method getMethod()
    {
        return delegate ;
    }

    /**
     * Returns the method Object reference.
     * @return The target reference of the slot.
     */
    public Object getTarget()
    {
        return target ;
    }

    /**
     * Compares the object with the other object.
     * @return -1, 0 or 1.
     */
    public int compareTo( Slot entry )
    {
        if ( priority > entry.priority )
        {
            return -1;
        }
        else if ( priority < entry.priority )
        {
            return 1;
        }
        else
        {
            return 0 ;
        }
    }

    /**
     * Returns the String representation of the object.
     * @return The String representation of the object.
     */
    public String toString()
    {
        return "[" + this.getClass().getName() + "]" ;
    }
}