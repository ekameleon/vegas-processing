/*
  Version: MPL 2.0/GPL 2.0/LGPL 2.1

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/

  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.

  The Original Code is [VEGAS framework].

  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com> and Zwetan Kjukov <zwetan@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2003-2018
  the Initial Developers. All Rights Reserved.

  Contributor(s):

  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package system.process;

import java.util.HashMap;

/**
 * Batch a serie of tasks and notify when all actions are finished.
 * <br>
 * <b>Example:</b><br>
 * <pre>
 * import system.process.BatchTask ;
 *
 * void setup()
 * {
 *     size(800, 500);
 *     background(100);
 *
 *     Console finish   = new Console("finish") ;
 *     Console progress = new Console("progress") ;
 *     Console start    = new Console("start") ;
 *
 *     Do command1 = new Do("#1" , 2000 ) ;
 *     Do command2 = new Do("#2" , 5000 ) ;
 *     Do command3 = new Do("#3" , 3000 ) ;
 *
 *     BatchTask batch = new BatchTask() ;
 *
 *     batch.finishIt().connect( finish , "receive" ) ;
 *     batch.progressIt().connect( progress , "receive" ) ;
 *     batch.startIt().connect( start , "receive" ) ;
 *
 *     batch.add( command1 ) ;
 *     batch.add( command2 , 1000 ) ;
 *     batch.add( command3 , 0 , true  ) ;
 *
 *     batch.run() ;
 * }
 * </pre>
 * The "Do" class extends the Task class to create a concrete Action representation.<br>
 * <pre>
 * import system.process.Task ;
 *
 * public class Do extends Task
 * {
 *     public int delay ;
 *     public String name ;
 *
 *     public Do( String name )
 *     {
 *         this( name , 1000 ) ;
 *     }
 *
 *     public Do( String name , int delay )
 *     {
 *         this.name = name ;
 *         this.delay = delay ;
 *     }
 *
 *     public Do clone()
 *     {
 *         return new Do( name , delay ) ;
 *     }
 *
 *     public void run( Object... args )
 *     {
 *         notifyStarted() ;
 *         println( this + " run" ) ;
 *         try
 *         {
 *             Thread.sleep(delay);
 *         }
 *         catch( InterruptedException ex )
 *         {
 *            ex.printStackTrace();
 *         }
 *         notifyFinished() ;
 *     }
 *
 *     public String toString()
 *     {
 *         return "[Do name" + name + " delay:" + delay + "]" ;
 *     }
 * }
 * </pre>
 * The "Console" class is a slot who receive message from signals.<br>
 * <pre>
 * import system.process.BatchTask ;
 * import system.process.Progress ;
 *
 * public class Console
 * {
 *     public String name ;
 *
 *     public Console( String name )
 *     {
 *         this.name = name ;
 *     }
 *
 *     public void receive( Object object )
 *     {
 *         BatchTask task = (BatchTask) object ;
 *         println( this + " task:" + task + " phase:" + task.phase() ) ;
 *     }
 *
 *     public void receive( Progress progress )
 *     {
 *         Do current = (Do) progress.currentTarget ;
 *         BatchTask batch = (BatchTask) progress.target ;
 *         println( this + " batch:" + batch + " phase:" + batch.phase() + " num:" + batch.size() + " current:" + current ) ;
 *     }
 *
 *     public String toString()
 *     {
 *         return "[Console " + name + "]" ;
 *     }
 * }
 * </pre>
 */
public class BatchTask extends TaskGroup
{
    private Action _current = null ;

    private HashMap<Action,ActionEntry> _currents = new HashMap<Action,ActionEntry>();

    /**
     * Creates a new BatchTask instance.
     */
    public BatchTask() {}

    /**
     * Creates a new BatchTask instance.
     * @param id The unique identifier of the object.
     */
    public BatchTask( Object id )
    {
        super(id) ;
    }

    /**
     * Returns the current Action reference.
     * @return the current Action reference.
     */
    public Action current()
    {
        return _current ;
    }

    @Override
    public void next( Object object )
    {
        Action action = (Action) object ;

        boolean flag = false ;
        ActionEntry entry = null ;

        if ( _currents.containsKey( action ) )
        {
            entry = _currents.get( action ) ;

            flag = ( _mode != TaskGroupMode.EVERLASTING )
                && ( ( _mode == TaskGroupMode.TRANSIENT ) || ( entry.getAuto() && (_mode == TaskGroupMode.NORMAL ) ) ) ;

            _currents.remove( action ) ;
        }

        if( flag )
        {
            action.finishIt().disconnect( this , "next" ) ;
            _actions.remove( entry ) ;
        }

        if ( _current != null )
        {
            notifyChanged() ;
        }

        _current = action ;

        Progress info = new Progress() ;

        info.currentTarget = _current ;

        notifyProgress( info ) ;

        if ( _currents.isEmpty() )
        {
            _current = null ;
            notifyFinished() ;
        }
    }

    @Override
    public void reset()
    {
        if( _running )
        {
            stop() ;
        }
        if( _actions.size() > 0 )
        {
            _currents.clear();
            remove();
        }
    }

    @Override
    public void resume()
    {
        if ( _stopped )
        {
            _running = true ;
            _stopped = false ;
            notifyResumed() ;
            if ( _actions.size() > 0 )
            {
                int l = _actions.size() ;
                while( --l > -1 )
                {
                    ActionEntry entry = _actions.get(l) ;
                    Action action = entry.getAction() ;
                    if ( action instanceof Resumable )
                    {
                        Resumable o = (Resumable) action ;
                        o.resume() ;
                    }
                    else
                    {
                        next( action ) ;
                    }
                }
            }
        }
        else
        {
            run() ;
        }
    }

    @Override
    public void start()
    {
        if( !_running )
        {
            run();
        }
    }

    @Override
    public void stop()
    {
        if ( _running )
        {
            if ( _actions.size() > 0 )
            {
                int l = _actions.size();
                while( --l > -1 )
                {
                    ActionEntry e = _actions.get(l) ;
                    Action      a = e.getAction();
                    if ( a instanceof Stoppable )
                    {
                        Stoppable o = (Stoppable) a ;
                        o.stop() ;
                    }
                }
            }
            _running = false ;
            _stopped = true ;
            notifyStopped() ;
        }
    }

    @Override
    public void run( Object... args )
    {
        if ( !_running )
        {
            notifyStarted() ;

            _stopped = false ;
            _current = null ;

            _currents.clear() ;

            int l = _actions.size() ;
            if ( _actions.size() > 0 )
            {
                for( int i = 0 ; i<l ; i++ )
                {
                    ActionEntry entry = _actions.get(i) ;
                    _currents.put( entry.getAction() , entry ) ;
                }

                for( int i = 0 ; i<l ; i++ )
                {
                    ActionEntry entry = _actions.get(i) ;
                    entry.getAction().run() ;
                }
            }
            else
            {
                notifyFinished() ;
            }
        }
    }
}
