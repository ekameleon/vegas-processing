/*
  Version: MPL 2.0/GPL 2.0/LGPL 2.1

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/

  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.

  The Original Code is [VEGAS framework].

  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com> and Zwetan Kjukov <zwetan@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2003-2018
  the Initial Developers. All Rights Reserved.

  Contributor(s):

  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package system.process;

import java.lang.Exception;
import system.signals.Signal;

/**
 * An abstract class to creates complex Action with complex behaviors.
 */
public abstract class AbstractProcess extends Task implements Resetable, Resumable, Startable, Stoppable
{
    public AbstractProcess()
    {
        super() ;
    }

    public AbstractProcess( Object id ) { super(id) ; }

    // ----------- Public Properties

    /**
     * Indicates if the task must be looped.
     */
    public boolean looping = false ;

    /**
     * Indicates if the class throws errors or notify a finished event when the task failed.
     */
    public boolean throwError = false ;

    // ----------- Signals

    private Signal _changeIt   = new Signal( Object.class ) ;
    private Signal _clearIt    = new Signal( Object.class ) ;
    private Signal _errorIt    = new Signal( Object.class , String.class ) ;
    private Signal _infoIt     = new Signal( Object.class , Object.class ) ;
    private Signal _loopIt     = new Signal( Object.class ) ;
    private Signal _pauseIt    = new Signal( Object.class ) ;
    private Signal _progressIt = new Signal( Progress.class ) ;
    private Signal _resumeIt   = new Signal( Object.class ) ;
    private Signal _stopIt     = new Signal( Object.class ) ;
    private Signal _timeoutIt  = new Signal( Object.class ) ;

    /**
     * The signal who emit when the process is changed.
     * @return The signal who emit when the process is changed.
     */
    public Signal changeIt()
    {
        return _changeIt ;
    }

    /**
     * The signal who emit when the process is cleared.
     * @return The signal who emit when the process is cleared.
     */
    public Signal clearIt()
    {
        return _clearIt ;
    }

    /**
     * The signal who emit when the process is failed.
     * @return The signal who emit when the process is failed.
     */
    public Signal errorIt()
    {
        return _errorIt ;
    }

    /**
     * The signal who emit when the process send an information.
     * @return The signal who emit when the process send an information.
     */
    public Signal infoIt()
    {
        return _infoIt ;
    }

    /**
     * The signal who emit when the process is looped.
     * @return The signal who emit when the process is looped.
     */
    public Signal loopIt()
    {
        return _loopIt ;
    }

    /**
     * The signal who emit when the process is paused.
     * @return The signal who emit when the process is paused.
     */
    public Signal pauseIt()
    {
        return _pauseIt ;
    }

    /**
     * The signal who emit when the process is in progress.
     * @return The signal who emit when the process is in progress.
     */
    public Signal progressIt()
    {
        return _progressIt ;
    }

    /**
     * The signal who emit when the process is resumed.
     * @return The signal who emit when the process is resumed.
     */
    public Signal resumeIt()
    {
        return _resumeIt ;
    }

    /**
     * The signal who emit when the process is stopped.
     * @return The signal who emit when the process is stopped.
     */
    public Signal stopIt()
    {
        return _stopIt ;
    }

    /**
     * The signal who emit when the process is timeout.
     * @return The signal who emit when the process is timeout.
     */
    public Signal timeoutIt()
    {
        return _timeoutIt ;
    }

    // ----------- Public Methods

    /**
     * Notify when the process is changed.
     */
    public void notifyChanged()
    {
        if ( !this.__lock__ )
        {
            _changeIt.emit( this ) ;
        }
    }

    /**
     * Notify when the process is cleared.
     */
    public void notifyCleared()
    {
        if ( !this.__lock__ )
        {
            _clearIt.emit( this ) ;
        }
    }

    /**
     * Notify when the process failed.
     * @param message The exception message to notify.
     * @throws Exception if the throwError property is true.
     */
    public void notifyError( String message ) throws Exception
    {
        this._running = false ;
        this._phase = TaskPhase.ERROR ;
        if ( !this.__lock__ )
        {
            _errorIt.emit( this , message ) ;
        }
        if( throwError )
        {
            throw new Exception( message ) ;
        }
    }

    /**
     * Notify when the process send an information.
     * @param info The information object to send.
     */
    public void notifyInfo( Object info )
    {
        if ( !this.__lock__ )
        {
            _infoIt.emit( this , info ) ;
        }
    }

    /**
     * Notify when the process failed.
     */
    public void notifyLooped()
    {
        this._running = true ;
        this._phase = TaskPhase.RUNNING ;
        if ( !this.__lock__ )
        {
            _loopIt.emit( this ) ;
        }
    }

    /**
     * Notify when the process is paused.
     */
    public void notifyPaused()
    {
        this._running = false ;
        this._phase = TaskPhase.STOPPED ;
        if ( !this.__lock__ )
        {
            _pauseIt.emit( this ) ;
        }
    }

    /**
     * Notify when the process is in progress.
     */
    public void notifyProgress()
    {
        this._running = true ;
        this._phase = TaskPhase.RUNNING ;
        if ( !this.__lock__ )
        {
            Progress info = new Progress() ;
            info.target = this ;
            info.currentTarget = this ;
            _progressIt.emit( info ) ;
        }
    }

    /**
     * Notify when the process is in progress.
     * @param progress The Progress object to send.
     */
    public void notifyProgress( Progress progress )
    {
        this._running = true ;
        this._phase = TaskPhase.RUNNING ;
        if ( !this.__lock__ )
        {
            progress.target = this ;
            if( progress.currentTarget == null )
            {
                progress.currentTarget = this ;
            }
            _progressIt.emit( progress ) ;
        }
    }

    /**
     * Notify when the process is in progress.
     * @param loaded The number of items or bytes loaded.
     * @param total The total number of items or bytes that will be loaded if the loading process succeeds.
     */
    public void notifyProgress( float loaded , float total )
    {
        this._running = true ;
        this._phase = TaskPhase.RUNNING ;
        if ( !this.__lock__ )
        {
            Progress info = new Progress( loaded , total ) ;
            info.target = this ;
            info.currentTarget = this ;
            _progressIt.emit( info ) ;
        }
    }

    /**
     * Notify when the process is paused.
     */
    public void notifyResumed()
    {
        this._running = true ;
        this._phase = TaskPhase.RUNNING ;
        if ( !this.__lock__ )
        {
            _resumeIt.emit( this ) ;
        }
    }

    /**
     * Notify when the process is stopped.
     */
    public void notifyStopped()
    {
        this._running = false ;
        this._phase = TaskPhase.STOPPED ;
        if ( !this.__lock__ )
        {
            _stopIt.emit( this ) ;
        }
    }

    /**
     * Notify when the process is timeout.
     */
    public void notifyTimeout()
    {
        this._running = false ;
        this._phase = TaskPhase.TIMEOUT ;
        if ( !this.__lock__ )
        {
            _timeoutIt.emit( this ) ;
        }
    }

    /**
     * Resets the process.
     */
    public abstract void reset();

    /**
     * Resume the process.
     */
    public abstract void resume();

    /**
     * Starts the process.
     */
    public abstract void start();

    /**
     * Stops the process.
     */
    public abstract void stop();
}