/*
  Version: MPL 2.0/GPL 2.0/LGPL 2.1

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/

  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.

  The Original Code is [VEGAS framework].

  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com> and Zwetan Kjukov <zwetan@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2003-2018
  the Initial Developers. All Rights Reserved.

  Contributor(s):

  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package system.process;

import java.util.ArrayList;

/**
 * Enables you to apply a common {@link system.process.Runnable} command to a group of {@link system.process.Runnable} commands.
 * <br>
 * <b>Example:</b>
 * <pre>
 * import system.process.Batch ;
 * import system.process.Runnable ;
 *
 * void setup()
 * {
 *     Command command1 = new Command("command #1") ;
 *     Command command2 = new Command("command #2") ;
 *
 *     Batch batch = new Batch() ;
 *
 *     batch.add( command1 ) ;
 *     batch.add( command2 ) ;
 *     batch.run() ;
 * }
 *
 * public class Command implements Runnable
 * {
 *     public String name ;
 *
 *     public Command( String name )
 *     {
 *         this.name = name ;
 *     }
 *
 *     public void run( Object... args )
 *     {
 *         println( name + " run" ) ;
 *     }
 * }
 * </pre>
 */
public class Batch implements Cloneable, Runnable, Stoppable
{
    private ArrayList<Runnable> _entries = new ArrayList<Runnable>();

    /**
     * Creates a new Batch instance.
     */
    public Batch() {}

    /**
     * Creates a new Batch instance.
     * @param commands The collection who contains a list of commands to batch.
     */
    public Batch( ArrayList<Runnable> commands )
    {
        _entries.addAll( commands ) ;
    }

    /**
     * Adds a new command in the batch.
     * @param command The command to register.
     * @return <code>true</code> If the command is registered.
     */
    public boolean add( Runnable command )
    {
        return _entries.add( command ) ;
    }

    /**
     * Inserts the specified element at the specified position in this batch.
     * @param index  index at which the specified Runnable object is to be inserted
     * @param command the Runnable object to be inserted.
     * @throws IndexOutOfBoundsException Invoked when the index is out of range.
     */
    public void add( int index, Runnable command)
    {
        _entries.add( index , command ) ;
    }

    /**
     * Returns a shallow copy of the object.
     * @return a shallow copy of the object.
     */
    public Batch clone()
    {
        return new Batch( _entries ) ;
    }

    /**
     * Returns the index of the first occurrence of the specified element.
     * in this list, or -1 if this list does not contain the element.
     * More formally, returns the lowest index <tt>i</tt> such that
     * <tt>(o==null&nbsp;?&nbsp;get(i)==null&nbsp;:&nbsp;o.equals(get(i)))</tt>,
     * or -1 if there is no such index.
     * @param  command The Runnable reference to find.
     * @return the index of the first occurrence of the specified element.
     */
    public int indexOf( Runnable command )
    {
        return _entries.indexOf( command );
    }

    /**
     * Returns <tt>true</tt> if this list contains no elements.
     * @return <tt>true</tt> if this list contains no elements.
     */
    public boolean isEmpty()
    {
        return _entries.isEmpty();
    }

    /**
     * Removes a command in the batch.
     * @param command The command to unregister.
     * @return <code>true</code> if the command is unregistered.
     */
    public boolean remove( Runnable command )
    {
        return _entries.remove( command ) ;
    }

    /**
     * Execute the command.
     * @param args A serie of optional arguments.
     */
    public void run( Object... args )
    {
        int size = _entries.size() ;
        if( size > 0 )
        {
            for ( int i = 0 ; i < size ; i++)
            {
                _entries.get(i).run() ;
            }
        }

    }

    /**
     * Retrieves the number of elements in this batch.
     * @return the number of elements in this batch.
     */
    public int size()
    {
        return _entries.size();
    }

    /**
     * Stops all commands (Stoppable) in the Batch.
     */
    @Override
    public void stop()
    {
        int size = _entries.size() ;
        if( size > 0 )
        {
            for ( int i = 0 ; i < size ; i++)
            {
                Runnable command = _entries.get(i);
                if( command instanceof Stoppable )
                {
                    Stoppable o = (Stoppable)command ;
                    o.stop() ;
                }run() ;
            }
        }
    }

    /**
     * Returns the Array representation of the object.
     * @return the number of elements in this batch.
     */
    public Runnable[] toArray()
    {
        return (Runnable[]) _entries.toArray();
    }

    /**
     * Returns the String representation of the object.
     * @return the String representation of the object.
     */
    public String toString()
    {
        return "[Batch]" ;
    }
}
