/*
  Version: MPL 2.0/GPL 2.0/LGPL 2.1

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/

  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.

  The Original Code is [VEGAS framework].

  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com> and Zwetan Kjukov <zwetan@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2003-2018
  the Initial Developers. All Rights Reserved.

  Contributor(s):

  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package system.process;

import java.util.Timer ;
import java.util.TimerTask ;

/**
 * The <code>Ticker</code> objects which let you run code on a specified time sequence.
 * This ticker object use an internal <code>java.util.Timer</code> emitter to calls or evaluates an expression at specified intervals
 * <p><b>Example</b> :</p>
 * <pre>
 * {@code
 * import system.logging.targets.PrintTarget ;
 * import system.logging.Log ;
 * import system.logging.Logger ;
 * import system.logging.LoggerLevel ;
 *
 * import system.process.Ticker ;
 *
 * public class Vegas
 * {
 *     public static void main(String [ ] args)
 *     {
 *         Vegas application = new Vegas() ;
 *         application.init() ;
 *     }
 *
 *     public void init()
 *     {
 *         PrintTarget target = new PrintTarget() ;
 *
 *         target.setFilters( new String[]{ "*" } ) ; // default
 *
 *         target.setLevel( LoggerLevel.ALL ) ;
 *
 *         target.useColor = true ;
 *         target.useDark  = false ;
 *
 *         target.includeDate    = true ;
 *         target.includeTime    = true ;
 *         target.includeLevel   = true ;
 *         target.includeChannel = true ;
 *         target.includeLines   = true ;
 *
 *         Console finish   = new Console("finish") ;
 *         Console loop     = new Console("loop") ;
 *         Console progress = new Console("progress") ;
 *         Console resume   = new Console("resume") ;
 *         Console start    = new Console("start") ;
 *         Console stop     = new Console("stop") ;
 *
 *         Ticker ticker = new Ticker(1000/60 ,true , true );
 *
 *         ticker.setDelay(1000)
 *               .setUseSeconds( false )
 *               .setNumLoop( 10 ) ;
 *
 *         ticker.finishIt().connect( finish , "receive" ) ;
 *         ticker.loopIt().connect( loop , "receive" ) ;
 *         ticker.progressIt().connect( progress , "receive" ) ;
 *         ticker.resumeIt().connect( resume , "receive" ) ;
 *         ticker.startIt().connect( start , "receive" ) ;
 *         ticker.stopIt().connect( stop , "receive" ) ;
 *
 *         ticker.run() ;
 *     }
 *
 *     public static Logger logger = Log.getLogger("system.logging") ;
 * }
 * }
 * </pre>
 * <p>The Console class source code :</p>
 * <pre>
 * {@code
 * import system.process.Progress;
 * import system.process.Ticker;
 *
 * public class Console
 * {
 *     String name ;
 *
 *     public Console( String name )
 *     {
 *         this.name = name ;
 *     }
 *
 *     public void receive( Object object )
 *     {
 *         Ticker ticker = (Ticker) object ;
 *         Vegas.logger.debug( this + " ticker:" + ticker ) ;
 *     }
 *
 *     public void receive( Progress progress )
 *     {
 *         Ticker ticker = (Ticker) progress.target ;
 *         Vegas.logger.debug( this + " ticker:" + ticker + " currentLoop:" + ticker.currentLoop() ) ;
 *         if( ticker.currentLoop() == 4 )
 *         {
 *             ticker.stop() ;
 *             ticker.resume() ;
 *         }
 *     }
 *
 *     public String toString()
 *     {
 *         return "[Console " + name + "]" ;
 *     }
 * }
 * }
 * </pre>
 */
public class Ticker extends AbstractProcess
{
    /**
     * Creates a new Ticker instance with a default delay of 100 ms.
     */
    public Ticker()
    {
        this(100 , false ) ;
    }

    /**
     * Creates a new Ticker instance.
     * @param delay The delay between two intervals when the ticker is running.
     */
    public Ticker( int delay )
    {
        this( delay , false ) ;
    }

    /**
     * Creates a new Ticker instance.
     * @param delay The delay between two intervals when the ticker is running.
     * @param useSeconds Indicates if the delay is in seconds (else milliseconds).
     */
    public Ticker( int delay , boolean useSeconds )
    {
        this( delay , useSeconds , true ) ;
    }

    /**
     * Creates a new Ticker instance.
     * @param delay The delay between two intervals when the ticker is running.
     * @param useSeconds Indicates if the delay is in seconds (else milliseconds).
     * @param looping Indicates if the Ticker is looping.
     */
    public Ticker( int delay , boolean useSeconds , boolean looping )
    {
        this( delay , useSeconds , looping , 0 ) ;
    }

    /**
     * Creates a new Ticker instance.
     * @param delay The delay between two intervals when the ticker is running.
     * @param useSeconds Indicates if the delay is in seconds (else milliseconds).
     * @param looping Indicates if the Ticker is looping.
     * @param numLoop Indicates the number of loops. If zero, the timer repeats infinitely.
     */
    public Ticker( int delay , boolean useSeconds , boolean looping , int numLoop )
    {
        super() ;
        _delay = ( delay > 0 ) ? delay : 0 ;
        _numLoop = (numLoop > 0) ? numLoop : 0 ;
        _useSeconds = useSeconds ;
        _timer = new Timer() ;
        this.looping = looping ;
    }

    /**
     * Returns a shallow copy of the object.
     * @return a shallow copy of the object.
     */
    @Override
    public Ticker clone()
    {
        Ticker copy = new Ticker( _delay , _useSeconds , looping );
        copy.setID( copy.getID() );
        return copy ;
    }

    /**
     * Indicates the delay between timer events, in milliseconds (or seconds it the <code>useSeconds</code> is <code>true</code>).
     * @return the delay between timer events, in milliseconds (or seconds it the <code>useSeconds</code> is <code>true</code>).
     */
    public int delay()
    {
        return _delay ;
    }

    /**
     * Indicates the current count value if the ticker use the <code>looping</code> and <code>numLoop</code> option.
     * @return the current count value if the ticker use the <code>looping</code> and <code>numLoop</code> option.
     */
    public int currentLoop()
    {
        return _currentLoop ;
    }

    /**
     * Indicates the number of repetitions. If zero, the timer repeats infinitely.
     * If nonzero, the timer runs the specified number of times and then stops.
     * @return The number of repetitions of this ticker.
     */
    public int numLoop()
    {
        return _numLoop;
    }

    @Override
    public void reset()
    {
        if( _running )
        {
            stop() ;
        }
        _currentLoop = 0 ;
    }

    @Override
    public void resume()
    {
        if ( _stopped )
        {
            _running = true ;
            _stopped = false ;
            notifyResumed() ;
            _next = new TickerNext( this , _timer ) ;
            _next.timer.schedule( _next, 0, _useSeconds ? (_delay * 1000) : _delay);
        }
        else if( !_running )
        {
            start() ;
        }
    }

    @Override
    public void run( Object... args )
    {
        start() ;
    }

    /**
     * Sets the delay between timer events, in milliseconds (or seconds it the <code>useSeconds</code> is <code>true</code>).
     * @param value The delay between to intervals of the ticker.
     * @throws IllegalStateException if the process is running.
     * @return The current Ticker reference.
     */
    public Ticker setDelay( int value )
    {
        if ( _running )
        {
            throw new IllegalStateException( this + ".setDelay() failed, this method can't be invoked during the running phase.") ;
        }
        _delay = ( value > 0 ) ? value : 0 ;
        return this ;
    }

    /**
     * Sets the number of repetitions. If zero, the timer repeats infinitely.
     * If nonzero, the timer runs the specified number of times and then stops.
     * @param value The number of repetitions of this ticker.
     * @return The current Ticker reference.
     */
    public Ticker setNumLoop( int value )
    {
        _numLoop = (value > 0) ? value : 0 ;
        return this ;
    }

    /**
     * Indicates the ticker delay value is in seconds.
     * @param value The boolean flag to indicates if the delay value is in seconds.
     * @throws IllegalStateException if the process is running.
     * @return The current Ticker reference.
     */
    public Ticker setUseSeconds( boolean value )
    {
        if ( _running )
        {
            throw new IllegalStateException( this + ".setUseSeconds() failed, this method can't be invoked during the running phase.") ;
        }
        _useSeconds = value ;
        return this ;
    }

    @Override
    public void start()
    {
        if( _stopped )
        {
            resume() ;
        }
        else if( !_running )
        {
            _currentLoop = 0;
            _stopped = false;
            notifyStarted();
            _next = new TickerNext( this , _timer ) ;
            _next.timer.schedule( _next, 0, _useSeconds ? (_delay * 1000) : _delay);
        }
    }

    @Override
    public void stop()
    {
        if ( _running )
        {
            _running = false ;
            _stopped = true ;
            if( _next != null )
            {
                _next.cancel();
                _next = null;
            }
            notifyStopped() ;
        }
    }

    /**
     * Indicates true if the timer is stopped.
     * @return <code>true</code> if the process is stopped.
     */
    public boolean stopped()
    {
        return _stopped ;
    }

    /**
     * Indicates the ticker delay value is in seconds.
     * @return <code>true</code> if the ticker delay value is in seconds.
     */
    public boolean useSeconds()
    {
        return _useSeconds ;
    }

    private int        _currentLoop = 0 ;
    private int        _delay = 100 ;
    private int        _numLoop = 0 ;
    private TickerNext _next ;
    private Timer      _timer ;
    protected boolean  _stopped = false ;
    private boolean    _useSeconds = false ;

    class TickerNext extends TimerTask
    {
        TickerNext( Ticker ticker , Timer timer )
        {
            this.ticker = ticker ;
            this.timer  = timer ;
        }

        private Progress progress = new Progress();
        private Ticker ticker ;
        private Timer timer ;

        public void run()
        {
            if ( !ticker.running() )
            {
                cancel();
                return ;
            }

            ticker.notifyProgress(progress) ;

            if ( ticker.looping )
            {
                if ( ticker._numLoop == 0 )
                {
                    _currentLoop = 0  ;
                    ticker.notifyLooped() ;
                    return ;
                }
                else if ( ++ticker._currentLoop < ticker._numLoop )
                {
                    ticker.notifyLooped() ;
                    return ;
                }
            }

            _currentLoop = 0  ;

            _next.cancel();

            ticker._timer.purge() ;

            ticker._stopped = false ;

            ticker.notifyFinished() ;
        }
    }
}



