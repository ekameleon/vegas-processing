/*
  Version: MPL 2.0/GPL 2.0/LGPL 2.1

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/

  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.

  The Original Code is [VEGAS framework].

  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com> and Zwetan Kjukov <zwetan@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2003-2018
  the Initial Developers. All Rights Reserved.

  Contributor(s):

  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package system.process;

/**
 * The ActionEntry objects contains all informations about an {@link system.process.Action} in a {@link TaskGroup} object.
 */
public class ActionEntry implements Comparable<ActionEntry>
{
    /**
     * The Action reference.
     */
    protected Action _action ;

    /**
     * The auto disconnect flag value.
     */
    protected boolean _auto ;

    /**
     * The priority value.
     */
    protected int _priority = 0 ;

    /**
     * Creates a new ActionEntry instance.
     * @param action The Action reference.
     * @param priority The priority of the entry.
     * @param auto The auto mode.
     */
    public ActionEntry( Action action , int priority , boolean auto )
    {
        this._action   = action ;
        this._auto     = auto ;
        this._priority = priority ;
    }

    /**
     * Returns the Action reference.
     * @return the Action reference.
     */
    public Action getAction()
    {
        return _action ;
    }

    /**
     * Returns the auto boolean value.
     * @return The auto disconnect boolean flag.
     */
    public boolean getAuto()
    {
        return _auto ;
    }

    /**
     * Returns the priority value.
     * @return the priority value.
     */
    public int getPriority()
    {
        return _priority ;
    }

    /**
     * Compares the object with the other object.
     * @param entry The object to compare.
     * @return -1, 0 or 1.
     */
    public int compareTo( ActionEntry entry )
    {
        if ( _priority > entry._priority )
        {
            return -1;
        }
        else if ( _priority < entry._priority )
        {
            return 1;
        }
        else
        {
            return 0 ;
        }
    }

}
