/*
  Version: MPL 2.0/GPL 2.0/LGPL 2.1

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/

  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.

  The Original Code is [VEGAS framework].

  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com> and Zwetan Kjukov <zwetan@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2003-2018
  the Initial Developers. All Rights Reserved.

  Contributor(s):

  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package system.process;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

/**
 * The abstract class to creates collections who group some Action objects in one.
 */
public abstract class TaskGroup extends AbstractProcess
{
    public TaskGroup() {}
    public TaskGroup(Object id) { super(id) ; }

    /// -------- Properties

    protected ArrayList<ActionEntry> _actions = new ArrayList<ActionEntry>() ;

    protected TaskGroupMode _mode = TaskGroupMode.NORMAL ;

    protected boolean _stopped = false ;

    /// -------- Getters/Setters

    /**
     * Returns the current mode of the group.
     * @return The current mode of the group.
     */
    public TaskGroupMode getMode()
    {
        return _mode ;
    }

    /**
     * Defines the current mode of the group.
     * @param mode The mode setting of the group.
     */
    public void setMode( TaskGroupMode mode )
    {
        _mode = mode ;
    }


    /// -------- Methods

    /**
     * Adds a new Action in the collection.
     * @param action The Action reference to register.
     */
    public void add( Action action )
    {
        add( action , 0 , false ) ;
    }

    /**
     * Adds a new Action in the collection.
     * @param action The Action reference to register.
     * @param priority The priority flag to sort the listener.
     */
    public void add( Action action , int priority )
    {
        add( action , priority , false ) ;
    }

    /**
     * Adds a new Action in the collection.
     * @param action The Action reference to register.
     * @param priority The priority flag to sort the listener.
     * @param autoRemove Indicates if the action must be unregister after the first invocation.
     */
    public void add( Action action , int priority , boolean autoRemove )
    {
        if ( this._running )
        {
            throw new RuntimeException( this + " add failed, the Action is in progress." ) ;
        }

        priority = ( priority > 0 ) ? priority : 0 ;

        action.finishIt().disconnect() ;

        action.finishIt().connect( this , "next" ) ;

        _actions.add( new ActionEntry( action , priority , autoRemove ) ) ;

        Collections.sort( _actions ) ;
    }

    /**
     * Returns <code>true</code> if the specified Action is registered in the group.
     * @param action Test if the passed-in Action is registered in the group.
     * @return <code>true</code> if the specified Action is registered in the group.
     */
    public boolean contains( Action action )
    {
        if( _actions.size() > 0 )
        {
            Iterator<ActionEntry> it = _actions.iterator() ;
            while( it.hasNext() )
            {
                ActionEntry entry = it.next() ;
                if( entry.getAction() == action ) return true ;
            }
        }
        return false ;
    }

    /**
     * Disposes the group and disconnect all actions but don't remove them.
     */
    public void dispose()
    {
        if( _actions.size() > 0 )
        {
            Iterator<ActionEntry> it = _actions.iterator() ;
            while( it.hasNext() )
            {
                ActionEntry entry = it.next() ;
                entry.getAction().finishIt().disconnect() ;
            }
        }
    }

    /**
     * Returns the <code>Action</code> register in the collection at the specified index value or <code>null</code>.
     * @param index The index of the Action to find.
     * @return the <code>Action</code> register in the collection at the specified index value or <code>null</code>.
     */
    public Action get( int index )
    {
        ActionEntry entry = _actions.get( index ) ;
        return entry.getAction() ;
    }

    /**
     * Tests if this list has no elements.
     * @return <code>true</code> if this list has no elements; <code>false</code> otherwise.
     */
    public boolean isEmpty()
    {
        return _actions.isEmpty() ;
    }

    /**
     * Indicates if the Action is stopped.
     * @return <code>true</code> if the Action is stopped.
     */
    public boolean stopped()
    {
        return _stopped ;
    }

    /**
     * Invoked when the current task is finished.
     * @param object The object emitted by the current Action when the task is finished.
     */
    public abstract void next( Object object );

    /**
     * Removes all the Action objects in the group.
     * @return <code>true</code> if the remove method succeed.
     */
    public boolean remove()
    {
        if ( this._running )
        {
            throw new RuntimeException( this + " remove failed, the Action is in progress." ) ;
        }
        if( _actions.size() > 0 )
        {
            Iterator<ActionEntry> it = _actions.iterator() ;
            while( it.hasNext() )
            {
                ActionEntry entry = it.next() ;
                entry.getAction().finishIt().disconnect() ;
                it.remove() ;
            }
        }
        return false ;
    }

    /**
     * Removes all the Action objects in the group.
     * @param action The Action reference to unregister.
     * @return <code>true</code> if the remove method succeed.
     */
    public boolean remove( Action action )
    {
        if ( this._running )
        {
            throw new RuntimeException( this + " remove failed, the Action is in progress." ) ;
        }
        if( _actions.size() > 0 )
        {
            Iterator<ActionEntry> it = _actions.iterator() ;
            while( it.hasNext() )
            {
                ActionEntry entry = it.next() ;
                Action current = entry.getAction() ;
                if( current == action )
                {
                    current.finishIt().disconnect() ;
                    it.remove() ;
                    return true ;
                }
            }
        }
        return false ;
    }

    /**
     * The numbers of Action registered in the batch.
     * @return the numbers of Action registered in the batch.
     */
    public int size()
    {
        return _actions.size() ;
    }
}
