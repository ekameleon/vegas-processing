/*
  Version: MPL 2.0/GPL 2.0/LGPL 2.1

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/

  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.

  The Original Code is [VEGAS framework].

  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com> and Zwetan Kjukov <zwetan@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2003-2018
  the Initial Developers. All Rights Reserved.

  Contributor(s):

  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package system.process;

import system.data.ValueObject ;

/**
 * Defines all information during a progress process.
 */
public class Progress extends ValueObject
{
    /**
     * Creates a new Progress instance.
     */
    public Progress() {}

    /**
     * Creates a new Progress instance.
     * @param loaded The number of items or bytes loaded when the listener processes the event.
     * @param total The total number of items or bytes that will be loaded if the loading process succeeds.
     */
    public Progress( float loaded , float total )
    {
        this.loaded = loaded ;
        this.total  = total ;
    }

    /**
     * Creates a new Progress instance.
     * @param loaded The number of items or bytes loaded when the listener processes the event.
     * @param total The total number of items or bytes that will be loaded if the loading process succeeds.
     * @param message The optional string expression to display during the loading process.
     */
    public Progress( float loaded , float total , String message )
    {
        this.loaded  = loaded ;
        this.total   = total ;
        this.message = message ;
    }

    /**
     * The optional current target reference.
     */
    public Object currentTarget ;

    /**
     * The number of items or bytes loaded.
     */
    public float loaded = 0 ;

    /**
     * A string expression to display during the loading progress.
     */
    public String message = "" ;

    /**
     * The optional target reference.
     */
    public Object target ;

    /**
     * The total number of items or bytes that will be loaded if the loading process succeeds.
     */
    public float total = 0 ;

    /**
     * Returns a shallow copy of the object.
     * @return a shallow copy of the object.
     */
    @Override
    public Progress clone()
    {
        Progress entry = new Progress( loaded , total , message ) ;
        entry.target = target ;
        entry.setID( _id ) ;
        return entry ;
    }

    /**
     * Returns the string representation of the object.
     * @return the string representation of the object.
     */
    public String toString()
    {
        return formatToString( null
                , "loaded:" + loaded , "total:" + total
        ) ;
    }
}
