/*
  Version: MPL 2.0/GPL 2.0/LGPL 2.1

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/

  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.

  The Original Code is [VEGAS framework].

  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com> and Zwetan Kjukov <zwetan@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2003-2018
  the Initial Developers. All Rights Reserved.

  Contributor(s):

  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package system.process;

/**
 * A chain is a sequence with a finite or infinite number of actions.
 * All actions registered in the chain can be executed one by one with different strategies (loop, auto remove, etc).
 * <br>
 * <b>Example:</b><br>
 * <pre>
 * import system.process.Chain ;
 * import system.process.TaskGroupMode ;
 *
 * void setup()
 * {
 *     size(800, 500);
 *     background(100);
 *
 *     Console finish   = new Console("finish") ;
 *     Console progress = new Console("progress") ;
 *     Console start    = new Console("start") ;
 *
 *     Do command1 = new Do( "#1" ) ;
 *     Do command2 = new Do( "#2" ) ;
 *     Do command3 = new Do( "#3" ) ;
 *
 *     Chain chain = new Chain() ;
 *
 *     chain.finishIt().connect( finish , "receive" ) ;
 *     chain.progressIt().connect( progress , "receive" ) ;
 *     chain.startIt().connect( start , "receive" ) ;
 *
 *     chain.add( command1 ) ;
 *     chain.add( command2 , 1000 ) ;
 *     chain.add( command3 , 0 , true  ) ;
 *
 *     chain.run() ;
 *
 *     println( "-----" ) ;
 *
 *     chain.run() ;
 * }
 * </pre>
 * The "Do" class extends the Task class to create a concrete Action representation.<br>
 * <pre>
 * import system.process.Task ;
 *
 * public class Do extends Task
 * {
 *     public String name ;
 *
 *     public Do( String name )
 *     {
 *         this.name = name ;
 *     }
 *
 *     public Do clone()
 *     {
 *         return new Do( name ) ;
 *     }
 *
 *     public void run( Object... args )
 *     {
 *         notifyStarted() ;
 *         println( this + " run" ) ;
 *         notifyFinished() ;
 *     }
 *
 *     public String toString()
 *     {
 *         return "[Do " + name + "]" ;
 *     }
 * }
 * </pre>
 * The "Console" class is a slot who receive message from signals.<br>
 * <pre>
 * import system.process.Chain ;
 * import system.process.Progress ;
 *
 * public class Console
 * {
 *     public String name ;
 *
 *     public Console( String name )
 *     {
 *         this.name = name ;
 *     }
 *
 *     public void receive( Object object )
 *     {
 *         Chain task = (Chain) object ;
 *         println( this + " task:" + task + " phase:" + task.phase() ) ;
 *     }
 *
 *     public void receive( Progress progress )
 *     {
 *         Chain task = (Chain) progress.target ;
 *         println( this + " task:" + task + " phase:" + task.phase() + " num:" + task.size() + " current:" + progress.currentTarget ) ;
 *     }
 *
 *     public String toString()
 *     {
 *         return "[Console " + name + "]" ;
 *     }
 * }
 * </pre>
 */
public class Chain extends TaskGroup
{
    /**
     * Creates a new Chain instance.
     */
    public Chain() {}

    /**
     * Creates a new Chain instance.
     * @param id The unique identifier of the object.
     */
    public Chain( Object id )
    {
        super(id) ;
    }

    /**
     * Specifies the number of the times the chain should loop during playback.
     */
    public int numLoop = 0 ;

    private ActionEntry _current = null ;

    private int _currentLoop = 0 ;

    protected int _position = 0 ;

    /**
     * Returns a shallow copy of the object.
     * @return a shallow copy of the object.
     */
    @Override
    public Chain clone()
    {
        Chain copy = new Chain( _id );
        copy.setMode( getMode() ) ;
        copy.numLoop = numLoop ;
        return copy ;
    }

    /**
     * Returns the current Action reference.
     * @return the current Action reference.
     */
    public Action current()
    {
        return _current != null ? _current.getAction() : null;
    }

    /**
     * Indicates the current countdown loop value.
     * @return the current countdown loop value.
     */
    public int currentLoop()
    {
        return _currentLoop ;
    }

    /**
     * Retrieves the next action reference in the chain with the current position.
     * @return the next action reference in the chain with the current position.
     */
    public Action element()
    {
        return hasNext() ? _actions.get(_position).getAction() : null ;
    }

    /**
     * Indicates if the chain contains a next action (based with the current position value).
     * @return <code>true</code> if the chain contains a next action.
     */
    public boolean hasNext()
    {
        return _position < _actions.size() ;
    }

    @Override
    public void next( Object object )
    {
        if ( _current != null )
        {
            if ( _mode != TaskGroupMode.EVERLASTING )
            {
                if ( _mode == TaskGroupMode.TRANSIENT || (_current.getAuto() && _mode == TaskGroupMode.NORMAL) )
                {
                    Action action = _current.getAction() ;
                    action.finishIt().disconnect( this , "next" ) ;
                    _position-- ;
                    _actions.remove( _current ) ;
                }
            }
            notifyChanged() ;
            _current = null ;
        }

        if ( _actions.size() > 0 )
        {
            if ( hasNext() )
            {
                _current = _actions.get( _position ) ;

                _position++ ;

                Progress info = new Progress() ;

                info.currentTarget = _current.getAction() ;

                notifyProgress( info ) ;

                _current.getAction().run() ;
            }
            else if ( looping )
            {
                _position = 0 ;
                if( numLoop == 0 )
                {
                    notifyLooped() ;
                    _currentLoop = 0  ;
                    next(null ) ;
                }
                else if ( _currentLoop < numLoop )
                {
                    _currentLoop ++ ;
                    notifyLooped() ;
                    next(null ) ;
                }
                else
                {
                    _currentLoop = 0 ;
                    notifyFinished() ;
                }
            }
            else
            {
                _currentLoop = 0 ;
                _position    = 0 ;
                notifyFinished() ;
            }
        }
        else
        {
            notifyFinished() ;
        }
    }

    /**
     * Indicates the current numeric position of the chain.
     * @return the current numeric position of the chain.
     */
    public int position()
    {
        return _position ;
    }

    @Override
    public void reset()
    {
        if( _running )
        {
            stop() ;
        }
        if( _actions.size() > 0 )
        {
            remove();
        }
    }

    @Override
    public void resume()
    {
        if ( _stopped )
        {
            _running = true ;
            _stopped = false ;
            notifyResumed() ;
            if ( _current != null )
            {
                Action action = _current.getAction() ;
                if( action instanceof Resumable )
                {
                    Resumable object = (Resumable) action ;
                    object.resume() ;
                }
            }
            else
            {
                next(null ) ;
            }
        }
        else
        {
            run() ;
        }
    }

    @Override
    public void run( Object... args )
    {
        if ( !_running )
        {
            notifyStarted() ;
            _current     = null  ;
            _stopped     = false ;
            _position    = 0 ;
            _currentLoop = 0 ;

            if( _actions.size() > 0 )
            {
                next(null ) ;
            }
            else
            {
                notifyFinished();
            }
        }
    }

    @Override
    public void start()
    {
        run() ;
    }

    @Override
    public void stop()
    {
        if ( _running )
        {
            if ( _current != null )
            {
                if ( _current.getAction() instanceof Stoppable )
                {
                    Stoppable object = (Stoppable) _current.getAction() ;
                    object.stop() ;
                }
            }
            _running = false ;
            _stopped = true ;
            notifyStopped() ;
        }
    }
}
