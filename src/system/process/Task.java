/*
  Version: MPL 2.0/GPL 2.0/LGPL 2.1

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/

  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.

  The Original Code is [VEGAS framework].

  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com> and Zwetan Kjukov <zwetan@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2003-2018
  the Initial Developers. All Rights Reserved.

  Contributor(s):

  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package system.process;

import system.data.ValueObject ;
import system.signals.Signal;

/**
 * An abstract Action object to create a set of complex commands or actions.
 */
public abstract class Task extends ValueObject implements Action
{
    private Signal _finishIt = new Signal( Object.class ) ;
    private Signal  _startIt = new Signal( Object.class ) ;

    protected boolean __lock__ = false ;
    protected TaskPhase _phase = TaskPhase.INACTIVE ;
    protected boolean _running = false ;

    public Task()
    {
        super() ;
    }

    public Task(Object id)
    {
        super(id) ;
    }

    /**
     * Returns the signal who emit when the action is finished.
     * @return the signal who emit when the action is finished.
     */
    public Signal finishIt()
    {
        return _finishIt ;
    }

    /**
     * Indicates if the object is locked.
     * @return <code>true</code> if the object is locked.
     */
    public boolean isLocked()
    {
        return __lock__ ;
    }

    /**
     * Locks the object.
     */
    public void lock()
    {
        __lock__ = true ;
    }

    /**
     * Invoked when the process is finished.
     */
    public void notifyFinished()
    {
        this._running = false ;
        this._phase = TaskPhase.FINISHED ;
        this._finishIt.emit( this ) ;
        this._phase = TaskPhase.INACTIVE ;
    }

    /**
     * Invoked when the process is started.
     */
    public void notifyStarted()
    {
        this._running = true ;
        this._phase  = TaskPhase.STARTED ;
        this._startIt.emit( this ) ;
        this._phase = TaskPhase.RUNNING ;
    }

    /**
     * Indicates the current phase of the task.
     * @return The current phase of the task.
     */
    public TaskPhase phase()
    {
        return _phase ;
    }

    /**
     * Execute the command.
     * @param args A serie of optional arguments.
     */
    public abstract void run( Object... args ) ;

    /**
     * Indicates if the action is running.
     * @return <code>true</code> if the process is running.
     */
    public boolean running()
    {
        return _running ;
    }

    /**
     * Returns the signal who emit when the action is started.
     * @return the signal who emit when the action is started.
     */
    public Signal startIt()
    {
        return _startIt ;
    }

    /**
     * Unlocks the object.
     */
    public void unlock()
    {
        __lock__ = false ;
    }
}