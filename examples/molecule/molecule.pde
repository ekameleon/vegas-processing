import core.easings.*;

import graphics.Align ;
import graphics.geom.Rectangle ;

import molecule.Molecule ;
import molecule.display.Quad ;
import molecule.display.Shape ;
import molecule.display.Sprite ;
import molecule.display.Stage ;
import molecule.events.ResizeEvent ;

import system.events.Event ;
import system.events.EventListener ;
import system.transitions.*;
import system.process.Chain ;

// ----------------

Molecule molecule;
Sprite   root ;
Stage    stage ;

// ----------------

Quad quad1 ;
Quad quad2 ;

Shape shape1 ;
Shape shape2 ;

// ----------------

Tween tween1 ;
Tween tween2 ;
Tween tween3 ;
Tween tween4 ;

// ----------------


void setup()
{
    size(860,500);
    
    molecule = new Molecule( this ) ;
    
    root  = (Sprite) molecule.root() ;
    stage = (Stage) molecule.stage() ;
    
    stage.addEventListener( ResizeEvent.RESIZE , new ResizeListener() ) ;
    
    // ----------
    
    quad1 = new Quad( 30 , 30 , #FF0000 ) ;
    
    quad1.setTo( 25 , 25 ) ;
    
    // quad1.setScale(1.5,1.5) ;
    //quad1.setPivotX(15)
    //     .setPivotY(15)
    //     .setScaleX(2)
    //     .setScaleY(2) ;
    
    //quad1.alignPivot(Align.CENTER,Align.CENTER);
    
    // ----------
    
    quad2 = new Quad( 30 , 30 , #FFC300 ) ;
    quad2.setTo( 65 , 25 ) ;
    
    // ----------
    
    shape1 = new Shape("svg/aries.svg");
    shape1.setColor( #FF0000 ) ;
    shape1.setTo( 120 , 25 ) ;
   
    // ----------
    
    shape2 = new Shape("svg/aries.svg");
    shape2.setColor( #FFFFFF ) ;
    shape2.setTo( 160 , 25 ) ;
    //shape2.setRotation( 45 ) ;
    
    // ----------
    
    root.addChild( quad1 ) ;
    root.addChild( quad2 ) ;
    root.addChild( shape1 ) ;
    root.addChild( shape2 ) ;
    
    // ----------
    
    tween1 = new Tween( quad1 , Linear.ease, 60 ) ;
    tween1.from( "x" , quad1.getX() ).to( "x" , 400 ).easing( "x" , Sine.easeOut )
          .from( "y" , quad1.getY() ).to( "y" , 430 ).easing( "y" , Bounce.easeOut );
          //.from( "rotation" , 0 ).to( "rotation" , 360 ).easing( "rotation" , Linear.ease );
         
    tween2 = new Tween( quad2 , Elastic.easeOut, 60 ) ;
    tween2.from( "x" , quad2.getX() ).to( "x" , 460 ).easing( "x" , Sine.easeOut )
          .from( "y" , quad2.getY() ).to( "y" , 430 ).easing( "y" , Back.easeOut ) ;
       
    tween3 = new Tween( shape1 , Elastic.easeOut, 60 ) ;
    tween3.from( "x" , shape1.getX() ).to( "x" , 550 ).easing( "x" , Sine.easeOut )
          .from( "y" , shape1.getY() ).to( "y" , 430 ).easing( "y" , Elastic.easeOut ) ;
          
    tween4 = new Tween( shape2 , Elastic.easeIn, 60 ) ;
    tween4.from( "rotation" , 0 ).to( "rotation" , 360 ).easing( "rotation" , Linear.ease )
          .from( "x" , shape2.getX() ).to( "x" , 600 ).easing( "x" , Sine.easeOut )
          .from( "y" , shape2.getY() ).to( "y" , 430 ).easing( "y" , Back.easeOut ) ;
       
    // ----------
    
    Chain chain = new Chain() ;
    
    chain.add( tween1 );
    chain.add( tween2 );
    chain.add( tween3 );
    chain.add( tween4 );
    
    chain.run() ;
}

void draw()
{
   molecule.render() ;
}


class ResizeListener implements EventListener
{
    public ResizeListener(){}
    
    public void handleEvent( Event event )
    {
        println( (ResizeEvent) event ) ;
    }
    
    public String toString()
    {
        return "[ResizeListener]" ;
    }
}
