import system.logging.targets.PrintTarget ;
import system.logging.Log ;
import system.logging.Logger ;
import system.logging.LoggerLevel ;

PrintTarget target = new PrintTarget() ;
Logger logger = Log.getLogger("system.logging.logger") ;


void setup()
{
    size(800, 500);
    background( #294B7B );
    
    target.removeFilter( "*" ) ; // remove the * wildcard
    target.addFilter( "system.logging.*" ) ;
    
    target.setLevel( LoggerLevel.ALL ) ;
     
    target.includeDate    = false ;
    target.includeTime    = false ;
    target.includeMilliseconds = false ;
    target.includeLevel   = true ;
    target.includeChannel = false ;
    target.includeLines   = true ;
        
    logger.debug    ( "hello {0} {1}" , "debug"    , "!" ) ;
    logger.info     ( "hello {0} {1}" , "info"     , "!" ) ;
    logger.warning  ( "hello {0} {1}" , "warning"  , "!" ) ;
    logger.error    ( "hello {0} {1}" , "error"    , "!" ) ;
    logger.critical ( "hello {0} {1}" , "critical" , "!" ) ;
    logger.wtf      ( "hello {0} {1}" , "wtf"      , "!" ) ;
    
    logger.debug("hello world") ;
    
    noLoop();
}

void draw()
{
   logger.debug( "draw" ) ;
}