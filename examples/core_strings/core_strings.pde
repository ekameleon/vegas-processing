import core.strings ;

void setup()
{
    size(800, 500);
    background(100);
  
    // ------- indexOfAny
    
    println( strings.fastformat( "hello {0}", "world" ) ); // "hello world"
    
    // ------- indexOfAny
    
    println(strings.indexOfAny("zzabyycdxx", "za")) ; // 0
    println(strings.indexOfAny("zzabyycdxx", "by")) ; // 3
    println(strings.indexOfAny("aba","z")) ; // -1

    println(strings.indexOfAny( "zzabyycdxx", ("za").toCharArray() )) ; // 0
    println(strings.indexOfAny( "zzabyycdxx", ("by").toCharArray() )) ; // 3
    println(strings.indexOfAny( "aba"       ,  ("z").toCharArray() )) ; // -1
    
}