import core.easings.Linear;
/** 
<p><strong>Easing equations demo based on the Robert Penner's easing equations implementation.</strong></p>
<p>all rects take the same time to travel from left to right, but each color uses a different equation.</p>
<p>click on the applet to reset movement</p>
*/
import core.easings.*;

int   time      = 0 ;
float beginning = 0 ;
float change    = 470 ;
float duration  = 200 ;

void setup() 
{
    size(480,450);
    frameRate(60);
    background(0);
}

void draw() 
{    
    background(25);
 
    fill(140,255,100);
    rect( Linear.ease.calculate(time, beginning, change, duration),10,10,10);

    fill(100,0,100);
    rect(Quadratic.easeIn.calculate(time, beginning, change, duration),50,10,10);
    rect(Quadratic.easeOut.calculate(time, beginning, change, duration),60,10,10);
    rect(Quadratic.easeInOut.calculate(time, beginning, change, duration),70,10,10);
    
    fill(100,100,50);
    rect(Cubic.easeIn.calculate(time, beginning, change, duration),90,10,10);
    rect(Cubic.easeOut.calculate(time, beginning, change, duration),100,10,10);
    rect(Cubic.easeInOut.calculate(time, beginning, change, duration),110,10,10);
    
    fill(125,0,255);
    rect(Quartic.easeIn.calculate(time, beginning, change, duration),130,10,10);
    rect(Quartic.easeOut.calculate(time, beginning, change, duration),140,10,10);
    rect(Quartic.easeInOut.calculate(time, beginning, change, duration),150,10,10);
    
    fill(0,125,255);
    rect(Quintic.easeIn.calculate(time, beginning, change, duration),170,10,10);
    rect(Quintic.easeOut.calculate(time, beginning, change, duration),180,10,10);
    rect(Quintic.easeInOut.calculate(time, beginning, change, duration),190,10,10);
    
    fill(255,0,255);
    rect(Sine.easeIn.calculate(time, beginning, change, duration),210,10,10);
    rect(Sine.easeOut.calculate(time, beginning, change, duration),220,10,10);
    rect(Sine.easeInOut.calculate(time, beginning, change, duration),230,10,10);
    
    fill(255,0,0);
    rect(Circular.easeIn.calculate(time, beginning, change, duration),250,10,10);
    rect(Circular.easeOut.calculate(time, beginning, change, duration),260,10,10);
    rect(Circular.easeInOut.calculate(time, beginning, change, duration),270,10,10);
    
    fill(255,100,0);
    rect(Expo.easeIn.calculate(time, beginning, change, duration),290,10,10);
    rect(Expo.easeOut.calculate(time, beginning, change, duration),300,10,10);
    rect(Expo.easeInOut.calculate(time, beginning, change, duration),310,10,10);
    
    fill(100,255,0);
    rect(Back.easeIn.calculate(time, beginning, change, duration),330,10,10);
    rect(Back.easeOut.calculate(time, beginning, change, duration),340,10,10);
    rect(Back.easeInOut.calculate(time, beginning, change, duration),350,10,10);
    
    fill(180,255,255);
    rect(Bounce.easeIn.calculate(time, beginning, change, duration),370,10,10);
    rect(Bounce.easeOut.calculate(time, beginning, change, duration),380,10,10);
    rect(Bounce.easeInOut.calculate(time, beginning, change, duration),390,10,10);
    
    fill(0,255,255);
    rect(Elastic.easeIn.calculate(time, beginning, change, duration),410,10,10);
    rect(Elastic.easeOut.calculate(time, beginning, change, duration),420,10,10);
    rect(Elastic.easeInOut.calculate(time, beginning, change, duration),430,10,10);

    if (time < duration) 
    {
        time++;
    }
    
}
void mousePressed() 
{
    time = 0;    
}