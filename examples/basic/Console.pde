public class Console
{
    public String name ;

    /**
     * Creates a new Console instance.
     */
    public Console( String name )
    {
        this.name = name ;
    }

    /**
     * This method is called when the receiver is connected with a Signal object.
     * @param args the argument list to dispatch.
     */
    public void receive( String message )
    {
        println( this.name + " message: " + message ) ;
    }

    /**
     * Returns the String representation of the object.
     * @return The String representation of the object.
     */
    public String toString()
    {
        return "[Console " + name + "]" ;
    }
}