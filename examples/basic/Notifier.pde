import system.signals.Receiver ;

public class Notifier implements Receiver
{
    public String name ;

    /**
     * Creates a new Console instance.
     */
    public Notifier( String name )
    {
        this.name = name ;
    }

    /**
     * This method is called when the receiver is connected with a Signal object.
     * @param args the argument list to dispatch.
     */
    public void receive( Object object )
    {
        println( this.name + " receive: " + object ) ;
    }

    /**
     * Returns the String representation of the object.
     * @return The String representation of the object.
     */
    public String toString()
    {
        return "[Notifier " + name + "]" ;
    }
}