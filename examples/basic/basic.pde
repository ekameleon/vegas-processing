import system.events.Event ;

import system.signals.Signal ;

void setup()
{
    size(800, 500);
    background(100);

    Console console1 = new Console( "console1" ) ;
    Console console2 = new Console( "console2" ) ;
    Console console3 = new Console( "console3" ) ;

    Signal signal = new Signal( String.class ) ;

    signal.connect( console1 , "receive" ) ;
    signal.connect( console2 , "receive" , 1000 , true ) ;
    signal.connect( console3 , "receive" ) ;

    println("-----") ;

    println( "connected: " + signal.connected() ) ;
    println( "size: " + signal.size() ) ;

    println("-----") ;

    signal.emit( "hello" ) ;

    println("-----") ;

    signal.emit( "world" ) ;

    signal.disconnect( console3 , "receive" ) ;

    println("-----") ;

    println( "contains console1.receive: " + signal.contains( console1 , "receive" ) ) ;
    println( "contains console2.receive: " + signal.contains( console2 , "receive" ) ) ;
    println( "contains console3.receive: " + signal.contains( console3 , "receive" ) ) ;

    println("-----") ;

    signal.emit( "!" ) ;

    signal.disconnect() ; // disconnect all

    println("-----") ;

    println( "connected: " + signal.connected() ) ;
    println( "size: " + signal.size() ) ;
    
    println("-----") ;
    
    Signal signal2 = new Signal( Object.class ) ;
    
    Notifier toast = new Notifier("toast") ;
    
    signal2.connect( toast , 0 , true ) ;
    
    signal2.emit( new Event("click") );
}