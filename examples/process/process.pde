import system.process.Chain ;

void setup()
{
    size(800, 500);
    background(100);

    Console finish   = new Console("finish") ;
    Console progress = new Console("progress") ;
    Console start    = new Console("start") ;
    
    Do command1 = new Do("#1" , 2000 ) ;
    Do command2 = new Do("#2" , 5000 ) ; 
    Do command3 = new Do("#3" , 3000 ) ; 
    
    Chain chain = new Chain() ;
    
    chain.finishIt().connect( finish , "receive" ) ;
    chain.progressIt().connect( progress , "receive" ) ;
    chain.startIt().connect( start , "receive" ) ;
    
    chain.add( command1 ) ;
    chain.add( command2 , 1000 ) ;
    chain.add( command3 , 0 , true  ) ;
    
    chain.run() ;
}