import system.process.Chain ;
import system.process.Progress ;

public class Console
{
    public String name ;
    
    public Console( String name )
    {
        this.name = name ;
    }
  
    public void receive( Object object )
    {
        Chain task = (Chain) object ;
        println( this + " task:" + task + " phase:" + task.phase() ) ;
    }
    
    public void receive( Progress progress )
    {
        Do current = (Do) progress.currentTarget ;
        Chain task = (Chain) progress.target ;
        println( this + " batch:" + task + " phase:" + task.phase() + " num:" + task.size() + " current:" + current ) ;
    }
    
    public String toString()
    {
       return "[Console " + name + "]" ;
    }
}