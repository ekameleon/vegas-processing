import system.process.Task ;

public class Do extends Task
{
    public int delay ;
    public String name ;
  
    public Do( String name )
    {
        this( name , 1000 ) ;
    }
  
    public Do( String name , int delay )
    {
        this.name = name ;
        this.delay = delay ;
    }

    public Do clone()
    {
        return new Do( name , delay ) ;
    }
  
    public void run( Object... args )
    {
        notifyStarted() ;
        println( this + " run" ) ;
        try
        {
            Thread.sleep(delay);
        }
        catch( InterruptedException ex )
        {
            ex.printStackTrace();
        }
        notifyFinished() ;
    }
  
    public String toString()
    {
        return "[Do name" + name + " delay:" + delay + "]" ;    
    }
} 