
import system.events.EventListener ;

public class Console implements EventListener
{
    public String name ;

    public Console( String name )
    {
        this.name = name ;
    }

    public void handleEvent( Event event )
    {
        println( this.name + " : " + event.getType() ) ;
    }

    /**
     * Returns the String representation of the object.
     * @return The String representation of the object.
     */
    public String toString()
    {
        return "[Console " + name + "]" ;
    }
}