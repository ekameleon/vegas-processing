import system.events.Event;
import system.events.EventDispatcher;

void setup()
{
    size(800, 500);
    background(100);

    EventDispatcher dispatcher = new EventDispatcher() ;

    Console console1 = new Console( "console1" ) ;
    Console console2 = new Console( "console2" ) ;
    Console console3 = new Console( "console3" ) ;
    
    dispatcher.addEventListener( "click" , console1 ) ;
    dispatcher.addEventListener( "click" , console2 , false , 1000 ) ;
    dispatcher.addEventListener( "click" , console3 ) ;

    dispatcher.dispatchEvent( new Event( "click" ) ) ;
    
    separator() ;
    
    dispatcher.removeEventListener( "click" , console2 ) ;

    dispatcher.dispatchEvent( new Event( "click" ) ) ;
}

void draw()
{

}

void separator()
{
    println("-----") ;
}