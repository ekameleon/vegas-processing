import core.easings.Linear;
import core.easings.*;
import system.transitions.*;

Console finish = new Console("finish" ) ;
Console change = new Console("change" ) ;
Console start  = new Console("start"  ) ;

Rectangle rect ;
Tween tween ;

void setup() 
{
    size(480,450);
    frameRate(60);
    background(0);
    
    rect = new Rectangle( 10 , 10 , 20 , 20 ) ;
    
    tween = new Tween( rect , Elastic.easeOut, 60 ) ;
    
    tween.setFPS( 60 )
         .setDuration( 60 ) ;
         
    //tween.looping = true ;
    //tween.setUseSeconds( true ).setDuration( 2 );
    
    tween.finishIt().connect( finish ,"receive" ) ;
    //tween.changeIt().connect( change ,"receive" ) ;
    tween.startIt().connect( start  ,"receive" ) ;  

    tween.from( "x" , 10 ).to( "x" , 460 ).easing( "x" , Sine.easeOut )
         .from( "y" , 10 ).to( "y" , 430 ).easing( "y" , Bounce.easeOut ) ;
    tween.run() ;
}

void draw() 
{    
    background(25);
    rect.render() ;
    
}
void mousePressed() 
{
    if( tween.running() )
    {
       tween.stop() ; 
    }
    
    tween.from( "alpha" , 0 ).to( "alpha" , 0.6 ).easing( "x" , Linear.ease )
         .from( "x" , rect.x ).to( "x" , mouseX - rect.width * .5 ).easing( "x" , Back.easeOut )
         .from( "y" , rect.y ).to( "y" , mouseY - rect.height * .5 ).easing( "y" , Expo.easeOut ) ;
    tween.run() ;
}

public class Rectangle
{
    public Rectangle( float x , float y , float width , float height )
    {
        this.x = x ;
        this.y = y ;
        this.width = width ;
        this.height = height ;
    }

    private float _alpha = 1 ;
    
    public float x = 0 ;
    public float y = 0 ;

    public float width = 0 ;
    public float height = 0 ;
    
    public Float getAlpha()
    {
         return _alpha ;
    }
    
    public Rectangle setAlpha( Float value )
    {
         _alpha = Math.max( Math.min( value, 1 ), 0 ) ;
         return this ;
    }
    
    
    public void render()
    {
        fill(140,255,100,_alpha*100);
        rect( x, y, width, height);
    }    
     
    public String toString()
    {
        return "[Rectangle x:" + x + " y:" + y + " width:" + width + " height:" + height + "]" ;
    }
}

public class Console
{
    String name ;

    public Console( String name )
    {
        this.name = name ;
    }

    public void receive( Object object )
    {
        Tween tween = (Tween) object ;
        println( this + " rect:" + rect + " tween:" + tween + " time:" + tween.time() + " duration" + tween.duration() ); ;
    }

    public String toString()
    {
        return "[Console " + name + "]" ;
    }
}