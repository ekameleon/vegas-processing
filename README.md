# VEGAS Processing #

A Processing library to create Rich Internet Applications or Games with Processing (JAVA).

### Example

```processing
import system.signals.Signal ;

void setup()
{
    size(800, 500);
    background(100);

    Console console1 = new Console( "console1" ) ;
    Console console2 = new Console( "console2" ) ;
    Console console3 = new Console( "console3" ) ;

    Signal signal = new Signal( String.class ) ;

    signal.connect( console1 , "receive" ) ;
    signal.connect( console2 , "receive" , 1000 , true ) ;
    signal.connect( console3 , "receive" ) ;

    println("-----") ;

    println( "connected: " + signal.connected() ) ;
    println( "size: " + signal.size() ) ;

    println("-----") ;

    signal.emit( "hello" ) ;

    println("-----") ;

    signal.emit( "world" ) ;

    signal.disconnect( console3 , "receive" ) ;

    println("-----") ;

    println( "contains console1.receive: " + signal.contains( console1 , "receive" ) ) ;
    println( "contains console2.receive: " + signal.contains( console2 , "receive" ) ) ;
    println( "contains console3.receive: " + signal.contains( console3 , "receive" ) ) ;

    println("-----") ;

    signal.emit( "!" ) ;

    signal.disconnect() ; // disconnect all

    println("-----") ;

    println( "connected: " + signal.connected() ) ;
    println( "size: " + signal.size() ) ;

    println("-----") ;

    signal.emit( "Finally nothing" ) ;
}

public class Console
{
    public String name ;

    /**
     * Creates a new Console instance.
     */
    public Console( String name )
    {
        this.name = name ;
    }

    /**
     * This method is called when the receiver is connected with a Signal object.
     * @param args the argument list to dispatch.
     */
    public void receive( String message )
    {
        println( this.name + " message: " + message ) ;
    }

    /**
     * Returns the String representation of the object.
     * @return The String representation of the object.
     */
    public String toString()
    {
        return "[Console " + name + "]" ;
    }
}
```

## Install the library

Contributed libraries may be downloaded separately and manually placed within 
the "libraries" folder of your Processing sketchbook. To find (and change) the 
Processing sketchbook location on your computer, open the Preferences window 
from the Processing application (PDE) and look for the "Sketchbook location" 
item at the top.


Download and copy the contributed library's folder into the "libraries" folder at this 
location. You will need to create the "libraries" folder if this is your first 
contributed library.

By default the following locations are used for your sketchbook folder: 
  
  
  * For **Mac** users, the sketchbook folder is located inside ~/Documents/Processing. 
  * For **Windows** users, the sketchbook folder is located inside 
    'My Documents'/Processing.

The folder structure for library **VEGAS Processing** should be as follows:

```
Processing Sketch
  libraries
    vegas
      examples
      library
        vegas.jar
      reference
      src
```    

Some folders like "examples" or "src" might be missing. After library  **VEGAS Processing** has been successfully installed, restart the Processing application.

If you're having trouble, have a look at the **Processing Wiki** for more 
information: http://wiki.processing.org/w/How_to_Install_a_Contributed_Library

**Note** : It's important to use the name **"vegas"** to copy all the library folders and files.

### Git install

1 - Clone the VEGAS Processing repository in your Sketch into the "libraries" folder.

```
$ git clone git@bitbucket.org:ekameleon/vegas-processing.git vegas
```

2 - Examples

Test the **VEGAS processing** library, open a libraries/vegas-processing/examples .pde file. 
