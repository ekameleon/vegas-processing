# VEGAS Processing - OpenSource Framework - Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

### Added

 * First Vegas JAVA library
 * system.events
 * system.signals
 * system.process (in progress)
 * system.data (in progress)

## [1.0.0]


